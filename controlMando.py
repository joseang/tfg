#!/usr/bin/env python
# -*- coding: utf-8 -*-
import rospy, sys, os
from multiprocessing import Process
from sensor_msgs.msg import Joy, JointState
from modulos.ejecutarOrdenes import enviarPosicion
from modulos.funciones import leerArticulacionesActuales, joint_actual
from PyQt4 import QtCore, QtGui
from interfaz.ventanaControlJoystick import *
'''
				+           -
articulacion 1: L2          R2
articulacion 2: SUBIR       BAJAR
articulacion 3: L1          R1
articulacion 4: IZQUIERDA   DERECHA
articulacion 5: TRIANGULO   EQUIS (X)
articulacion 6: CUADRADO    CIRCULO

'''
class controlMando(QtGui.QDialog):
	def __init__(self,parent=None):
		QtGui.QWidget.__init__(self, parent,QtCore.Qt.WindowStaysOnTopHint)
		self.ui = Ui_Mando()
		self.ui.setupUi(self)
		self.estado=False
		self.tiempo=1.6
		self.connect(self.ui.botonActivar, QtCore.SIGNAL('clicked()'), self.activarMando)
		rospy.Subscriber('/joy',Joy, self.valoresJoy)

	def activarMando(self):
		if self.ui.botonActivar.isChecked():
			self.ui.botonActivar.setText('ACTIVADO')
			self.estado=True
		else:
			self.ui.botonActivar.setText('ACTIVAR')
			self.estado=False

	def valoresJoy(self, data):
		paso=0.2
		th=leerArticulacionesActuales()
		if data.buttons[0]==True:
			# print "Cuadrado"
			th[5]=th[5]+paso
		elif data.buttons[1]==True:
			# print "Equis"
			th[4]=th[4]-paso
		elif data.buttons[2]==True:
			# print "Circulo"
			th[5]=th[5]-paso
		elif data.buttons[3]==True:
			# print "Triangulo"
			th[4]=th[4]+paso
		elif data.buttons[4]==True:
			# print "L1"
			th[2]=th[2]+paso
		elif data.buttons[5]==True:
			# print "R1"
			th[2]=th[2]-paso
		elif data.buttons[6]==True:
			# print "L2"
			th[0]=th[0]-paso
		elif data.buttons[7]==True:
			# print "R2"
			th[0]=th[0]+paso
		elif data.buttons[8]==True:
			# print "Select"
			pass
		elif data.buttons[9]==True:
			# print "Start"
			pass
		elif data.buttons[10]==True:
			# print "Joystick Izquierda"
			pass
		elif data.buttons[11]==True:
			# print "Joystick Derecha"
			pass
		if data.axes[6]==1:
			# print "izquierda"
			th[3]=th[3]-paso
		elif data.axes[7]==1:
			# print "subir"
			th[1]=th[1]+paso
		elif data.axes[6]==-1:
			# print "derecha"
			th[3]=th[3]+paso
		elif data.axes[7]==-1:
			# print "bajar"
			th[1]=th[1]-paso
		if data.axes[5]==-1:
			if self.tiempo>0.1:
				self.tiempo=self.tiempo-0.3
		elif data.axes[5]==1:
			self.tiempo=self.tiempo+0.3

		if self.estado:
			enviarPosicion(th,self.tiempo)



def iniciar_nodo_joy():
	os.system("rosrun joy joy_node")

if __name__ == "__main__":
	iniciar_joy=Process(target=iniciar_nodo_joy)
	iniciar_joy.start()
	app=QtGui.QApplication(sys.argv)
	control_mando=controlMando()
	control_mando.show()
	rospy.init_node('control_mando', anonymous=False)
	sys.exit(app.exec_())
