#!/usr/bin/env python
# -*- coding: utf-8 -*-

from interfaz.ventanaGuardarPosiciones import *
from modulos.funciones import *
from modulos.ejecutarOrdenes import *
from cinematica.auxiliar_cinematica import *
import sys, os
import numpy as np

class guardarPosiciones(QtGui.QDialog):
	envioListaPosicionesATabla = QtCore.pyqtSignal(list)
	def __init__(self,parent=None):
		QtGui.QWidget.__init__(self, parent,QtCore.Qt.WindowStaysOnTopHint)
		self.ui = Ui_GuardarPosiciones()
		self.ui.setupUi(self)
		self.expandido=False
		self.cargarListaPosiciones()
		self.ventanaReducida()
		self.connect(self.ui.botonGuardarEscrita, QtCore.SIGNAL('clicked()'), self.guardarEscrita)
		self.connect(self.ui.botonGuardarActual, QtCore.SIGNAL('clicked()'), self.guardarActual)
		self.connect(self.ui.botonBorrarPosicion, QtCore.SIGNAL('clicked()'), self.borrarPosicion)
		self.connect(self.ui.botonMostrarPosicion, QtCore.SIGNAL('clicked()'), self.mostrarPosicion)
		self.connect(self.ui.botonIrPosicion, QtCore.SIGNAL('clicked()'), self.botonIrPosicion)
		self.connect(self.ui.botonExpandirVentana, QtCore.SIGNAL('clicked()'), self.expandirVentana)
		self.connect(self.ui.botonLimpiarEscrito, QtCore.SIGNAL('clicked()'), self.limpiarValores)

	@QtCore.pyqtSlot(list)
	def receptorSenalArchivoCargado(self, archivoCargado):
		self.cargarListaPosiciones()

	def recopilarPosiciones(self):
		listaPosicionesConDatos=[]
		listaPosiciones=obtenerNumeroPosicionesDisponibles()
		for numeroPosicion in listaPosiciones:
			datosPosicion=list(self.obtenerPosicionSeleccionada(numeroPosicion))
			datosPosicion=[int(numeroPosicion), datosPosicion]
			listaPosicionesConDatos.append(datosPosicion)
		return list(listaPosicionesConDatos)

	def enviarActualizacionTablaPosiciones(self):
		listaPosicionesConDatos=self.recopilarPosiciones()
		self.enviarListaPosicionesATabla(listaPosicionesConDatos)

	def enviarListaPosicionesATabla(self, listaPosicionesEnviar):
		self.envioListaPosicionesATabla.emit(listaPosicionesEnviar)

	''' Boton Guardar Escrita '''
	def guardarEscrita(self):
		numeroPosicion=self.ui.numeroPosicion.currentText()
		if numeroPosicion=="":
			mensaje=u'Introduce un número de posición'
			QtGui.QMessageBox.about(self, 'Error',mensaje)
			return
		datosPosicion=self.obtenerPosicionEscrita()
		if not datosPosicion:
			mensaje=u'Posición vacía'
			QtGui.QMessageBox.about(self, 'Error',mensaje)
			return
		[x, y, z, roll, pitch, yaw]=datosPosicion
		matH=matHomo([x,y,z,np.deg2rad(roll),np.deg2rad(pitch),np.deg2rad(yaw)])
		if self.ui.opcionAbsoluta.isChecked():
			tipoPosicion='absoluta'
			th_p=[0.1,0,0,0,0,0]
			try:
				resultado=ikine(matH,th_p)
			except:
				QtGui.QMessageBox.about(self, 'Error','No esta en el espacio de trabajo')
			if len(resultado)!=0:
				direccionArchivo=direccionAbsoluta+'/posiciones/P'+numeroPosicion+".txt"
				posicion=[x,y,z,roll,pitch,yaw]
				escribirPosicionEnArchivo(direccionArchivo,tipoPosicion,posicion)
			elif len(resultado)==0:
				QtGui.QMessageBox.about(self, 'Error','No esta en el espacio de trabajo')
		elif self.ui.opcionRelativa.isChecked():
			tipoPosicion='relativa'
			posicionReferencia=self.ui.posicionReferencia.currentText()
			if posicionReferencia=="":
				QtGui.QMessageBox.about(self, 'Error','Introduce un numero de posicion de referencia')
				return
			if numeroPosicion==posicionReferencia:
				QtGui.QMessageBox.about(self, 'Error','Las posiciones no pueden ser las mismas')
			else:
				archivoPosicion=direccionAbsoluta+'/posiciones/P'+numeroPosicion+".txt"
				archivoReferencia=str(direccionAbsoluta+"/posiciones/P"+posicionReferencia+".txt")
				datosReferencia=np.loadtxt(archivoReferencia)
				x_total=datosReferencia[2]+x
				y_total=datosReferencia[3]+y
				z_total=datosReferencia[4]+z
				roll_total=datosReferencia[5]+roll
				pitch_total=datosReferencia[6]+pitch
				yaw_total=datosReferencia[7]+yaw
				matH=matHomo([x_total,y_total,z_total,np.deg2rad(roll_total),np.deg2rad(pitch_total),np.deg2rad(yaw_total)])
				th_p=[0.1,0,0,0,0,0]
				try:
					resultado=ikine(matH,th_p)
				except:
					QtGui.QMessageBox.about(self, 'Error','No está en el espacio de trabajo')
				if len(resultado)!=0:
					escribirPosicionEnArchivo(archivoPosicion,tipoPosicion,[x,y,z,roll,pitch,yaw],posicionReferencia)
				elif len(resultado)!=0:
					QtGui.QMessageBox.about(self, 'Error','No está en el espacio de trabajo')
		self.cargarListaPosiciones()
		self.enviarActualizacionTablaPosiciones()

	def cargarListaPosiciones(self):
		self.ui.numeroPosicion.clear()
		self.ui.posicionReferencia.clear()
		listaPosiciones=obtenerNumeroPosicionesDisponibles()
		for i in listaPosiciones:
			self.ui.numeroPosicion.insertItem(0,i)
			self.ui.posicionReferencia.insertItem(0,i)

	def obtenerPosicionEscrita(self):
		try:
			x=float(self.ui.posicionX.text())
			y=float(self.ui.posicionY.text())
			z=float(self.ui.posicionZ.text())
			yaw=float(self.ui.orientacionYaw.text())
			pitch=float(self.ui.orientacionPitch.text())
			roll=float(self.ui.orientacionRoll.text())
			return x, y, z, roll, pitch, yaw
		except Exception:
			QtGui.QMessageBox.about(self, 'Error','Solo se pueden introducir numeros')
			return False

	def calcularPosicionConArticulacion(self,thetas):
		matHomo=fkineArm(thetas)
		[x, y, z]=list(np.array(matHomo[0:3,3]).reshape(-1,))
		matRotacion=matHomo[0:3,0:3]
		roll, pitch, yaw=obtenerAngulosDesdeMatRot(matRotacion)
		roll, pitch, yaw= np.rad2deg(roll), np.rad2deg(pitch), np.rad2deg(yaw)
		return [x, y, z, roll, pitch, yaw]

	''' Guardar Posicion Actual '''
	def guardarActual(self):
		thetas=leerArticulacionesActuales()
		[x,y,z,roll, pitch, yaw]=self.calcularPosicionConArticulacion(thetas)
		numeroPosicion=self.ui.numeroPosicion.currentText()
		if numeroPosicion=="":
			QtGui.QMessageBox.about(self, 'Error','Introduce un numero de posicion')
			return
		archivo=direccionAbsoluta+'/posiciones/P'+numeroPosicion+".txt"
		if self.ui.opcionAbsoluta.isChecked():
			tipo='absoluta'
			posicion=[x,y,z,roll,pitch,yaw]
			escribirPosicionEnArchivo(archivo,tipo,posicion)
		elif self.ui.opcionRelativa.isChecked():
			QtGui.QMessageBox.about(self, 'Error','Solo se pueden absolutas')
		self.cargarListaPosiciones()
		self.enviarActualizacionTablaPosiciones()

	''' Borrar Posicion Seleccionada '''
	def borrarPosicion(self):
		numeroPosicion=self.ui.numeroPosicion.currentText()
		if numeroPosicion=="":
			return
		archivo=direccionAbsoluta+"/posiciones/P"+numeroPosicion+".txt"
		os.remove(archivo)
		self.cargarListaPosiciones()
		self.enviarActualizacionTablaPosiciones()

	''' Limpiar los cuadros de texto '''
	def limpiarValores(self):
		self.ui.posicionX.clear()
		self.ui.posicionY.clear()
		self.ui.posicionZ.clear()
		self.ui.orientacionRoll.clear()
		self.ui.orientacionPitch.clear()
		self.ui.orientacionYaw.clear()

	''' Mostrar la posicion en los cuadros de texto '''
	def mostrarPosicion(self):
		numeroPosicion=self.ui.numeroPosicion.currentText()
		if numeroPosicion=="":
			QtGui.QMessageBox.about(self, 'Error','Introduce un numero de posicion')
			return
		datos=self.obtenerPosicionSeleccionada(numeroPosicion)
		if datos[0]==1:
			self.ui.opcionAbsoluta.setChecked(True)
		elif datos[0]==0: # check relativa; selecciona el valor
			self.ui.opcionRelativa.setChecked(True)
			posicionReferencia=datos[1]
			index=self.ui.posicionReferencia.findText(str(int(posicionReferencia)))
			self.ui.posicionReferencia.setCurrentIndex(index)
		self.ui.posicionX.setText('%.0f' % datos[2])
		self.ui.posicionY.setText('%.0f' % datos[3])
		self.ui.posicionZ.setText('%.0f' % datos[4])
		self.ui.orientacionRoll.setText('%.0f' % datos[5])
		self.ui.orientacionPitch.setText('%.0f' % datos[6])
		self.ui.orientacionYaw.setText('%.0f' % datos[7])

	def obtenerPosicionSeleccionada(self, numeroPosicion):
		try:
			numeroPosicion=int(numeroPosicion)
		except Exception:
			QtGui.QMessageBox.about(self, 'Error','Solo se pueden introducir numeros')
		archivo=direccionAbsoluta+"/posiciones/P"+str(numeroPosicion)+".txt"
		return np.loadtxt(archivo) # abs_rel n_pos_ref x y z pitch roll

	''' Ir a la posicion '''
	def botonIrPosicion(self):
		numeroPosicion=self.ui.numeroPosicion.currentText()
		if numeroPosicion=="":
			QtGui.QMessageBox.about(self, 'Error','Introduce un numero de posicion')
			return
		self.mostrarPosicion()
		irPosicion(numeroPosicion,3,'velocidad')

	def ventanaExpandida(self):
		self.resize(345,393)
		self.setMinimumSize(345,393)
		self.setMaximumSize(345,393)
		self.expandido=True

	def ventanaReducida(self):
		self.resize(345,163)
		self.setMinimumSize(345,163)
		self.setMaximumSize(345,163)
		self.expandido=False

	def expandirVentana(self):
		if self.expandido==False:
			self.ventanaExpandida()
		elif self.expandido==True:
			self.ventanaReducida()


if __name__ == '__main__':
	app = QtGui.QApplication(sys.argv)
	ventanaGuardarPosiciones = guardarPosiciones()
	ventanaGuardarPosiciones.show()
	sys.exit(app.exec_())
