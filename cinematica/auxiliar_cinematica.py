from transformations import rotation_matrix, euler_from_matrix, euler_matrix
import numpy as np

def obtenerAngulosDesdeMatRot(matRotacion):
	angulos=euler_from_matrix(matRotacion, 'rxyz')
	return angulos # roll, pitch, yaw

def obtenerMatRotacionDesdeAngulos(roll, pitch, yaw):
	matHomogenea=euler_matrix(roll, pitch, yaw, 'rxyz')
	return matHomogenea[:3,:3]

def matHomo(posicion): # [x,y,z,roll,pitch,yaw]
	''' los angulos ya entran en radianes '''
	roll, pitch, yaw=posicion[3], posicion[4], posicion[5]
	matHomogenea=np.zeros([4,4])
	matHomogenea[:3,:3]=obtenerMatRotacionDesdeAngulos(yaw,pitch,roll)
	matHomogenea[0,3]=posicion[0]
	matHomogenea[1,3]=posicion[1]
	matHomogenea[2,3]=posicion[2]
	return matHomogenea
