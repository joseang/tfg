#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os, time
import numpy as np
from interfaz.ventanaMovimientoManual import *
from modulos.funciones import *
from modulos.ejecutarOrdenes import *
from cinematica.kinematic_functions import *
from cinematica.auxiliar_cinematica import *
from control_msgs.msg import FollowJointTrajectoryActionGoal
from trajectory_msgs.msg import JointTrajectory
from sensor_msgs.msg import JointState
from modulos.funciones import obtenerTiempoDesdeVelocidad

class movimientoManual(QtGui.QDialog):
	def __init__(self,parent=None):
		QtGui.QWidget.__init__(self, parent,QtCore.Qt.WindowStaysOnTopHint)
		self.ui = Ui_movimientoManual()
		self.ui.setupUi(self)
		self.definirDimensionesVentana()
		self.mostrarArticulacionesActuales()
		self.configuracionSpinArticulaciones()
		self.ui.marcoTipo.currentChanged.connect(self.definirDimensionesVentana)
		self.modificandoDatosRecibidos=False
		self.datosModificados=True

		self.ui.sliderArticulacion1.valueChanged.connect(self.sliderArticulacionCambia)
		self.ui.sliderArticulacion1.valueChanged.connect(self.sliderArticulacionCambia)
		self.ui.sliderArticulacion2.valueChanged.connect(self.sliderArticulacionCambia)
		self.ui.sliderArticulacion3.valueChanged.connect(self.sliderArticulacionCambia)
		self.ui.sliderArticulacion4.valueChanged.connect(self.sliderArticulacionCambia)
		self.ui.sliderArticulacion5.valueChanged.connect(self.sliderArticulacionCambia)
		self.ui.sliderArticulacion6.valueChanged.connect(self.sliderArticulacionCambia)
		self.ui.sliderVelocidad.valueChanged.connect(self.sliderArticulacionCambia)
		self.connect(self.ui.spinArticulacion1, QtCore.SIGNAL('valueChanged(int)'), self.modificadaArticulacionSpin)
		self.connect(self.ui.spinArticulacion2, QtCore.SIGNAL('valueChanged(int)'), self.modificadaArticulacionSpin)
		self.connect(self.ui.spinArticulacion3, QtCore.SIGNAL('valueChanged(int)'), self.modificadaArticulacionSpin)
		self.connect(self.ui.spinArticulacion4, QtCore.SIGNAL('valueChanged(int)'), self.modificadaArticulacionSpin)
		self.connect(self.ui.spinArticulacion5, QtCore.SIGNAL('valueChanged(int)'), self.modificadaArticulacionSpin)
		self.connect(self.ui.spinArticulacion6, QtCore.SIGNAL('valueChanged(int)'), self.modificadaArticulacionSpin)
		self.connect(self.ui.botonAbrirPinza, QtCore.SIGNAL('clicked()'), abrirPinza)
		self.connect(self.ui.botonCerrarPinza, QtCore.SIGNAL('clicked()'), cerrarPinza)
		self.connect(self.ui.botonAbrirPinza2, QtCore.SIGNAL('clicked()'), abrirPinza)
		self.connect(self.ui.botonCerrarPinza2, QtCore.SIGNAL('clicked()'), cerrarPinza)

		self.connect(self.ui.botonMasX, QtCore.SIGNAL('clicked()'), self.moverXPositivo)
		self.connect(self.ui.botonMenosX, QtCore.SIGNAL('clicked()'), self.moverXNegativo)
		self.connect(self.ui.botonMasY, QtCore.SIGNAL('clicked()'), self.moverYPositivo)
		self.connect(self.ui.botonMenosY, QtCore.SIGNAL('clicked()'), self.moverYNegativo)
		self.connect(self.ui.botonMasZ, QtCore.SIGNAL('clicked()'), self.moverZPositivo)
		self.connect(self.ui.botonMenosZ, QtCore.SIGNAL('clicked()'), self.moverZNegativo)
		self.connect(self.ui.botonMasPitch, QtCore.SIGNAL('clicked()'), self.moverPitchPositivo)
		self.connect(self.ui.botonMenosPitch, QtCore.SIGNAL('clicked()'), self.moverPitchNegativo)
		self.connect(self.ui.botonMasRoll, QtCore.SIGNAL('clicked()'), self.moverRollPositivo)
		self.connect(self.ui.botonMenosRoll, QtCore.SIGNAL('clicked()'), self.moverRollNegativo)
		self.connect(self.ui.botonMasYaw, QtCore.SIGNAL('clicked()'), self.moverYawPositivo)
		self.connect(self.ui.botonMenosYaw, QtCore.SIGNAL('clicked()'), self.moverYawNegativo)

		rospy.Subscriber('/arm/joint_states',JointState,self.actualizarArticulaciones)

	def actualizarArticulaciones(self,data):
		if self.isActiveWindow():
			if self.datosModificados==False and self.ui.marcoTipo.currentIndex()==0 and not self.modificandoDatosRecibidos:
				self.modificandoDatosRecibidos=True
				articulaciones=data.position
				th=[]
				for i in range(len(articulaciones)):
					th.append(np.rad2deg(articulaciones[i]))
				self.actualizarValorTextoAutomatico(th)
				self.actualizarValorSlider(th)
				time.sleep(1.0)
				self.modificandoDatosRecibidos=False
				self.datosModificados=True
		else:
			self.datosModificados=False
		pass

	def configuracionSpinArticulaciones(self):
		self.ui.spinArticulacion1.setKeyboardTracking(False)
		self.ui.spinArticulacion2.setKeyboardTracking(False)
		self.ui.spinArticulacion3.setKeyboardTracking(False)
		self.ui.spinArticulacion4.setKeyboardTracking(False)
		self.ui.spinArticulacion5.setKeyboardTracking(False)
		self.ui.spinArticulacion6.setKeyboardTracking(False)

	def definirDimensionesVentana(self):
		if self.ui.marcoTipo.currentIndex()==0:
			# ARTICULACIONES
			self.ui.marcoTipo.setMinimumSize(391,501)
			self.ui.marcoTipo.setMaximumSize(391,501)
			self.ui.marcoTipo.resize(391,501)
			self.setMinimumSize(412,523)
			self.setMaximumSize(412,523)
			self.resize(412,523)
		elif self.ui.marcoTipo.currentIndex()==1:
			# XYZ
			self.ui.marcoTipo.setMinimumSize(361,221)
			self.ui.marcoTipo.setMaximumSize(361,221)
			self.ui.marcoTipo.resize(411,221)
			self.setMinimumSize(382,243)
			self.setMaximumSize(382,243)
			self.resize(382,243)
			self.datosModificados=False

	def modificadaArticulacionSpin(self, valor):
		if not self.modificandoDatosRecibidos:
			th1_grados=self.ui.spinArticulacion1.value()
			th2_grados=self.ui.spinArticulacion2.value()
			th3_grados=self.ui.spinArticulacion3.value()
			th4_grados=self.ui.spinArticulacion4.value()
			th5_grados=self.ui.spinArticulacion5.value()
			th6_grados=self.ui.spinArticulacion6.value()
			theta_grados=[th1_grados, th2_grados, th3_grados, th4_grados, th5_grados, th6_grados]
			th1_rad=np.deg2rad(th1_grados)
			th2_rad=np.deg2rad(th2_grados)
			th3_rad=np.deg2rad(th3_grados)
			th4_rad=np.deg2rad(th4_grados)
			th5_rad=np.deg2rad(th5_grados)
			th6_rad=np.deg2rad(th6_grados)
			theta_radianes=[th1_rad, th2_rad, th3_rad, th4_rad, th5_rad, th6_rad]
			valorVelocidad=self.ui.valorVelocidad.value()
			self.actualizarValorSlider(theta_grados)

	def mostrarArticulacionesActuales(self):
		thetasActuales=leerArticulacionesActuales()
		for i in range(len(thetasActuales)):
			thetasActuales[i]=int(np.degrees(thetasActuales[i]))
		self.ui.spinArticulacion1.setValue(int(thetasActuales[0]))
		self.ui.spinArticulacion2.setValue(int(thetasActuales[1]))
		self.ui.spinArticulacion3.setValue(int(thetasActuales[2]))
		self.ui.spinArticulacion4.setValue(int(thetasActuales[3]))
		self.ui.spinArticulacion5.setValue(int(thetasActuales[4]))
		self.ui.spinArticulacion6.setValue(int(thetasActuales[5]))
		self.ui.sliderArticulacion1.setValue(thetasActuales[0])
		self.ui.sliderArticulacion2.setValue(thetasActuales[1])
		self.ui.sliderArticulacion3.setValue(thetasActuales[2])
		self.ui.sliderArticulacion4.setValue(thetasActuales[3])
		self.ui.sliderArticulacion5.setValue(thetasActuales[4])
		self.ui.sliderArticulacion6.setValue(thetasActuales[5])

	def actualizarValorTextoAutomatico(self,articulaciones):
		if not self.modificandoDatosRecibidos:
			self.actualizarValorTexto(articulaciones)

	def actualizarValorTexto(self,articulaciones):
		print
		print "comienzo de actualizarValorTexto"
		self.ui.spinArticulacion1.setValue(int(articulaciones[0]))
		print "hecho 0"
		self.ui.spinArticulacion2.setValue(int(articulaciones[1]))
		print "hecho 1"
		self.ui.spinArticulacion3.setValue(int(articulaciones[2]))
		print "hecho 2"
		self.ui.spinArticulacion4.setValue(int(articulaciones[3]))
		print "hecho 3"
		self.ui.spinArticulacion5.setValue(int(articulaciones[4]))
		print "hecho 4"
		self.ui.spinArticulacion6.setValue(int(articulaciones[5]))
		print "hecho 5"


	def actualizarValorSliderAutomatico(self,articulaciones):
		if not self.modificandoDatosRecibidos:
			self.actualizarValorSlider(articulaciones)

	def actualizarValorSlider(self,articulaciones):
		self.ui.sliderArticulacion1.setValue(articulaciones[0])
		self.ui.sliderArticulacion2.setValue(articulaciones[1])
		self.ui.sliderArticulacion3.setValue(articulaciones[2])
		self.ui.sliderArticulacion4.setValue(articulaciones[3])
		self.ui.sliderArticulacion5.setValue(articulaciones[4])
		self.ui.sliderArticulacion6.setValue(articulaciones[5])

	def sliderArticulacionCambia(self):
		if not self.modificandoDatosRecibidos:
			th1_grados=int(self.ui.sliderArticulacion1.value())
			th2_grados=int(self.ui.sliderArticulacion2.value())
			th3_grados=int(self.ui.sliderArticulacion3.value())
			th4_grados=int(self.ui.sliderArticulacion4.value())
			th5_grados=int(self.ui.sliderArticulacion5.value())
			th6_grados=int(self.ui.sliderArticulacion6.value())
			th1_rad=np.deg2rad(th1_grados)
			th2_rad=np.deg2rad(th2_grados)
			th3_rad=np.deg2rad(th3_grados)
			th4_rad=np.deg2rad(th4_grados)
			th5_rad=np.deg2rad(th5_grados)
			th6_rad=np.deg2rad(th6_grados)
			thetas_grados=[th1_grados,th2_grados,th3_grados,th4_grados,th5_grados,th6_grados]
			thetas_rad=[th1_rad,th2_rad,th3_rad,th4_rad,th5_rad,th6_rad]
			self.actualizarValorTexto(thetas_grados)
			velocidad=float(self.ui.sliderVelocidad.value())
			matHomo=fkineArm(thetas_rad)
			if self.tocaMesa(matHomo):
				return
			velocidad=float(velocidad)/10
			tiempo=obtenerTiempoDesdeVelocidad(thetas_rad, velocidad)
			if self.modificandoDatosRecibidos==False:
				enviarPosicion(thetas_rad,tiempo)

	def moverXPositivo(self):
		self.moverEjeXYZ('x','positivo')
	def moverYPositivo(self):
		self.moverEjeXYZ('y','positivo')
	def moverZPositivo(self):
		self.moverEjeXYZ('z','positivo')
	def moverXNegativo(self):
		self.moverEjeXYZ('x','negativo')
	def moverYNegativo(self):
		self.moverEjeXYZ('y','negativo')
	def moverZNegativo(self):
		self.moverEjeXYZ('z','negativo')
	def moverYawPositivo(self):
		self.modificarOrientacion('yaw', 'positivo')
	def moverPitchPositivo(self):
		self.modificarOrientacion('pitch','positivo')
	def moverRollPositivo(self):
		self.modificarOrientacion('roll','positivo')
	def moverYawNegativo(self):
		self.modificarOrientacion('yaw', 'negativo')
 	def moverPitchNegativo(self):
		self.modificarOrientacion('pitch','negativo')
	def moverRollNegativo(self):
		self.modificarOrientacion('roll','negativo')

	def modificarOrientacion(self,queMover,sentido):
		if not self.modificandoDatosRecibidos:
			articulaciones=leerArticulacionesActuales()
			if comprobarHome(articulaciones):
				articulaciones=[0.1,0,0,0,0,0]
			matHomogenea=fkineArm(articulaciones)
			matRotEntrada=matHomogenea[:3,:3]
			roll, pitch, yaw=obtenerAngulosDesdeMatRot(matRotEntrada)
			paso_grados=int(self.ui.valorPaso.text())
			paso=np.deg2rad(paso_grados)
			if sentido=='negativo':
				paso=-paso
			if queMover=='yaw':
				yaw=yaw+paso
			elif queMover=='pitch':
				pitch=pitch+paso
			elif queMover=='roll':
				roll=roll+paso
			matRotacionFinal=obtenerMatRotacionDesdeAngulos(roll, pitch, yaw)

			f_roll, f_pitch, f_yaw=obtenerAngulosDesdeMatRot(matRotacionFinal)
			dif_roll=np.rad2deg(f_roll-roll)
			dif_pitch=np.rad2deg(f_pitch-pitch)
			dif_yaw=np.rad2deg(f_yaw-yaw)

			matHomogenea[:3,:3]=matRotacionFinal
			if self.tocaMesa(matHomogenea):
				return
			velocidad=self.ui.valorVelocidad.value()
			try:
				th=ikine(matHomogenea,articulaciones)[:6]
				tiempo=obtenerTiempoDesdeVelocidad(th,velocidad)
				if self.modificandoDatosRecibidos==False:
					enviarPosicion(th,tiempo)
			except:
				mensaje=u'No está en el espacio de trabajo'
				QtGui.QMessageBox.about(self, 'Error',mensaje)

	def moverEjeXYZ(self, eje, sentido):
		if not self.modificandoDatosRecibidos:
			numeroEjes={'x': 0, 'y': 1, 'z' : 2}
			paso=int(self.ui.valorPaso.text())
			articulaciones=leerArticulacionesActuales()
			if comprobarHome(articulaciones):
				articulaciones=[0.1,0,0,0,0,0]
			matHomo=fkineArm(articulaciones)
			posicion=matHomo[0:3,3]
			ejeValorInicial=posicion[numeroEjes[eje]]
			if sentido=='positivo':
				ejeValorFinal=ejeValorInicial+paso
			elif sentido=='negativo':
				ejeValorFinal=ejeValorInicial-paso
			posicion[numeroEjes[eje]]=ejeValorFinal
			matHomo[0:3,3]=posicion
			if self.tocaMesa(matHomo):
				return
			try:
				resultado=ikine(matHomo,articulaciones)
				valorVelocidad=self.ui.valorVelocidad.value()
				if len(resultado)!=0:
					resultado=resultado[:6]
					tiempo=obtenerTiempoDesdeVelocidad(resultado, valorVelocidad)
					if self.modificandoDatosRecibidos==False:
						enviarPosicion(resultado,tiempo)
				else:
					mensaje=u'No está en el espacio de trabajo'
					QtGui.QMessageBox.about(self, 'Error',mensaje)
			except:
				mensaje=u'No está en el espacio de trabajo'
				QtGui.QMessageBox.about(self, 'Error',mensaje)

	def keyPressEvent(self, event):
		if event.text()=='q': #mas_x
			if self.ui.marcoTipo.currentIndex()==1:
				self.moverEjeXYZ('x','positivo')
		elif event.text()=='w': #mas_y
			if self.ui.marcoTipo.currentIndex()==1:
				self.moverEjeXYZ('y','positivo')
		elif event.text()=='e': #mas_z
			if self.ui.marcoTipo.currentIndex()==1:
				self.moverEjeXYZ('z','positivo')
		elif event.text()=='r': #mas_yaw
			if self.ui.marcoTipo.currentIndex()==1:
				self.modificarOrientacion('yaw','positivo')
		elif event.text()=='t': #mas_pitch
			if self.ui.marcoTipo.currentIndex()==1:
				self.modificarOrientacion('pitch','positivo')
		elif event.text()=='y': #mas_roll
			if self.ui.marcoTipo.currentIndex()==1:
				self.modificarOrientacion('roll','positivo')
		elif event.text()=='a': #menos_x
			if self.ui.marcoTipo.currentIndex()==1:
				self.moverEjeXYZ('x','negativo')
		elif event.text()=='s': #menos_y
			if self.ui.marcoTipo.currentIndex()==1:
				self.moverEjeXYZ('y','negativo')
		elif event.text()=='d': #menos_z
			if self.ui.marcoTipo.currentIndex()==1:
				self.moverEjeXYZ('z','negativo')
		elif event.text()=='f': #menos_yaw
			if self.ui.marcoTipo.currentIndex()==1:
				self.modificarOrientacion('yaw','negativo')
		elif event.text()=='g': #menos_pitch
			if self.ui.marcoTipo.currentIndex()==1:
				self.modificarOrientacion('pitch','negativo')
		elif event.text()=='h': #menos_roll
			if self.ui.marcoTipo.currentIndex()==1:
				self.modificarOrientacion('roll','negativo')
		elif event.text()=='1': #abrir pinza
			if self.ui.marcoTipo.currentIndex()==1:
				abrirPinza()
		elif event.text()=='2': #cerrar pinza
			if self.ui.marcoTipo.currentIndex()==1:
				cerrarPinza()

	def tocaMesa(self,matHomogenea):
		if matHomogenea[2,3]<5:
			return True
		else:
			return False



def iniciar_general():
	roscore = subprocess.Popen('roscore')
	time.sleep(2)
	rospy.init_node('manual_powerball', anonymous=False)
	iniciarSuscripciones()

def init_real():
	servicio_init=rospy.ServiceProxy('/arm/driver/init', Trigger)
	respuesta=servicio_init()

def iniciar_real_aux():
	os.system("gnome-terminal -x sh -c 'roslaunch schunk_lwa4p robot.launch && exec bash'");
	time.sleep(6)
	init_real()

if __name__ == '__main__':
	app=QtGui.QApplication(sys.argv)
	ventanaMovimientoManual=movimientoManual()
	ventanaMovimientoManual.show()
	rospy.init_node('movimientoManual', anonymous=False)
	sys.exit(app.exec_())
