#!/usr/bin/env python
# -*- coding: utf-8 -*-
from PyQt4 import QtCore, QtGui
import rospy, sys, os
from interfaz.ventanaInstalacion import *
from interfaz.ventanaInstalarROS import *

class instalar_configurar_ROS(QtGui.QDialog):
    def __init__(self,parent=None):
        QtGui.QWidget.__init__(self,parent,QtCore.Qt.WindowStaysOnTopHint)
        self.ui=Ui_instalarROS()
        self.ui.setupUi(self)
        self.connect(self.ui.botonAceptar, QtCore.SIGNAL('clicked()'), self.aceptar)

    def aceptar(self):
        self.close()

class instalacion(QtGui.QDialog):
    def __init__(self,parent=None):
        global queInstalar
        QtGui.QWidget.__init__(self,parent,QtCore.Qt.WindowStaysOnTopHint)
        self.ui=Ui_Instalacion()
        self.ui.setupUi(self)
        self.connect(self.ui.botonAceptar, QtCore.SIGNAL('clicked()'), self.aceptar)
        queInstalar=open("instalacion/queInstalar.sh",'w+')
        queInstalar.truncate()
        queInstalar.close()
        queInstalar=open("instalacion/queInstalar.sh", 'a')

    def aceptar(self):
        global queInstalar
        if self.ui.checkInstalarROS.checkState()==2:
            ventana_conf_ros=instalar_configurar_ROS()
            ventana_conf_ros.exec_()

        if self.ui.checkSimulacion.checkState()==2:
            queInstalar.write("sh instalacion/instalacion_simulacion.sh")

        if self.ui.checkRobotReal.checkState()==2:
            queInstalar.write("sh instalacion/instalacion_robot_real.sh")

        if self.ui.checkAplicacion.checkState()==2:
            queInstalar.write("sh instalacion/instalacion_aplicacion.sh")

        queInstalar.close()
        os.system("gnome-terminal -x sh -c 'sh instalacion/queInstalar.sh'");

if __name__ == '__main__':
	app = QtGui.QApplication(sys.argv)
	ventanaInstalacion = instalacion()
	ventanaInstalacion.show()
	sys.exit(app.exec_())
