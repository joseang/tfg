sudo apt-get install ros-indigo-libpcan ros-indigo-brics-actuator ros-indigo-libntcan git
sudo apt-get install ros-indigo-hardware-interface ros-indigo-joint-limits-interface ros-indigo-controller-interface libmuparser-dev
sudo apt-get install ros-indigo-cob-control-mode-adapter ros-indigo-joint-trajectory-controller ros-indigo-rqt-joint-trajectory-controller ros-indigo-joint-state-controller ros-indigo-position-controllers ros-indigo-velocity-controllers ros-indigo-cob-dashboard ros-indigo-cob-command-gui
git clone https://github.com/ipa320/cob_common.git ~/catkin_ws/src
git clone https://github.com/ros-industrial/ros_canopen.git ~/catkin_ws/src
cd ~/catkin_ws
catkin_make
git clone https://github.com/ipa320/schunk_robots.git ~/catkin_ws/src
git clone https://github.com/ipa320/schunk_modular_robotics.git ~/catkin_ws/src
cd ~/catkin_ws
catkin_make
cd /opt/ros/indigo/share
sudo rm -r schunk_description
sudo git clone https://joseang@bitbucket.org/joseang/schunk_description.git
sudo chmod 755 schunk_description
ifconfig
sudo ip link set can0 type can bitrate 500000
sudo ip link set can0 up
sudo chmod 777 /etc/network/interfaces
echo "allow-hotplug can0" >> /etc/network/interfaces
echo "iface can0 can static" >> /etc/network/interfaces
echo "    bitrate 500000" >> /etc/network/interfaces
echo "#    up ip link set $IFACE txqueuelen 20 # uncomment if more than 4 nodes are used" >> /etc/network/interfaces
