sudo apt-get install ros-indigo-cob-command-tools ros-indigo-cob-common ros-indigo-cob-driver ros-indigo-cob-extern ros-indigo-schunk-modular-robotics
cd ~/catkin_ws/src/
git clone https://joseang@bitbucket.org/joseang/schunk_robots.git
cd ~/catkin_ws
catkin_make
cd /opt/ros/indigo/share
sudo rm -r schunk_description
sudo git clone https://joseang@bitbucket.org/joseang/schunk_description.git
sudo chmod 755 schunk_description
sudo apt-get install ros-indigo-cob-control-mode-adapter ros-indigo-cob-controller-configuration-gazebo ros-indigo-controller-manager
sudo apt-get install ros-indigo-cob-gazebo-plugins
sudo apt-get install ros-indigo-gazebo-ros ros-indigo-eigen-conversions ros-indigo-roslint
cd ~/catkin_ws/src
git clone https://github.com/JenniferBuehler/general-message-pkgs.git
git clone https://github.com/JenniferBuehler/gazebo-pkgs.git
cd ~/catkin_ws
catkin_make
sudo usermod -a -G dialout $USER
