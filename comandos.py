#!/usr/bin/env python
# -*- coding: utf-8 -*-
import rospy, sys, os
from interfaz.ventanaComandos import *
from cinematica.kinematic_functions import *
from modulos.obtenerDatosComandos import *
from modulos.funciones import *

class comandos(QtGui.QDialog):
	senalEnviarComandos = QtCore.pyqtSignal(list)
	def __init__(self,parent=None):
		QtGui.QWidget.__init__(self,parent,QtCore.Qt.WindowStaysOnTopHint)
		self.ui = Ui_Comandos()
		self.ui.setupUi(self)
		self.move(486,124)
		self.ui.arbolComandos.doubleClicked.connect(self.seleccionadaOrden)

	def enviarDatos(self, listaDatosEnviar):
		print listaDatosEnviar
		self.senalEnviarComandos.emit(listaDatosEnviar)

	def seleccionadaOrden(self):
		item=self.ui.arbolComandos.currentItem()     #item actual seleccionado
		item_parent=item.parent()           # item de orden superior al seleccionado
		model_parent=self.ui.arbolComandos.indexFromItem(item_parent,0)
		parent=model_parent.row()           # 0=Control ejes, 1=programa flujo
		model=self.ui.arbolComandos.indexFromItem(item,0)
		fila=model.row()                    # orden selecconionada dentro de cada 'parent'
		if parent==0:
			if fila==0: #abrir pinza
				datosObtenidos=["ABRIR_PINZA", None]
				self.enviarDatos(datosObtenidos)
			elif fila==1: #cerrar pinza
				datosObtenidos=["CERRAR_PINZA", None]
				self.enviarDatos(datosObtenidos)
			elif fila==2: #ir
				ventanaIrPosicion=ordenIr()
				if ventanaIrPosicion.exec_():
					datosInfo=ventanaIrPosicion.obtenerDatos()
					datosObtenidos=["IR", datosInfo]
					ventanaIrPosicion.accept()
					self.enviarDatos(datosObtenidos)
				else:
					ventanaIrPosicion.close()
			elif fila==3: #ir segun articulaciones
				ventanaTrayectoriaArticulaciones=ordenTrayectoriaArticulaciones()
				if ventanaTrayectoriaArticulaciones.exec_():
					datosInfo=ventanaTrayectoriaArticulaciones.obtenerDatos()
					datosObtenidos=["TR_ARTICULACIONES", datosInfo]
					ventanaTrayectoriaArticulaciones.accept()
					self.enviarDatos(datosObtenidos)
				else:
					ventanaTrayectoriaArticulaciones.close()
			elif fila==4: #ir segun xyz
				ventanaTrayectoriaCartesiana=ordenTrayectoriaCartesiana()
				if ventanaTrayectoriaCartesiana.exec_():
					datosInfo=ventanaTrayectoriaCartesiana.obtenerDatos()
					datosObtenidos=["TR_XYZ", datosInfo]
					ventanaTrayectoriaCartesiana.accept()
					self.enviarDatos(datosObtenidos)
				else:
					ventanaTrayectoriaCartesiana.close()
			elif fila==5: #linealmente
				ventanaLineal=ordenLineal()
				if ventanaLineal.exec_():
					datosInfo=ventanaLineal.obtenerDatos()
					datosObtenidos=["LINEAL", datosInfo]
					ventanaLineal.accept()
					self.enviarDatos(datosObtenidos)
				else:
					ventanaLineal.close()
			elif fila==6: #circularmente
				ventanaCircular=ordenCircular()
				if ventanaCircular.exec_():
					datosInfo=ventanaCircular.obtenerDatos()
					datosObtenidos=["CIRCULAR", datosInfo]
					ventanaCircular.accept()
					self.enviarDatos(datosObtenidos)
				else:
					ventanaCircular.close()
			elif fila==7: #memorizar posicion
				ventanaMemorizar=ordenMemorizar()
				if ventanaMemorizar.exec_():
					datosInfo=ventanaMemorizar.obtenerDatos()
					datosObtenidos=["MEMORIZAR", datosInfo]
					ventanaMemorizar.accept()
					self.enviarDatos(datosObtenidos)
				else:
					ventanaMemorizar.close()
		elif parent==1:
			if fila==0: #esperar
				ventanaEsperar=ordenEsperar()
				if ventanaEsperar.exec_():
					datosInfo=ventanaEsperar.obtenerDatos()
					datosObtenidos=["ESPERAR", datosInfo]
					ventanaEsperar.accept()
					self.enviarDatos(datosObtenidos)
				else:
					ventanaEsperar.close()
			elif fila==1: #saltar a
				ventanaSaltar=ordenSaltar()
				if ventanaSaltar.exec_():
					datosInfo=ventanaSaltar.obtenerDatos().rstrip()
					datosObtenidos=["SALTAR", datosInfo]
					ventanaSaltar.accept()
					self.enviarDatos(datosObtenidos)
				else:
					ventanaSaltar.close()
			elif fila==2: #comentario
				ventanaComentario=ordenComentario()
				if ventanaComentario.exec_():
					datosInfo=ventanaComentario.obtenerDatos()
					datosObtenidos=["COMENTARIO", datosInfo]
					ventanaComentario.accept()
					self.enviarDatos(datosObtenidos)
				else:
					ventanaComentario.close()
			elif fila==3: #variable
				ventanaVariable=ordenVariable()
				if ventanaVariable.exec_():
					datosInfo=ventanaVariable.obtenerDatos()
					datosObtenidos=["VARIABLE", datosInfo]
					print datosObtenidos
					ventanaVariable.accept()
					self.enviarDatos(datosObtenidos)
				else:
					ventanaVariable.close()
			elif fila==4: #saltar si...
				ventanaSaltarSi=ordenSaltarSi()
				if ventanaSaltarSi.exec_():
					datosInfo=ventanaSaltarSi.obtenerDatos()
					datosObtenidos=["SALTAR_SI", datosInfo]
					ventanaSaltarSi.accept()
					self.enviarDatos(datosObtenidos)
				else:
					ventanaSaltarSi.close()
			elif fila==5: #saltar si se activa un sensor
				ventanaSiSensor=ordenSiSensores()
				if ventanaSiSensor.exec_():
					datosInfo=ventanaSiSensor.obtenerDatos()
					datosObtenidos=["COMPROBAR_SENSOR",datosInfo]
					ventanaSiSensor.accept()
					self.enviarDatos(datosObtenidos)
				else:
					ventanaSiSensor.close()
			elif fila==6: #interrupciones
				ventanaInterrupciones=ordenInterrupciones()
				if ventanaInterrupciones.exec_():
					datosInfo=ventanaInterrupciones.obtenerDatos()
					datosObtenidos=["INTERRUPCION",datosInfo]
					ventanaInterrupciones.accept()
					self.enviarDatos(datosObtenidos)
				else:
					ventanaInterrupciones.close()
			elif fila==7: #crear subrutina
				CrearSubrutina=ordenCrearSubrutina().exec_()
			elif fila==8: #llamar subrutina
				# Se debe seleccionar entre las subrutinas ya creadas.
				ventanaSeleccionarSubrutina=ordenSeleccionarSubrutina()
				if ventanaSeleccionarSubrutina.exec_():
					datosInfo=ventanaSeleccionarSubrutina.obtenerDatos()
					datosObtenidos=["LLAMAR_SUBRUTINA",datosInfo]
					ventanaSeleccionarSubrutina.accept()
					self.enviarDatos(datosObtenidos)
				else:
					ventanaSeleccionarSubrutina.close()
			elif fila==9: #etiqueta
				ventanaEtiqueta=ordenEtiqueta()
				if ventanaEtiqueta.exec_():
					datosInfo=ventanaEtiqueta.obtenerDatos()
					datosObtenidos=["ETIQUETA",datosInfo]
					ventanaEtiqueta.accept()
					self.enviarDatos(datosObtenidos)
				else:
					ventanaEtiqueta.close()


if __name__ == '__main__':
	app = QtGui.QApplication(sys.argv)
	ventanaComandos = comandos()
	ventanaComandos.show()
	sys.exit(app.exec_())
