#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys, math, os, time
from PyQt4 import QtGui
import numpy as np
from interfaz.ventanaTablaPosiciones import *

class tablaPosiciones(QtGui.QDialog):
	def __init__(self,parent=None):
		QtGui.QWidget.__init__(self, parent,QtCore.Qt.WindowStaysOnTopHint)
		self.ui = Ui_TablaPosiciones()
		self.ui.setupUi(self)
		self.establecerAnchoDeColumnas()
		self.tamanoVentana(768,360)

	def establecerAnchoDeColumnas(self):
		self.ui.tablaPosiciones.setColumnWidth(0,42)
		self.ui.tablaPosiciones.setColumnWidth(1,75)
		self.ui.tablaPosiciones.setColumnWidth(2,80)
		self.ui.tablaPosiciones.setColumnWidth(3,60)
		self.ui.tablaPosiciones.setColumnWidth(4,80)
		self.ui.tablaPosiciones.setColumnWidth(5,80)
		self.ui.tablaPosiciones.setColumnWidth(6,80)
		self.ui.tablaPosiciones.setColumnWidth(7,80)
		self.ui.tablaPosiciones.setColumnWidth(8,80)
		self.ui.tablaPosiciones.setColumnWidth(9,80)

	def tamanoVentana(self,x,y):
		self.ui.tablaPosiciones.setMinimumSize(x,y)
		self.ui.tablaPosiciones.setMaximumSize(x,y)
		self.ui.tablaPosiciones.resize(x,y)
		self.setMinimumSize(x,y)
		self.setMaximumSize(x,y)
		self.resize(x,y)

	def mostrarPosicionesTabla(self,listaPosiciones):
		fila=0
		for posicion in listaPosiciones:
			archivo='posiciones/P'+posicion+".txt"
			datos=np.loadtxt(archivo)
			valorCasilla = QtGui.QTableWidgetItem(posicion)
			valorCasilla.setTextAlignment(4)
			self.ui.tablaPosiciones.setItem(fila,0,valorCasilla)
			if datos[0]==1: #absoluta
				valorCasilla = QtGui.QTableWidgetItem('X')
				valorCasilla.setTextAlignment(4)
				self.ui.tablaPosiciones.setItem(fila,1,valorCasilla)
			elif datos[0]==0: #relativa
				valorCasilla = QtGui.QTableWidgetItem('X')
				valorCasilla.setTextAlignment(4)
				self.ui.tablaPosiciones.setItem(fila,2,valorCasilla)
				valorCasilla = QtGui.QTableWidgetItem('%.0f' % datos[1])
				valorCasilla.setTextAlignment(4)
				self.ui.tablaPosiciones.setItem(fila,3,valorCasilla)
			for i in range(6): # antes range(5)
				valorCasilla = QtGui.QTableWidgetItem(int(datos[i+2]))
				valorCasilla.setTextAlignment(4)
				self.ui.tablaPosiciones.setItem(fila,4+i,valorCasilla)
			fila=fila+1
			self.ui.tablaPosiciones.insertRow(fila)
			time.sleep(0.2)

	def borrarTabla(self):
		self.ui.tablaPosiciones.setRowCount(0)
		self.ui.tablaPosiciones.setRowCount(1)

	def introducirPosicionesEnTabla(self,datosPosicion,fila):
		valorCasilla = QtGui.QTableWidgetItem(str(datosPosicion[0]))
		valorCasilla.setTextAlignment(4)
		self.ui.tablaPosiciones.setItem(fila,0,valorCasilla)
		if int(datosPosicion[1])==1: #absoluta
			valorCasilla = QtGui.QTableWidgetItem('X')
			valorCasilla.setTextAlignment(4)
			self.ui.tablaPosiciones.setItem(fila,1,valorCasilla)
		elif int(datosPosicion[1])==0: #relativa
			valorCasilla = QtGui.QTableWidgetItem('X')
			valorCasilla.setTextAlignment(4)
			self.ui.tablaPosiciones.setItem(fila,2,valorCasilla)
			valorCasilla = QtGui.QTableWidgetItem('%.0f' % datosPosicion[2])
			valorCasilla.setTextAlignment(4)
			self.ui.tablaPosiciones.setItem(fila,3,valorCasilla)
		for i in range(6): # antes range(5)
			valorCasilla = QtGui.QTableWidgetItem('%.0f' % datosPosicion[i+3])
			valorCasilla.setTextAlignment(4)
			self.ui.tablaPosiciones.setItem(fila,4+i,valorCasilla)
		self.ui.tablaPosiciones.insertRow(fila+1)

	@QtCore.pyqtSlot(list)
	def receptorListaPosiciones(self, listaPosicionesRecibidas):
		fila=0
		self.borrarTabla()
		for lista in listaPosicionesRecibidas:
			lista_aux=lista[1]
			lista_aux.insert(0,lista[0])
			self.introducirPosicionesEnTabla(lista_aux, fila)
			fila+=1

if __name__ == "__main__":
	app=QtGui.QApplication(sys.argv)
	tabla=tablaPosiciones()
	tabla.show()
	sys.exit(app.exec_())
