# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ventanaOrdenesIntroducidas.ui'
#
# Created: Tue Dec  8 13:00:09 2015
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
import sys
sys.path.insert(0, '../')
from path_absoluto import obtenerDireccionAbsoluta
direccionAbsoluta=obtenerDireccionAbsoluta()

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Programa(object):
    def setupUi(self, Programa):
        Programa.setObjectName(_fromUtf8("Programa"))
        Programa.resize(421, 573)
        Programa.setMinimumSize(QtCore.QSize(421, 573))
        Programa.setMaximumSize(QtCore.QSize(421, 573))
        self.botonBorrar = QtGui.QPushButton(Programa)
        self.botonBorrar.setGeometry(QtCore.QRect(380, 10, 31, 27))
        self.botonBorrar.setText(_fromUtf8(""))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8(direccionAbsoluta+"/interfaz/iconos/borrar.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.botonBorrar.setIcon(icon)
        self.botonBorrar.setCheckable(True)
        self.botonBorrar.setObjectName(_fromUtf8("botonBorrar"))
        self.gridLayoutWidget = QtGui.QWidget(Programa)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(10, 50, 401, 511))
        self.gridLayoutWidget.setObjectName(_fromUtf8("gridLayoutWidget"))
        self.gridLayout = QtGui.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setMargin(0)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.tablaOrdenes = QtGui.QTableWidget(self.gridLayoutWidget)
        self.tablaOrdenes.setObjectName(_fromUtf8("tablaOrdenes"))
        self.tablaOrdenes.setColumnCount(1)
        self.tablaOrdenes.setRowCount(1)
        item = QtGui.QTableWidgetItem()
        self.tablaOrdenes.setVerticalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(10)
        item.setFont(font)
        self.tablaOrdenes.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        item.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
        self.tablaOrdenes.setItem(0, 0, item)
        self.tablaOrdenes.horizontalHeader().setDefaultSectionSize(400)
        self.gridLayout.addWidget(self.tablaOrdenes, 0, 0, 1, 1)
        self.botonGuardar = QtGui.QPushButton(Programa)
        self.botonGuardar.setGeometry(QtCore.QRect(10, 10, 61, 27))
        self.botonGuardar.setMinimumSize(QtCore.QSize(0, 0))
        self.botonGuardar.setMaximumSize(QtCore.QSize(81, 27))
        font = QtGui.QFont()
        font.setPointSize(9)
        self.botonGuardar.setFont(font)
        self.botonGuardar.setObjectName(_fromUtf8("botonGuardar"))
        self.botonAbrir = QtGui.QPushButton(Programa)
        self.botonAbrir.setGeometry(QtCore.QRect(180, 10, 51, 27))
        self.botonAbrir.setMinimumSize(QtCore.QSize(0, 0))
        self.botonAbrir.setMaximumSize(QtCore.QSize(81, 27))
        font = QtGui.QFont()
        font.setPointSize(9)
        self.botonAbrir.setFont(font)
        self.botonAbrir.setObjectName(_fromUtf8("botonAbrir"))
        self.botonGuardarComo = QtGui.QPushButton(Programa)
        self.botonGuardarComo.setGeometry(QtCore.QRect(80, 10, 91, 27))
        self.botonGuardarComo.setMinimumSize(QtCore.QSize(0, 0))
        self.botonGuardarComo.setMaximumSize(QtCore.QSize(120, 27))
        font = QtGui.QFont()
        font.setPointSize(9)
        self.botonGuardarComo.setFont(font)
        self.botonGuardarComo.setObjectName(_fromUtf8("botonGuardarComo"))

        self.retranslateUi(Programa)
        QtCore.QMetaObject.connectSlotsByName(Programa)
        Programa.setTabOrder(self.botonGuardar, self.botonGuardarComo)
        Programa.setTabOrder(self.botonGuardarComo, self.botonAbrir)
        Programa.setTabOrder(self.botonAbrir, self.botonBorrar)
        Programa.setTabOrder(self.botonBorrar, self.tablaOrdenes)

    def retranslateUi(self, Programa):
        Programa.setWindowTitle(_translate("Programa", "Programa", None))
        item = self.tablaOrdenes.verticalHeaderItem(0)
        item.setText(_translate("Programa", "1", None))
        item = self.tablaOrdenes.horizontalHeaderItem(0)
        item.setText(_translate("Programa", "Instrucciones", None))
        __sortingEnabled = self.tablaOrdenes.isSortingEnabled()
        self.tablaOrdenes.setSortingEnabled(False)
        self.tablaOrdenes.setSortingEnabled(__sortingEnabled)
        self.botonGuardar.setText(_translate("Programa", "Guardar", None))
        self.botonAbrir.setText(_translate("Programa", "Abrir", None))
        self.botonGuardarComo.setText(_translate("Programa", "Guardar como", None))
