# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ventanaTrayectoriaCartesiana.ui'
#
# Created: Mon Jun  6 02:42:08 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_TrayectoriaCartesiana(object):
    def setupUi(self, TrayectoriaCartesiana):
        TrayectoriaCartesiana.setObjectName(_fromUtf8("TrayectoriaCartesiana"))
        TrayectoriaCartesiana.resize(351, 152)
        TrayectoriaCartesiana.setMinimumSize(QtCore.QSize(351, 152))
        TrayectoriaCartesiana.setMaximumSize(QtCore.QSize(351, 152))
        self.label = QtGui.QLabel(TrayectoriaCartesiana)
        self.label.setGeometry(QtCore.QRect(20, 20, 181, 17))
        self.label.setObjectName(_fromUtf8("label"))
        self.textoDireccion = QtGui.QLineEdit(TrayectoriaCartesiana)
        self.textoDireccion.setGeometry(QtCore.QRect(20, 60, 201, 29))
        self.textoDireccion.setObjectName(_fromUtf8("textoDireccion"))
        self.botonCargar = QtGui.QPushButton(TrayectoriaCartesiana)
        self.botonCargar.setGeometry(QtCore.QRect(240, 60, 91, 27))
        self.botonCargar.setObjectName(_fromUtf8("botonCargar"))
        self.botonAceptar = QtGui.QPushButton(TrayectoriaCartesiana)
        self.botonAceptar.setGeometry(QtCore.QRect(110, 110, 91, 27))
        self.botonAceptar.setObjectName(_fromUtf8("botonAceptar"))
        self.botonEjecutar = QtGui.QPushButton(TrayectoriaCartesiana)
        self.botonEjecutar.setGeometry(QtCore.QRect(240, 110, 91, 27))
        self.botonEjecutar.setObjectName(_fromUtf8("botonEjecutar"))

        self.retranslateUi(TrayectoriaCartesiana)
        QtCore.QMetaObject.connectSlotsByName(TrayectoriaCartesiana)

    def retranslateUi(self, TrayectoriaCartesiana):
        TrayectoriaCartesiana.setWindowTitle(_translate("TrayectoriaCartesiana", "Ir según trayectoria cartesiana", None))
        self.label.setText(_translate("TrayectoriaCartesiana", "Archivo con la trayectoria:", None))
        self.botonCargar.setText(_translate("TrayectoriaCartesiana", "Cargar", None))
        self.botonAceptar.setText(_translate("TrayectoriaCartesiana", "Aceptar", None))
        self.botonEjecutar.setText(_translate("TrayectoriaCartesiana", "Ejecutar", None))

