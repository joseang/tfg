# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ventanaAuxiliar.ui'
#
# Created: Mon Jun  6 12:24:39 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_ventanaAuxiliar(object):
    def setupUi(self, ventanaAuxiliar):
        ventanaAuxiliar.setObjectName(_fromUtf8("ventanaAuxiliar"))
        ventanaAuxiliar.resize(371, 344)
        ventanaAuxiliar.setMinimumSize(QtCore.QSize(371, 344))
        ventanaAuxiliar.setMaximumSize(QtCore.QSize(371, 344))
        self.tabElegir = QtGui.QTabWidget(ventanaAuxiliar)
        self.tabElegir.setGeometry(QtCore.QRect(10, 10, 351, 321))
        self.tabElegir.setObjectName(_fromUtf8("tabElegir"))
        self.tab = QtGui.QWidget()
        self.tab.setObjectName(_fromUtf8("tab"))
        self.arbolVariables = QtGui.QTreeWidget(self.tab)
        self.arbolVariables.setGeometry(QtCore.QRect(10, 10, 331, 271))
        self.arbolVariables.setObjectName(_fromUtf8("arbolVariables"))
        item_0 = QtGui.QTreeWidgetItem(self.arbolVariables)
        self.tabElegir.addTab(self.tab, _fromUtf8(""))
        self.tab_2 = QtGui.QWidget()
        self.tab_2.setObjectName(_fromUtf8("tab_2"))
        self.arbolSensores = QtGui.QTreeWidget(self.tab_2)
        self.arbolSensores.setGeometry(QtCore.QRect(10, 10, 331, 271))
        self.arbolSensores.setObjectName(_fromUtf8("arbolSensores"))
        self.tabElegir.addTab(self.tab_2, _fromUtf8(""))
        self.tab_4 = QtGui.QWidget()
        self.tab_4.setObjectName(_fromUtf8("tab_4"))
        self.arbolCamaras = QtGui.QTreeWidget(self.tab_4)
        self.arbolCamaras.setGeometry(QtCore.QRect(10, 10, 331, 271))
        self.arbolCamaras.setObjectName(_fromUtf8("arbolCamaras"))
        self.tabElegir.addTab(self.tab_4, _fromUtf8(""))
        self.tab_3 = QtGui.QWidget()
        self.tab_3.setObjectName(_fromUtf8("tab_3"))
        self.arbolInterrupciones = QtGui.QTreeWidget(self.tab_3)
        self.arbolInterrupciones.setGeometry(QtCore.QRect(10, 10, 331, 271))
        self.arbolInterrupciones.setObjectName(_fromUtf8("arbolInterrupciones"))
        self.tabElegir.addTab(self.tab_3, _fromUtf8(""))

        self.retranslateUi(ventanaAuxiliar)
        self.tabElegir.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(ventanaAuxiliar)

    def retranslateUi(self, ventanaAuxiliar):
        ventanaAuxiliar.setWindowTitle(_translate("ventanaAuxiliar", "Variables, Sensores, Interrupciones", None))
        self.arbolVariables.headerItem().setText(0, _translate("ventanaAuxiliar", "Variable", None))
        self.arbolVariables.headerItem().setText(1, _translate("ventanaAuxiliar", "Valor", None))
        __sortingEnabled = self.arbolVariables.isSortingEnabled()
        self.arbolVariables.setSortingEnabled(False)
        self.arbolVariables.setSortingEnabled(__sortingEnabled)
        self.tabElegir.setTabText(self.tabElegir.indexOf(self.tab), _translate("ventanaAuxiliar", "Variables", None))
        self.arbolSensores.headerItem().setText(0, _translate("ventanaAuxiliar", "Tipo", None))
        self.arbolSensores.headerItem().setText(1, _translate("ventanaAuxiliar", "Número", None))
        self.arbolSensores.headerItem().setText(2, _translate("ventanaAuxiliar", "Estado", None))
        self.tabElegir.setTabText(self.tabElegir.indexOf(self.tab_2), _translate("ventanaAuxiliar", "Sensores", None))
        self.arbolCamaras.headerItem().setText(0, _translate("ventanaAuxiliar", "Número", None))
        self.arbolCamaras.headerItem().setText(1, _translate("ventanaAuxiliar", "Topic", None))
        self.arbolCamaras.headerItem().setText(2, _translate("ventanaAuxiliar", "Ver", None))
        self.tabElegir.setTabText(self.tabElegir.indexOf(self.tab_4), _translate("ventanaAuxiliar", "Cámaras", None))
        self.arbolInterrupciones.headerItem().setText(0, _translate("ventanaAuxiliar", "Sensor", None))
        self.arbolInterrupciones.headerItem().setText(1, _translate("ventanaAuxiliar", "Subrutina", None))
        self.arbolInterrupciones.headerItem().setText(2, _translate("ventanaAuxiliar", "Estado", None))
        self.tabElegir.setTabText(self.tabElegir.indexOf(self.tab_3), _translate("ventanaAuxiliar", "Interrupciones", None))

