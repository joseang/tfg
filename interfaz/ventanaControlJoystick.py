# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ventanaControlJoystick.ui'
#
# Created: Sat May 14 11:24:26 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Mando(object):
    def setupUi(self, Mando):
        Mando.setObjectName(_fromUtf8("Mando"))
        Mando.resize(202, 271)
        Mando.setMinimumSize(QtCore.QSize(202, 271))
        Mando.setMaximumSize(QtCore.QSize(202, 271))
        self.botonActivar = QtGui.QPushButton(Mando)
        self.botonActivar.setGeometry(QtCore.QRect(60, 190, 91, 61))
        self.botonActivar.setCheckable(True)
        self.botonActivar.setObjectName(_fromUtf8("botonActivar"))
        self.verticalLayoutWidget = QtGui.QWidget(Mando)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(20, 40, 101, 134))
        self.verticalLayoutWidget.setObjectName(_fromUtf8("verticalLayoutWidget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setMargin(0)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.label = QtGui.QLabel(self.verticalLayoutWidget)
        self.label.setObjectName(_fromUtf8("label"))
        self.verticalLayout.addWidget(self.label)
        self.label_2 = QtGui.QLabel(self.verticalLayoutWidget)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.verticalLayout.addWidget(self.label_2)
        self.label_3 = QtGui.QLabel(self.verticalLayoutWidget)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.verticalLayout.addWidget(self.label_3)
        self.label_4 = QtGui.QLabel(self.verticalLayoutWidget)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.verticalLayout.addWidget(self.label_4)
        self.label_5 = QtGui.QLabel(self.verticalLayoutWidget)
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.verticalLayout.addWidget(self.label_5)
        self.label_6 = QtGui.QLabel(self.verticalLayoutWidget)
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.verticalLayout.addWidget(self.label_6)
        self.verticalLayoutWidget_2 = QtGui.QWidget(Mando)
        self.verticalLayoutWidget_2.setGeometry(QtCore.QRect(130, 40, 21, 134))
        self.verticalLayoutWidget_2.setObjectName(_fromUtf8("verticalLayoutWidget_2"))
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.verticalLayoutWidget_2)
        self.verticalLayout_2.setMargin(0)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.label_7 = QtGui.QLabel(self.verticalLayoutWidget_2)
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.verticalLayout_2.addWidget(self.label_7)
        self.label_10 = QtGui.QLabel(self.verticalLayoutWidget_2)
        self.label_10.setObjectName(_fromUtf8("label_10"))
        self.verticalLayout_2.addWidget(self.label_10)
        self.label_9 = QtGui.QLabel(self.verticalLayoutWidget_2)
        self.label_9.setObjectName(_fromUtf8("label_9"))
        self.verticalLayout_2.addWidget(self.label_9)
        self.label_12 = QtGui.QLabel(self.verticalLayoutWidget_2)
        self.label_12.setObjectName(_fromUtf8("label_12"))
        self.verticalLayout_2.addWidget(self.label_12)
        self.label_11 = QtGui.QLabel(self.verticalLayoutWidget_2)
        self.label_11.setObjectName(_fromUtf8("label_11"))
        self.verticalLayout_2.addWidget(self.label_11)
        self.label_8 = QtGui.QLabel(self.verticalLayoutWidget_2)
        self.label_8.setObjectName(_fromUtf8("label_8"))
        self.verticalLayout_2.addWidget(self.label_8)
        self.verticalLayoutWidget_3 = QtGui.QWidget(Mando)
        self.verticalLayoutWidget_3.setGeometry(QtCore.QRect(160, 40, 21, 134))
        self.verticalLayoutWidget_3.setObjectName(_fromUtf8("verticalLayoutWidget_3"))
        self.verticalLayout_3 = QtGui.QVBoxLayout(self.verticalLayoutWidget_3)
        self.verticalLayout_3.setMargin(0)
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.label_13 = QtGui.QLabel(self.verticalLayoutWidget_3)
        self.label_13.setObjectName(_fromUtf8("label_13"))
        self.verticalLayout_3.addWidget(self.label_13)
        self.label_14 = QtGui.QLabel(self.verticalLayoutWidget_3)
        self.label_14.setObjectName(_fromUtf8("label_14"))
        self.verticalLayout_3.addWidget(self.label_14)
        self.label_15 = QtGui.QLabel(self.verticalLayoutWidget_3)
        self.label_15.setObjectName(_fromUtf8("label_15"))
        self.verticalLayout_3.addWidget(self.label_15)
        self.label_16 = QtGui.QLabel(self.verticalLayoutWidget_3)
        self.label_16.setObjectName(_fromUtf8("label_16"))
        self.verticalLayout_3.addWidget(self.label_16)
        self.label_17 = QtGui.QLabel(self.verticalLayoutWidget_3)
        self.label_17.setObjectName(_fromUtf8("label_17"))
        self.verticalLayout_3.addWidget(self.label_17)
        self.label_18 = QtGui.QLabel(self.verticalLayoutWidget_3)
        self.label_18.setObjectName(_fromUtf8("label_18"))
        self.verticalLayout_3.addWidget(self.label_18)
        self.horizontalLayoutWidget = QtGui.QWidget(Mando)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(130, 10, 51, 21))
        self.horizontalLayoutWidget.setObjectName(_fromUtf8("horizontalLayoutWidget"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setMargin(0)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label_19 = QtGui.QLabel(self.horizontalLayoutWidget)
        self.label_19.setAlignment(QtCore.Qt.AlignCenter)
        self.label_19.setObjectName(_fromUtf8("label_19"))
        self.horizontalLayout.addWidget(self.label_19)
        self.label_20 = QtGui.QLabel(self.horizontalLayoutWidget)
        self.label_20.setAlignment(QtCore.Qt.AlignCenter)
        self.label_20.setObjectName(_fromUtf8("label_20"))
        self.horizontalLayout.addWidget(self.label_20)

        self.retranslateUi(Mando)
        QtCore.QMetaObject.connectSlotsByName(Mando)

    def retranslateUi(self, Mando):
        Mando.setWindowTitle(_translate("Mando", "Control Joystick", None))
        self.botonActivar.setText(_translate("Mando", "ACTIVAR", None))
        self.label.setText(_translate("Mando", "Articulación 1", None))
        self.label_2.setText(_translate("Mando", "Articulación 2", None))
        self.label_3.setText(_translate("Mando", "Articulación 3", None))
        self.label_4.setText(_translate("Mando", "Articulación 4", None))
        self.label_5.setText(_translate("Mando", "Articulación 5", None))
        self.label_6.setText(_translate("Mando", "Articulación 6", None))
        self.label_7.setText(_translate("Mando", "L2", None))
        self.label_10.setText(_translate("Mando", "↑", None))
        self.label_9.setText(_translate("Mando", "L1", None))
        self.label_12.setText(_translate("Mando", "←", None))
        self.label_11.setText(_translate("Mando", "▲ ", None))
        self.label_8.setText(_translate("Mando", "▇", None))
        self.label_13.setText(_translate("Mando", "R2", None))
        self.label_14.setText(_translate("Mando", "↓", None))
        self.label_15.setText(_translate("Mando", "R1", None))
        self.label_16.setText(_translate("Mando", "→", None))
        self.label_17.setText(_translate("Mando", "X", None))
        self.label_18.setText(_translate("Mando", "○", None))
        self.label_19.setText(_translate("Mando", "+", None))
        self.label_20.setText(_translate("Mando", "-", None))

