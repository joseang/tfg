# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ventanaComandos_Nuevo.ui'
#
# Created: Sat Jun  4 23:46:33 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Comandos(object):
    def setupUi(self, Comandos):
        Comandos.setObjectName(_fromUtf8("Comandos"))
        Comandos.resize(290, 430)
        Comandos.setMinimumSize(QtCore.QSize(290, 430))
        Comandos.setMaximumSize(QtCore.QSize(290, 430))
        font = QtGui.QFont()
        font.setPointSize(10)
        Comandos.setFont(font)
        self.arbolComandos = QtGui.QTreeWidget(Comandos)
        self.arbolComandos.setGeometry(QtCore.QRect(10, 10, 270, 411))
        self.arbolComandos.setObjectName(_fromUtf8("arbolComandos"))
        item_0 = QtGui.QTreeWidgetItem(self.arbolComandos)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_0 = QtGui.QTreeWidgetItem(self.arbolComandos)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1 = QtGui.QTreeWidgetItem(item_0)

        self.retranslateUi(Comandos)
        QtCore.QMetaObject.connectSlotsByName(Comandos)

    def retranslateUi(self, Comandos):
        Comandos.setWindowTitle(_translate("Comandos", "Lista de comandos", None))
        self.arbolComandos.headerItem().setText(0, _translate("Comandos", "Comandos", None))
        __sortingEnabled = self.arbolComandos.isSortingEnabled()
        self.arbolComandos.setSortingEnabled(False)
        self.arbolComandos.topLevelItem(0).setText(0, _translate("Comandos", "CONTROL DE EJES", None))
        self.arbolComandos.topLevelItem(0).child(0).setText(0, _translate("Comandos", "Abrir Pinza", None))
        self.arbolComandos.topLevelItem(0).child(1).setText(0, _translate("Comandos", "Cerrar Pinza", None))
        self.arbolComandos.topLevelItem(0).child(2).setText(0, _translate("Comandos", "Ir a la Posición #", None))
        self.arbolComandos.topLevelItem(0).child(3).setText(0, _translate("Comandos", "Ir según articulaciones", None))
        self.arbolComandos.topLevelItem(0).child(4).setText(0, _translate("Comandos", "Ir según trayectoria XYZ", None))
        self.arbolComandos.topLevelItem(0).child(5).setText(0, _translate("Comandos", "Ir Linealmente a la Posición #", None))
        self.arbolComandos.topLevelItem(0).child(6).setText(0, _translate("Comandos", "Ir Circularmente a la Posición #", None))
        self.arbolComandos.topLevelItem(0).child(7).setText(0, _translate("Comandos", "Memorizar Posición #", None))
        self.arbolComandos.topLevelItem(1).setText(0, _translate("Comandos", "PROGRAMA FLUJO", None))
        self.arbolComandos.topLevelItem(1).child(0).setText(0, _translate("Comandos", "Esperar", None))
        self.arbolComandos.topLevelItem(1).child(1).setText(0, _translate("Comandos", "Salta A", None))
        self.arbolComandos.topLevelItem(1).child(2).setText(0, _translate("Comandos", "Comentario", None))
        self.arbolComandos.topLevelItem(1).child(3).setText(0, _translate("Comandos", "Poner Variable", None))
        self.arbolComandos.topLevelItem(1).child(4).setText(0, _translate("Comandos", "Si <variable>...Salta", None))
        self.arbolComandos.topLevelItem(1).child(5).setText(0, _translate("Comandos", "Si #Entrada ... Salta ", None))
        self.arbolComandos.topLevelItem(1).child(6).setText(0, _translate("Comandos", "Interrupciones", None))
        self.arbolComandos.topLevelItem(1).child(7).setText(0, _translate("Comandos", "Crear Subrutina", None))
        self.arbolComandos.topLevelItem(1).child(8).setText(0, _translate("Comandos", "Llamar Subrutina", None))
        self.arbolComandos.topLevelItem(1).child(9).setText(0, _translate("Comandos", "Etiqueta", None))
        self.arbolComandos.setSortingEnabled(__sortingEnabled)

