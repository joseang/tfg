# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ventanaInstalacion.ui'
#
# Created: Fri Aug 26 19:36:52 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Instalacion(object):
    def setupUi(self, Instalacion):
        Instalacion.setObjectName(_fromUtf8("Instalacion"))
        Instalacion.resize(252, 185)
        Instalacion.setMinimumSize(QtCore.QSize(252, 185))
        Instalacion.setMaximumSize(QtCore.QSize(252, 185))
        self.checkInstalarROS = QtGui.QCheckBox(Instalacion)
        self.checkInstalarROS.setGeometry(QtCore.QRect(10, 10, 201, 22))
        self.checkInstalarROS.setObjectName(_fromUtf8("checkInstalarROS"))
        self.checkSimulacion = QtGui.QCheckBox(Instalacion)
        self.checkSimulacion.setGeometry(QtCore.QRect(10, 40, 221, 22))
        self.checkSimulacion.setObjectName(_fromUtf8("checkSimulacion"))
        self.checkRobotReal = QtGui.QCheckBox(Instalacion)
        self.checkRobotReal.setGeometry(QtCore.QRect(10, 70, 221, 22))
        self.checkRobotReal.setObjectName(_fromUtf8("checkRobotReal"))
        self.checkAplicacion = QtGui.QCheckBox(Instalacion)
        self.checkAplicacion.setGeometry(QtCore.QRect(10, 100, 161, 22))
        self.checkAplicacion.setObjectName(_fromUtf8("checkAplicacion"))
        self.botonAceptar = QtGui.QPushButton(Instalacion)
        self.botonAceptar.setGeometry(QtCore.QRect(80, 140, 98, 27))
        self.botonAceptar.setObjectName(_fromUtf8("botonAceptar"))

        self.retranslateUi(Instalacion)
        QtCore.QMetaObject.connectSlotsByName(Instalacion)

    def retranslateUi(self, Instalacion):
        Instalacion.setWindowTitle(_translate("Instalacion", "Instalación", None))
        self.checkInstalarROS.setText(_translate("Instalacion", "Instalar y configurar ROS", None))
        self.checkSimulacion.setText(_translate("Instalacion", "Instalar Paquetes Simulación", None))
        self.checkRobotReal.setText(_translate("Instalacion", "Instalar Paquetes Robot Real", None))
        self.checkAplicacion.setText(_translate("Instalacion", "Instalar Aplicación", None))
        self.botonAceptar.setText(_translate("Instalacion", "Aceptar", None))

