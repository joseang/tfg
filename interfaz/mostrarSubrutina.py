# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mostrarSubrutina.ui'
#
# Created: Fri Jun  3 11:42:11 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_mostrarSubrutina(object):
    def setupUi(self, mostrarSubrutina):
        mostrarSubrutina.setObjectName(_fromUtf8("mostrarSubrutina"))
        mostrarSubrutina.resize(305, 355)
        mostrarSubrutina.setMinimumSize(QtCore.QSize(305, 355))
        mostrarSubrutina.setMaximumSize(QtCore.QSize(305, 355))
        self.tableWidget = QtGui.QTableWidget(mostrarSubrutina)
        self.tableWidget.setGeometry(QtCore.QRect(0, 10, 301, 341))
        self.tableWidget.setObjectName(_fromUtf8("tableWidget"))
        self.tableWidget.setColumnCount(1)
        self.tableWidget.setRowCount(1)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(0, item)

        self.retranslateUi(mostrarSubrutina)
        QtCore.QMetaObject.connectSlotsByName(mostrarSubrutina)

    def retranslateUi(self, mostrarSubrutina):
        mostrarSubrutina.setWindowTitle(_translate("mostrarSubrutina", "Subrutina", None))
        item = self.tableWidget.verticalHeaderItem(0)
        item.setText(_translate("mostrarSubrutina", "1", None))
        item = self.tableWidget.horizontalHeaderItem(0)
        item.setText(_translate("mostrarSubrutina", "Subrutina", None))

