# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ventanaTrayectoriaArticulaciones.ui'
#
# Created: Mon Jun  6 00:59:18 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_TrayectoriaArticulacion(object):
    def setupUi(self, TrayectoriaArticulacion):
        TrayectoriaArticulacion.setObjectName(_fromUtf8("TrayectoriaArticulacion"))
        TrayectoriaArticulacion.resize(351, 152)
        TrayectoriaArticulacion.setMinimumSize(QtCore.QSize(351, 152))
        TrayectoriaArticulacion.setMaximumSize(QtCore.QSize(351, 152))
        self.label = QtGui.QLabel(TrayectoriaArticulacion)
        self.label.setGeometry(QtCore.QRect(20, 20, 181, 17))
        self.label.setObjectName(_fromUtf8("label"))
        self.textoDireccion = QtGui.QLineEdit(TrayectoriaArticulacion)
        self.textoDireccion.setGeometry(QtCore.QRect(20, 60, 201, 29))
        self.textoDireccion.setObjectName(_fromUtf8("textoDireccion"))
        self.botonCargar = QtGui.QPushButton(TrayectoriaArticulacion)
        self.botonCargar.setGeometry(QtCore.QRect(240, 60, 91, 27))
        self.botonCargar.setObjectName(_fromUtf8("botonCargar"))
        self.botonAceptar = QtGui.QPushButton(TrayectoriaArticulacion)
        self.botonAceptar.setGeometry(QtCore.QRect(110, 110, 91, 27))
        self.botonAceptar.setObjectName(_fromUtf8("botonAceptar"))
        self.botonEjecutar = QtGui.QPushButton(TrayectoriaArticulacion)
        self.botonEjecutar.setGeometry(QtCore.QRect(240, 110, 91, 27))
        self.botonEjecutar.setObjectName(_fromUtf8("botonEjecutar"))

        self.retranslateUi(TrayectoriaArticulacion)
        QtCore.QMetaObject.connectSlotsByName(TrayectoriaArticulacion)

    def retranslateUi(self, TrayectoriaArticulacion):
        TrayectoriaArticulacion.setWindowTitle(_translate("TrayectoriaArticulacion", "Ir según trayectoria de articulaciones", None))
        self.label.setText(_translate("TrayectoriaArticulacion", "Archivo con la trayectoria:", None))
        self.botonCargar.setText(_translate("TrayectoriaArticulacion", "Cargar", None))
        self.botonAceptar.setText(_translate("TrayectoriaArticulacion", "Aceptar", None))
        self.botonEjecutar.setText(_translate("TrayectoriaArticulacion", "Ejecutar", None))

