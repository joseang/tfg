# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ventanaInstalarROS.ui'
#
# Created: Wed Sep 28 09:06:55 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_instalarROS(object):
    def setupUi(self, instalarROS):
        instalarROS.setObjectName(_fromUtf8("instalarROS"))
        instalarROS.resize(543, 697)
        instalarROS.setMinimumSize(QtCore.QSize(543, 697))
        instalarROS.setMaximumSize(QtCore.QSize(543, 697))
        self.label = QtGui.QLabel(instalarROS)
        self.label.setGeometry(QtCore.QRect(20, 20, 181, 17))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName(_fromUtf8("label"))
        self.textBrowser = QtGui.QTextBrowser(instalarROS)
        self.textBrowser.setGeometry(QtCore.QRect(20, 40, 501, 611))
        self.textBrowser.setObjectName(_fromUtf8("textBrowser"))
        self.botonAceptar = QtGui.QPushButton(instalarROS)
        self.botonAceptar.setGeometry(QtCore.QRect(210, 660, 98, 27))
        self.botonAceptar.setObjectName(_fromUtf8("botonAceptar"))

        self.retranslateUi(instalarROS)
        QtCore.QMetaObject.connectSlotsByName(instalarROS)

    def retranslateUi(self, instalarROS):
        instalarROS.setWindowTitle(_translate("instalarROS", "Instalar ROS", None))
        self.label.setText(_translate("instalarROS", "Instalar y configurar ROS", None))
        self.textBrowser.setHtml(_translate("instalarROS", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Ubuntu\'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">$ sudo apt-get update</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">$ sudo apt-get upgrade</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">$ sudo sh -c \'echo &quot;deb http://packages.ros.org/ros/ubuntu trusty main&quot; &gt;  /etc/apt/sources.list.d/ros-latest.list\'</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">$ wget http://packages.ros.org/ros.key -O - | sudo apt-key add -</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">$ sudo apt-get update</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">$ sudo apt-get install ros-indigo-desktop-full python-rosinstall</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">$ sudo rosdep init</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">$ rosdep update</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">$ echo &quot;source /opt/ros/indigo/setup.bash&quot; &gt;&gt; ~/.bashrc</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">$ source ~/.bashrc</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">$ mkdir -p ~/catkin_ws/src</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">$ cd ~/catkin_ws/src</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">$ catkin_init_workspace</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">$ cd ~/catkin_ws/</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">$ catkin_make</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">$ source ~/catkin_ws/devel/setup.bash</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">$ echo &quot;PS1=\'\\[\\e[1;34m\\]\\u\\[\\e[m\\]@\\[\\e[1;32m\\]\\h\\[\\e[m\\] \\[\\e[1m\\]\\W\\[\\e[m\\]\\$ \'&quot; &gt;&gt; ~/.bashrc</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">$ echo &quot;export ROS_WORKSPACE=~/catkin_ws&quot; &gt;&gt; ~/.myenvironmentvariables</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">$ echo &quot;source ~/catkin_ws/devel/setup.bash&quot; &gt;&gt; ~/.myenvironmentvariables</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">$ echo &quot;export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:~/catkin_ws&quot; &gt;&gt; ~/.myenvironmentvariables</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">$ echo &quot;source ~/.myenvironmentvariables&quot; &gt;&gt; ~/.bashrc</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">$ sudo sudo</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">$ echo &quot;source ~/.myenvironmentvariables&quot; &gt;&gt; /etc/X11/Xsession.d/40x11-common_xsessionrc</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">$ exit</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">$ source /etc/X11/Xsession.d/40x11-common_xsessionrc</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">$ source ~/.myenvironmentvariables</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">$ source ~/.bashrc</p></body></html>", None))
        self.botonAceptar.setText(_translate("instalarROS", "Aceptar", None))

