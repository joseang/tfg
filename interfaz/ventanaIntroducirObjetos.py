# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ventanaIntroducirObjetos.ui'
#
# Created: Tue Aug 23 11:04:47 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_IntroducirObjetos(object):
    def setupUi(self, IntroducirObjetos):
        IntroducirObjetos.setObjectName(_fromUtf8("IntroducirObjetos"))
        IntroducirObjetos.resize(393, 375)
        IntroducirObjetos.setMinimumSize(QtCore.QSize(393, 375))
        IntroducirObjetos.setMaximumSize(QtCore.QSize(393, 375))
        self.botonAnadir = QtGui.QPushButton(IntroducirObjetos)
        self.botonAnadir.setGeometry(QtCore.QRect(140, 330, 98, 27))
        self.botonAnadir.setMinimumSize(QtCore.QSize(98, 27))
        self.botonAnadir.setMaximumSize(QtCore.QSize(98, 27))
        self.botonAnadir.setObjectName(_fromUtf8("botonAnadir"))
        self.verticalLayoutWidget_3 = QtGui.QWidget(IntroducirObjetos)
        self.verticalLayoutWidget_3.setGeometry(QtCore.QRect(100, 10, 183, 165))
        self.verticalLayoutWidget_3.setObjectName(_fromUtf8("verticalLayoutWidget_3"))
        self.verticalLayout_6 = QtGui.QVBoxLayout(self.verticalLayoutWidget_3)
        self.verticalLayout_6.setMargin(0)
        self.verticalLayout_6.setObjectName(_fromUtf8("verticalLayout_6"))
        self.listaObjetosSensores = QtGui.QComboBox(self.verticalLayoutWidget_3)
        self.listaObjetosSensores.setObjectName(_fromUtf8("listaObjetosSensores"))
        self.listaObjetosSensores.addItem(_fromUtf8(""))
        self.listaObjetosSensores.addItem(_fromUtf8(""))
        self.listaObjetosSensores.addItem(_fromUtf8(""))
        self.listaObjetosSensores.addItem(_fromUtf8(""))
        self.listaObjetosSensores.addItem(_fromUtf8(""))
        self.listaObjetosSensores.addItem(_fromUtf8(""))
        self.listaObjetosSensores.addItem(_fromUtf8(""))
        self.verticalLayout_6.addWidget(self.listaObjetosSensores)
        self.horizontalLayout_12 = QtGui.QHBoxLayout()
        self.horizontalLayout_12.setObjectName(_fromUtf8("horizontalLayout_12"))
        self.verticalLayout_7 = QtGui.QVBoxLayout()
        self.verticalLayout_7.setObjectName(_fromUtf8("verticalLayout_7"))
        self.label_20 = QtGui.QLabel(self.verticalLayoutWidget_3)
        self.label_20.setMinimumSize(QtCore.QSize(40, 25))
        self.label_20.setMaximumSize(QtCore.QSize(40, 25))
        self.label_20.setAlignment(QtCore.Qt.AlignCenter)
        self.label_20.setObjectName(_fromUtf8("label_20"))
        self.verticalLayout_7.addWidget(self.label_20)
        self.label_21 = QtGui.QLabel(self.verticalLayoutWidget_3)
        self.label_21.setMinimumSize(QtCore.QSize(60, 25))
        self.label_21.setMaximumSize(QtCore.QSize(60, 25))
        self.label_21.setAlignment(QtCore.Qt.AlignCenter)
        self.label_21.setObjectName(_fromUtf8("label_21"))
        self.verticalLayout_7.addWidget(self.label_21)
        self.label_22 = QtGui.QLabel(self.verticalLayoutWidget_3)
        self.label_22.setMinimumSize(QtCore.QSize(40, 25))
        self.label_22.setMaximumSize(QtCore.QSize(40, 25))
        self.label_22.setAlignment(QtCore.Qt.AlignCenter)
        self.label_22.setObjectName(_fromUtf8("label_22"))
        self.verticalLayout_7.addWidget(self.label_22)
        self.label_19 = QtGui.QLabel(self.verticalLayoutWidget_3)
        self.label_19.setMinimumSize(QtCore.QSize(65, 25))
        self.label_19.setMaximumSize(QtCore.QSize(65, 25))
        self.label_19.setAlignment(QtCore.Qt.AlignCenter)
        self.label_19.setObjectName(_fromUtf8("label_19"))
        self.verticalLayout_7.addWidget(self.label_19)
        self.horizontalLayout_12.addLayout(self.verticalLayout_7)
        self.verticalLayout_8 = QtGui.QVBoxLayout()
        self.verticalLayout_8.setObjectName(_fromUtf8("verticalLayout_8"))
        self.listaColores = QtGui.QComboBox(self.verticalLayoutWidget_3)
        self.listaColores.setMinimumSize(QtCore.QSize(80, 27))
        self.listaColores.setMaximumSize(QtCore.QSize(50, 27))
        self.listaColores.setObjectName(_fromUtf8("listaColores"))
        self.listaColores.addItem(_fromUtf8(""))
        self.listaColores.addItem(_fromUtf8(""))
        self.listaColores.addItem(_fromUtf8(""))
        self.listaColores.addItem(_fromUtf8(""))
        self.listaColores.addItem(_fromUtf8(""))
        self.listaColores.addItem(_fromUtf8(""))
        self.listaColores.addItem(_fromUtf8(""))
        self.verticalLayout_8.addWidget(self.listaColores)
        self.numeroObjeto = QtGui.QLineEdit(self.verticalLayoutWidget_3)
        self.numeroObjeto.setMinimumSize(QtCore.QSize(55, 29))
        self.numeroObjeto.setMaximumSize(QtCore.QSize(55, 29))
        self.numeroObjeto.setObjectName(_fromUtf8("numeroObjeto"))
        self.verticalLayout_8.addWidget(self.numeroObjeto)
        self.listaPiezasAlimentador = QtGui.QComboBox(self.verticalLayoutWidget_3)
        self.listaPiezasAlimentador.setMinimumSize(QtCore.QSize(85, 27))
        self.listaPiezasAlimentador.setMaximumSize(QtCore.QSize(85, 27))
        self.listaPiezasAlimentador.setObjectName(_fromUtf8("listaPiezasAlimentador"))
        self.listaPiezasAlimentador.addItem(_fromUtf8(""))
        self.listaPiezasAlimentador.addItem(_fromUtf8(""))
        self.listaPiezasAlimentador.addItem(_fromUtf8(""))
        self.verticalLayout_8.addWidget(self.listaPiezasAlimentador)
        self.cantidadAlimentador = QtGui.QLineEdit(self.verticalLayoutWidget_3)
        self.cantidadAlimentador.setMinimumSize(QtCore.QSize(50, 25))
        self.cantidadAlimentador.setMaximumSize(QtCore.QSize(50, 25))
        self.cantidadAlimentador.setObjectName(_fromUtf8("cantidadAlimentador"))
        self.verticalLayout_8.addWidget(self.cantidadAlimentador)
        self.horizontalLayout_12.addLayout(self.verticalLayout_8)
        self.verticalLayout_6.addLayout(self.horizontalLayout_12)
        self.label_4 = QtGui.QLabel(IntroducirObjetos)
        self.label_4.setGeometry(QtCore.QRect(10, 190, 101, 20))
        self.label_4.setAlignment(QtCore.Qt.AlignCenter)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.verticalLayoutWidget = QtGui.QWidget(IntroducirObjetos)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(20, 220, 82, 101))
        self.verticalLayoutWidget.setObjectName(_fromUtf8("verticalLayoutWidget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setMargin(0)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.label_3 = QtGui.QLabel(self.verticalLayoutWidget)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.horizontalLayout_3.addWidget(self.label_3)
        self.posicionX = QtGui.QLineEdit(self.verticalLayoutWidget)
        self.posicionX.setMinimumSize(QtCore.QSize(61, 27))
        self.posicionX.setMaximumSize(QtCore.QSize(61, 27))
        self.posicionX.setObjectName(_fromUtf8("posicionX"))
        self.horizontalLayout_3.addWidget(self.posicionX)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.label_2 = QtGui.QLabel(self.verticalLayoutWidget)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.horizontalLayout_2.addWidget(self.label_2)
        self.posicionY = QtGui.QLineEdit(self.verticalLayoutWidget)
        self.posicionY.setMinimumSize(QtCore.QSize(61, 27))
        self.posicionY.setMaximumSize(QtCore.QSize(61, 27))
        self.posicionY.setObjectName(_fromUtf8("posicionY"))
        self.horizontalLayout_2.addWidget(self.posicionY)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label = QtGui.QLabel(self.verticalLayoutWidget)
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout.addWidget(self.label)
        self.posicionZ = QtGui.QLineEdit(self.verticalLayoutWidget)
        self.posicionZ.setMinimumSize(QtCore.QSize(61, 27))
        self.posicionZ.setMaximumSize(QtCore.QSize(61, 27))
        self.posicionZ.setObjectName(_fromUtf8("posicionZ"))
        self.horizontalLayout.addWidget(self.posicionZ)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.label_5 = QtGui.QLabel(IntroducirObjetos)
        self.label_5.setGeometry(QtCore.QRect(130, 190, 101, 17))
        self.label_5.setAlignment(QtCore.Qt.AlignCenter)
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.verticalLayoutWidget_2 = QtGui.QWidget(IntroducirObjetos)
        self.verticalLayoutWidget_2.setGeometry(QtCore.QRect(130, 220, 106, 101))
        self.verticalLayoutWidget_2.setObjectName(_fromUtf8("verticalLayoutWidget_2"))
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.verticalLayoutWidget_2)
        self.verticalLayout_2.setMargin(0)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.horizontalLayout_4 = QtGui.QHBoxLayout()
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))
        self.label_6 = QtGui.QLabel(self.verticalLayoutWidget_2)
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.horizontalLayout_4.addWidget(self.label_6)
        self.orientacionYaw = QtGui.QLineEdit(self.verticalLayoutWidget_2)
        self.orientacionYaw.setMinimumSize(QtCore.QSize(61, 27))
        self.orientacionYaw.setMaximumSize(QtCore.QSize(61, 27))
        self.orientacionYaw.setObjectName(_fromUtf8("orientacionYaw"))
        self.horizontalLayout_4.addWidget(self.orientacionYaw)
        self.verticalLayout_2.addLayout(self.horizontalLayout_4)
        self.horizontalLayout_5 = QtGui.QHBoxLayout()
        self.horizontalLayout_5.setObjectName(_fromUtf8("horizontalLayout_5"))
        self.label_7 = QtGui.QLabel(self.verticalLayoutWidget_2)
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.horizontalLayout_5.addWidget(self.label_7)
        self.orientacionPitch = QtGui.QLineEdit(self.verticalLayoutWidget_2)
        self.orientacionPitch.setMinimumSize(QtCore.QSize(61, 27))
        self.orientacionPitch.setMaximumSize(QtCore.QSize(61, 27))
        self.orientacionPitch.setObjectName(_fromUtf8("orientacionPitch"))
        self.horizontalLayout_5.addWidget(self.orientacionPitch)
        self.verticalLayout_2.addLayout(self.horizontalLayout_5)
        self.horizontalLayout_6 = QtGui.QHBoxLayout()
        self.horizontalLayout_6.setObjectName(_fromUtf8("horizontalLayout_6"))
        self.label_8 = QtGui.QLabel(self.verticalLayoutWidget_2)
        self.label_8.setObjectName(_fromUtf8("label_8"))
        self.horizontalLayout_6.addWidget(self.label_8)
        self.orientacionRoll = QtGui.QLineEdit(self.verticalLayoutWidget_2)
        self.orientacionRoll.setMinimumSize(QtCore.QSize(61, 27))
        self.orientacionRoll.setMaximumSize(QtCore.QSize(61, 27))
        self.orientacionRoll.setObjectName(_fromUtf8("orientacionRoll"))
        self.horizontalLayout_6.addWidget(self.orientacionRoll)
        self.verticalLayout_2.addLayout(self.horizontalLayout_6)
        self.label_9 = QtGui.QLabel(IntroducirObjetos)
        self.label_9.setGeometry(QtCore.QRect(260, 190, 101, 17))
        self.label_9.setAlignment(QtCore.Qt.AlignCenter)
        self.label_9.setObjectName(_fromUtf8("label_9"))
        self.verticalLayoutWidget_4 = QtGui.QWidget(IntroducirObjetos)
        self.verticalLayoutWidget_4.setGeometry(QtCore.QRect(260, 220, 114, 101))
        self.verticalLayoutWidget_4.setObjectName(_fromUtf8("verticalLayoutWidget_4"))
        self.verticalLayout_3 = QtGui.QVBoxLayout(self.verticalLayoutWidget_4)
        self.verticalLayout_3.setMargin(0)
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.horizontalLayout_7 = QtGui.QHBoxLayout()
        self.horizontalLayout_7.setObjectName(_fromUtf8("horizontalLayout_7"))
        self.textoTamano1 = QtGui.QLabel(self.verticalLayoutWidget_4)
        self.textoTamano1.setObjectName(_fromUtf8("textoTamano1"))
        self.horizontalLayout_7.addWidget(self.textoTamano1)
        self.valorTamano1 = QtGui.QLineEdit(self.verticalLayoutWidget_4)
        self.valorTamano1.setMinimumSize(QtCore.QSize(55, 27))
        self.valorTamano1.setMaximumSize(QtCore.QSize(55, 27))
        self.valorTamano1.setObjectName(_fromUtf8("valorTamano1"))
        self.horizontalLayout_7.addWidget(self.valorTamano1)
        self.verticalLayout_3.addLayout(self.horizontalLayout_7)
        self.horizontalLayout_8 = QtGui.QHBoxLayout()
        self.horizontalLayout_8.setObjectName(_fromUtf8("horizontalLayout_8"))
        self.textoTamano2 = QtGui.QLabel(self.verticalLayoutWidget_4)
        self.textoTamano2.setObjectName(_fromUtf8("textoTamano2"))
        self.horizontalLayout_8.addWidget(self.textoTamano2)
        self.valorTamano2 = QtGui.QLineEdit(self.verticalLayoutWidget_4)
        self.valorTamano2.setMinimumSize(QtCore.QSize(55, 27))
        self.valorTamano2.setMaximumSize(QtCore.QSize(55, 27))
        self.valorTamano2.setObjectName(_fromUtf8("valorTamano2"))
        self.horizontalLayout_8.addWidget(self.valorTamano2)
        self.verticalLayout_3.addLayout(self.horizontalLayout_8)
        self.horizontalLayout_9 = QtGui.QHBoxLayout()
        self.horizontalLayout_9.setObjectName(_fromUtf8("horizontalLayout_9"))
        self.textoTamano3 = QtGui.QLabel(self.verticalLayoutWidget_4)
        self.textoTamano3.setObjectName(_fromUtf8("textoTamano3"))
        self.horizontalLayout_9.addWidget(self.textoTamano3)
        self.valorTamano3 = QtGui.QLineEdit(self.verticalLayoutWidget_4)
        self.valorTamano3.setMinimumSize(QtCore.QSize(55, 27))
        self.valorTamano3.setMaximumSize(QtCore.QSize(55, 27))
        self.valorTamano3.setObjectName(_fromUtf8("valorTamano3"))
        self.horizontalLayout_9.addWidget(self.valorTamano3)
        self.verticalLayout_3.addLayout(self.horizontalLayout_9)

        self.retranslateUi(IntroducirObjetos)
        QtCore.QMetaObject.connectSlotsByName(IntroducirObjetos)
        IntroducirObjetos.setTabOrder(self.posicionX, self.posicionY)
        IntroducirObjetos.setTabOrder(self.posicionY, self.posicionZ)
        IntroducirObjetos.setTabOrder(self.posicionZ, self.orientacionYaw)
        IntroducirObjetos.setTabOrder(self.orientacionYaw, self.orientacionPitch)
        IntroducirObjetos.setTabOrder(self.orientacionPitch, self.orientacionRoll)
        IntroducirObjetos.setTabOrder(self.orientacionRoll, self.botonAnadir)

    def retranslateUi(self, IntroducirObjetos):
        IntroducirObjetos.setWindowTitle(_translate("IntroducirObjetos", "Introducir Objetos", None))
        self.botonAnadir.setText(_translate("IntroducirObjetos", "Añadir", None))
        self.listaObjetosSensores.setItemText(0, _translate("IntroducirObjetos", "Caja", None))
        self.listaObjetosSensores.setItemText(1, _translate("IntroducirObjetos", "Cilindro", None))
        self.listaObjetosSensores.setItemText(2, _translate("IntroducirObjetos", "Esfera", None))
        self.listaObjetosSensores.setItemText(3, _translate("IntroducirObjetos", "Sensor de presencia", None))
        self.listaObjetosSensores.setItemText(4, _translate("IntroducirObjetos", "Sensor de contacto", None))
        self.listaObjetosSensores.setItemText(5, _translate("IntroducirObjetos", "Alimentador de piezas", None))
        self.listaObjetosSensores.setItemText(6, _translate("IntroducirObjetos", "Cámara", None))
        self.label_20.setText(_translate("IntroducirObjetos", "Color:", None))
        self.label_21.setText(_translate("IntroducirObjetos", "Número:", None))
        self.label_22.setText(_translate("IntroducirObjetos", "Pieza:", None))
        self.label_19.setText(_translate("IntroducirObjetos", "Cantidad:", None))
        self.listaColores.setItemText(0, _translate("IntroducirObjetos", "Rojo", None))
        self.listaColores.setItemText(1, _translate("IntroducirObjetos", "Azul", None))
        self.listaColores.setItemText(2, _translate("IntroducirObjetos", "Verde", None))
        self.listaColores.setItemText(3, _translate("IntroducirObjetos", "Amarillo", None))
        self.listaColores.setItemText(4, _translate("IntroducirObjetos", "Blanco", None))
        self.listaColores.setItemText(5, _translate("IntroducirObjetos", "Negro", None))
        self.listaColores.setItemText(6, _translate("IntroducirObjetos", "Gris", None))
        self.listaPiezasAlimentador.setItemText(0, _translate("IntroducirObjetos", "Caja", None))
        self.listaPiezasAlimentador.setItemText(1, _translate("IntroducirObjetos", "Cilindro", None))
        self.listaPiezasAlimentador.setItemText(2, _translate("IntroducirObjetos", "Esfera", None))
        self.label_4.setText(_translate("IntroducirObjetos", "Posición [mm]", None))
        self.label_3.setText(_translate("IntroducirObjetos", "X", None))
        self.label_2.setText(_translate("IntroducirObjetos", "Y", None))
        self.label.setText(_translate("IntroducirObjetos", "Z", None))
        self.label_5.setText(_translate("IntroducirObjetos", "Orientación [°]", None))
        self.label_6.setText(_translate("IntroducirObjetos", "Yaw", None))
        self.label_7.setText(_translate("IntroducirObjetos", "Pitch", None))
        self.label_8.setText(_translate("IntroducirObjetos", "Roll", None))
        self.label_9.setText(_translate("IntroducirObjetos", "Tamaño [mm]", None))
        self.textoTamano1.setText(_translate("IntroducirObjetos", "Alto", None))
        self.textoTamano2.setText(_translate("IntroducirObjetos", "Largo", None))
        self.textoTamano3.setText(_translate("IntroducirObjetos", "Ancho", None))

