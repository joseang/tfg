#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys, rospy, thread, threading
from interfaz.ventanaMostrarSubrutina import *
from geometry_msgs.msg import Pose
from gazebo_msgs.srv import SpawnModel
from cinematica.transformations import quaternion_about_axis, quaternion_multiply
import random
import numpy as np
from modulos.comprobarArchivo import *
from rosgraph_msgs.msg import Log

from path_absoluto import obtenerDireccionAbsoluta
direccionAbsoluta=obtenerDireccionAbsoluta()

class mostrarSubrutina(QtGui.QDialog):
	def __init__(self,parent=None, nombreSubrutina=""):
		QtGui.QWidget.__init__(self, parent,QtCore.Qt.WindowStaysOnTopHint)
		self.ui = Ui_mostrarSubrutina()
		self.ui.setupUi(self)
		self.nombreSubrutina=nombreSubrutina
		self.listaOrdenes=[]
		self.listaInfo=[]
		self.ui.tablaMostrarSubrutina.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
		self.ui.tablaMostrarSubrutina.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
		self.establecerAnchoDeColumnas()

	def establecerAnchoDeColumnas(self):
		self.ui.tablaMostrarSubrutina.setColumnWidth(0,280)

	@QtCore.pyqtSlot(list)
	def receptorSenalOrdenSubrutina(self, listaRecibida):
		pass

	def marcarLineaAEjecutar(self, indice):
		numeroItems=self.ui.tablaMostrarSubrutina.rowCount()
		for i in range(numeroItems):
			itemPosible=self.ui.tablaMostrarSubrutina.item(0,i)
			if self.ui.tablaMostrarSubrutina.isItemSelected(itemPosible):
				self.ui.tablaMostrarSubrutina.setItemSelected(itemPosible,False)
		itemActual=self.ui.tablaMostrarSubrutina.item(0,indice)
		self.ui.tablaMostrarSubrutina.setItemSelected(itemActual,True)

	def borrarTabla(self):
		self.ui.tablaMostrarSubrutina.setRowCount(0)
		self.ui.tablaMostrarSubrutina.setRowCount(1)

	def cargarOrdenes(self, nombreSubrutina):
		direccionArchivo=direccionAbsoluta+'/data/subrutina_'+nombreSubrutina
		archivo=open(direccionArchivo,'r')
		lineasArchivo=archivo.readlines()
		archivo.close()
		self.cargarOrdenesEnTabla(lineasArchivo)

	def cargarOrdenesEnTabla(self, lineasArchivo):
		indiceIntroducirTabla=0
		ok=False
		for i in range(len(lineasArchivo)):
			contenidoLinea=lineasArchivo[i]
			contenidoLinea=contenidoLinea.split()
			if contenidoLinea[0]=="ABRIR_PINZA":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarAbrirPinza(contenidoLinea)
			elif contenidoLinea[0]=="CERRAR_PINZA":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarCerrarPinza(contenidoLinea)
			elif contenidoLinea[0]=="IR":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarIr(contenidoLinea)
			elif contenidoLinea[0]=="TR_ARTICULACIONES":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarTrayectoriaArticulaciones(contenidoLinea)
			elif contenidoLinea[0]=="TR_XYZ":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarTrayectoriaCartesiana(contenidoLinea)
			#elif contenidoLinea[0]=="ARTICULACIONES":
			#	[ok, datosAGuardar, mensajeAMostrar]=comprobarIrArticulaciones(contenidoLinea)
			#elif contenidoLinea[0]=="XYZ":
			#	[ok, datosAGuardar, mensajeAMostrar]=comprobarIrXYZ(contenidoLinea)
			elif contenidoLinea[0]=="LINEAL":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarLineal(contenidoLinea)
			elif contenidoLinea[0]=="CIRCULAR":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarCircular(contenidoLinea)
			elif contenidoLinea[0]=="MEMORIZAR":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarMemorizar(contenidoLinea)
			elif contenidoLinea[0]=="ESPERAR":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarEsperar(contenidoLinea)
			elif contenidoLinea[0]=="SALTAR":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarSaltar(contenidoLinea)
			elif contenidoLinea[0]=="COMENTARIO":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarComentario(contenidoLinea)
			elif contenidoLinea[0]=="ETIQUETA":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarEtiqueta(contenidoLinea)
			else:
				ok=False
			if ok:
				self.guardarOrden([contenidoLinea[0], datosAGuardar])
				self.mostrarMensaje(mensajeAMostrar, indiceIntroducirTabla)
				indiceIntroducirTabla+=1
			if ok==False:
				return False
		return [True, indiceIntroducirTabla]


	def guardarOrden(self, datos):
		self.listaOrdenes.append(datos[0])
		self.listaInfo.append(datos[1])


	def cargarTabla(self):
		indice=0
		ok=False
		for i in range(len(self.listaOrdenes)):
			nombreOrden=self.listaOrdenes[i]
			infoOrden=self.listaInfo[i]
			contenidoOrden=[self.listaOrdenes[i], self.listaInfo[i]]
			if nombreOrden=="ABRIR_PINZA":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarAbrirPinza(contenidoOrden)
			elif nombreOrden=="CERRAR_PINZA":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarCerrarPinza(contenidoOrden)
			elif nombreOrden=="IR":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarIr(contenidoOrden)
			elif nombreOrden=="TR_ARTICULACIONES":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarTrayectoriaArticulaciones(contenidoLinea)
			elif nombreOrden=="TR_XYZ":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarTrayectoriaCartesiana(contenidoLinea)
			elif nombreOrden=="LINEAL":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarLineal(contenidoOrden)
			elif nombreOrden=="CIRCULAR":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarCircular(contenidoOrden)
			elif nombreOrden=="MEMORIZAR":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarMemorizar(contenidoOrden)
			elif nombreOrden=="ESPERAR":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarEsperar(contenidoOrden)
			elif nombreOrden=="COMENTARIO":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarComentario(contenidoOrden)
			else:
				ok=False
			if ok:
				self.mostrarMensaje(mensajeAMostrar, indice)
				indice+=1
			if ok==False:
				return False

	def mostrarMensaje(self, texto, indice):
		valorCasillaMensaje=QtGui.QTableWidgetItem(texto)
		self.ui.tablaMostrarSubrutina.setItem(indice,0,valorCasillaMensaje)
		self.ui.tablaMostrarSubrutina.insertRow(indice+1)



if __name__ == '__main__':
	rospy.init_node('mostrarSubrutina')
	app = QtGui.QApplication(sys.argv)
	ventanamostrarSubrutina = mostrarSubrutina()
	ventanamostrarSubrutina.show()
	sys.exit(app.exec_())
