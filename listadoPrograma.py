#!/usr/bin/env python
# -*- coding: utf-8 -*-
from interfaz.ventanaListadoPrograma import *
from cinematica.kinematic_functions import *
import sys, os, time, thread, threading, rospy
from PyQt4 import QtCore, QtGui
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler, FileSystemEventHandler
from PyQt4.QtCore import QObject, pyqtSignal, pyqtSlot
from modulos.funciones import *
from modulos.comprobarArchivo import *
from modulos.guardarEnArchivo import *
from modulos.crearMensaje import *
from modulos.ejecutarOrdenes import *
from modulos.obtenerDatosComandos import *
from mostrarSubrutina import *
import numpy as np

from path_absoluto import obtenerDireccionAbsoluta
direccionAbsoluta=obtenerDireccionAbsoluta()

class listadoPrograma(QtGui.QDialog):
	procDone=QtCore.pyqtSignal(str)
	variableQueRecibe=QtCore.pyqtSignal(int)
	envioListaPosicionesATabla = QtCore.pyqtSignal(list)
	envioSenalArchivoCargado=QtCore.pyqtSignal(bool)
	envioSenalOrdenSubrutina=QtCore.pyqtSignal(list)
	def __init__(self,parent=None, ventanaMostrarSubrutina=""):
		global indiceTabla, hayDireccion, listaOrdenes, listaInfo
		QtCore.QCoreApplication.setAttribute(QtCore.Qt.AA_X11InitThreads)
		QtGui.QWidget.__init__(self,parent,QtCore.Qt.WindowStaysOnTopHint)
		self.ui=Ui_Programa()
		self.ui.setupUi(self)
		self.move(0,124)

		global objeto_linea
		objeto_linea=claseEnviarInt()
		objeto_linea.senal.connect(self.recibirOrdenFinalizada)

		self.ordenesSubrutinaEjecutadas=0
		self.listaInfoSub=[]
		self.listaOrdenesSub=[]
		self.ventanaMostrarSubrutina=ventanaMostrarSubrutina
		self.subrutina_iniciada=False

		self.ejecutandoseCiclo=False
		self.ejecutandoseLinea=False

		self.stop=False

		self.variables={}
		self.reiniciarInterrupciones()
		indiceTabla=0
		hayDireccion=False
		listaOrdenes=[]
		listaInfo=[]
		self.connect(self.ui.tablaOrdenes, QtCore.SIGNAL("itemDoubleClicked(QTableWidgetItem*)"), self.dobleClickOrdenes)
		self.ui.tablaOrdenes.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
		self.ui.tablaOrdenes.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
		self.connect(self.ui.botonBorrar, QtCore.SIGNAL('clicked()'), self.botonBorrarOrdenes)
		self.connect(self.ui.botonGuardar, QtCore.SIGNAL('clicked()'), self.botonGuardar)
		self.connect(self.ui.botonGuardarComo, QtCore.SIGNAL('clicked()'), self.botonGuardarComo)
		self.connect(self.ui.botonAbrir, QtCore.SIGNAL('clicked()'), self.botonAbrirPrograma)
		self.connect(self.ui.botonLinea, QtCore.SIGNAL('clicked()'), self.ejecutarLinea)
		self.connect(self.ui.botonCiclo, QtCore.SIGNAL('clicked()'), self.ejecutarCiclo)
		self.connect(self.ui.botonStop, QtCore.SIGNAL('clicked()'), self.stopOrdenes)
		# Variable GLOBALES
		# listaOrdenes, listaInfo, indiceTabla, hayDireccion, direccionGuardar


	def reiniciarTodo(self):
		self.borrarTablaCompleta()
		borrarTodo()
		self.variables={}
		self.reiniciarInterrupciones()

	''' boton Abrir '''
	def botonAbrirPrograma(self):
		global listaOrdenes, listaInfo, indiceTabla, hayDireccion, direccionGuardar
		direccionArchivo = QtGui.QFileDialog.getOpenFileName(
			self,
			"Cargar archivo",
			"~/",
			'*.txt')
		if direccionArchivo=="":
			return
		lineasArchivo=self.cargarLineasArchivo(direccionArchivo)
		self.reiniciarTodo()
		if self.cargarOrdenesEnTabla(lineasArchivo):
			hayDireccion=True
			direccionGuardar=direccionArchivo
		else:
			self.borrarTablaCompleta()
			QtGui.QMessageBox.about(self, 'Error','El archivo no es valido')

	def cargarLineasArchivo(self,direccionArchivo):
		archivo=open(direccionArchivo,'r')
		lineas=archivo.readlines()
		lineas_aux=[]
		for i in lineas:
			lineas_aux.append(i.rstrip())
		return lineas_aux

	def cargarOrdenesEnTabla(self,lineasArchivo):
		global listaInfo, listaOrdenes
		listaInfo, listaOrdenes = [], []
		indiceIntroducirTabla=0
		[ok, ok_posicion, ok_subrutina]=[False, False, False]
		listaPosiciones=[]
		for i in range(len(lineasArchivo)):
			contenidoLinea=lineasArchivo[i]
			contenidoLinea=contenidoLinea.split()
			if contenidoLinea[0]=="ABRIR_PINZA":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarAbrirPinza(contenidoLinea)
			elif contenidoLinea[0]=="CERRAR_PINZA":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarCerrarPinza(contenidoLinea)
			elif contenidoLinea[0]=="IR":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarIr(contenidoLinea)
			elif contenidoLinea[0]=="TR_ARTICULACIONES":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarTrayectoriaArticulaciones(contenidoLinea)
			elif contenidoLinea[0]=="TR_XYZ":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarTrayectoriaCartesiana(contenidoLinea)
			#elif contenidoLinea[0]=="ARTICULACIONES":
			#	[ok, datosAGuardar, mensajeAMostrar]=comprobarIrArticulaciones(contenidoLinea)
			#elif contenidoLinea[0]=="XYZ":
			#	[ok, datosAGuardar, mensajeAMostrar]=comprobarIrXYZ(contenidoLinea)
			elif contenidoLinea[0]=="LINEAL":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarLineal(contenidoLinea)
			elif contenidoLinea[0]=="CIRCULAR":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarCircular(contenidoLinea)
			elif contenidoLinea[0]=="MEMORIZAR":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarMemorizar(contenidoLinea)
			elif contenidoLinea[0]=="ESPERAR":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarEsperar(contenidoLinea)
			elif contenidoLinea[0]=="SALTAR":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarSaltar(contenidoLinea)
			elif contenidoLinea[0]=="SALTAR_SI":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarSaltarSi(contenidoLinea)
			elif contenidoLinea[0]=="COMENTARIO":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarComentario(contenidoLinea)
			elif contenidoLinea[0]=="VARIABLE":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarVariable(contenidoLinea)
			elif contenidoLinea[0]=="LLAMAR_SUBRUTINA":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarLlamarSubrutina(contenidoLinea)
			elif contenidoLinea[0]=="ETIQUETA":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarEtiqueta(contenidoLinea)
			elif contenidoLinea[0]=="COMPROBAR_SENSOR":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarSiSensor(contenidoLinea)
			elif contenidoLinea[0]=="INTERRUPCION":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarInterrupcion(contenidoLinea)
			elif contenidoLinea[0]=="NOMBRE":
				nombreSubrutina=contenidoLinea[1]
				indiceFinal=lineasArchivo.index("FIN SUBRUTINA "+nombreSubrutina)
				lineasSubrutina=lineasArchivo[i+1:indiceFinal]
				self.cargarSubrutinaDesdeArchivoGeneral(nombreSubrutina,lineasSubrutina)
				ok_subrutina=True
				ok=False
			else:
				ok=False
			if contenidoLinea[0]=="POSICION":
				[ok_posicion, numeroPosicion, valorPosicion]=comprobarPosicion(contenidoLinea)
			if ok and ok_subrutina==False:
				nombre_orden=contenidoLinea[0]
				self.guardarOrden([nombre_orden, datosAGuardar])
				self.mostrarMensaje(mensajeAMostrar, indiceIntroducirTabla)
				indiceIntroducirTabla+=1
			if ok_posicion:
				listaPosiciones.append([numeroPosicion, valorPosicion])
				archivoPosiciones=open(direccionAbsoluta+'/posiciones/P'+str(numeroPosicion)+'.txt','w+')
				archivoPosiciones.write(' '.join(str(e) for e in valorPosicion))
				archivoPosiciones.close()
			if ok==False and ok_posicion==False and ok_subrutina==False:
				return False
			if ok_subrutina==True:
				ok_subrutina==False
		self.enviarListaPosicionesATabla(listaPosiciones)
		return True

	def cargarSubrutinaDesdeArchivoGeneral(self,nombreSubrutina,lineas):
		archivo=open(direccionAbsoluta+'/data/subrutina_'+str(nombreSubrutina),'w+')
		for i in range(len(lineas)):
			contenidoLinea=lineas[i]
			contenidoLinea=contenidoLinea.split()
			if contenidoLinea[0]=="ABRIR_PINZA":
				archivo.write(lineas[i]+"\n")
			elif contenidoLinea[0]=="CERRAR_PINZA":
				archivo.write(lineas[i]+"\n")
			elif contenidoLinea[0]=="IR":
				archivo.write(lineas[i]+"\n")
			elif contenidoLinea[0]=="TR_ARTICULACIONES":
				archivo.write(lineas[i]+"\n")
			elif contenidoLinea[0]=="TR_XYZ":
				archivo.write(lineas[i]+"\n")
			elif contenidoLinea[0]=="ARTICULACIONES":
				archivo.write(lineas[i]+"\n")
			elif contenidoLinea[0]=="XYZ":
				archivo.write(lineas[i]+"\n")
			elif contenidoLinea[0]=="LINEAL":
				archivo.write(lineas[i]+"\n")
			elif contenidoLinea[0]=="CIRCULAR":
				archivo.write(lineas[i]+"\n")
			elif contenidoLinea[0]=="MEMORIZAR":
				archivo.write(lineas[i]+"\n")
			elif contenidoLinea[0]=="ESPERAR":
				archivo.write(lineas[i]+"\n")
			elif contenidoLinea[0]=="COMENTARIO":
				archivo.write(lineas[i]+"\n")
		archivo.close()
		return True

	def mostrarMensaje(self, texto, indice):
		valorCasillaMensaje=QtGui.QTableWidgetItem(texto)
		self.ui.tablaOrdenes.setItem(indice,0,valorCasillaMensaje)
		self.ui.tablaOrdenes.insertRow(indice+1)

	def guardarOrden(self,datos,indice=None):
		global listaOrdenes, listaInfo
		if indice==None:
			listaOrdenes.append(datos[0])
			listaInfo.append(datos[1])
		else:
			listaOrdenes.insert(indice+1,datos[0])
			listaInfo.insert(indice+1,datos[1])

	def enviarListaPosicionesATabla(self, listaPosicionesEnviar):
		self.envioListaPosicionesATabla.emit(listaPosicionesEnviar)
		self.enviarSenalArchivoCargado()

	def enviarSenalArchivoCargado(self):
		self.envioSenalArchivoCargado.emit(True)

	def actualizarTablaOrdenes(self):
		global listaOrdenes, listaInfo
		self.guardarEnArchivoTemporal()
		self.borrarTablaCompleta()
		self.cargarArchivoTemporal()

	def dobleClickOrdenes(self, item):
		global listaOrdenes, listaInfo
		indice=item.row()
		if listaOrdenes!=[]:
			orden=listaOrdenes[indice]
			info=listaInfo[indice]
			if orden=="IR":
				ventanaIr=ordenIr(listaInfo=info)
				if ventanaIr.exec_():
					datosInfo=ventanaIr.obtenerDatos()
					listaInfo[indice]=datosInfo
					ventanaIr.accept()
					self.actualizarTablaOrdenes()
				else:
					ventanaIr.close()
			elif orden=="TR_ARTICULACIONES":
				ventanaTrayectoriaArticulaciones=ordenTrayectoriaArticulaciones(listaInfo=info)
				if ventanaTrayectoriaArticulaciones.exec_():
					datosInfo=ventanaTrayectoriaArticulaciones.obtenerDatos()
					listaInfo[indice]=datosInfo
					ventanaTrayectoriaArticulaciones.accept()
					self.actualizarTablaOrdenes()
				else:
					ventanaTrayectoriaArticulaciones.close()
			elif orden=="TR_XYZ":
				ventanaTrayectoriaCartesiana=ordenTrayectoriaCartesiana(listaInfo=info)
				if ventanaTrayectoriaCartesiana.exec_():
					datosInfo=ventanaTrayectoriaCartesiana.obtenerDatos()
					listaInfo[indice]=datosInfo
					ventanaTrayectoriaCartesiana.accept()
					self.actualizarTablaOrdenes()
				else:
					ventanaTrayectoriaCartesiana.close()
			#elif orden=="ARTICULACIONES":
			#	ventanaIrArticulaciones=ordenIrArticulaciones(listaInfo=info)
			#	if ventanaIrArticulaciones.exec_():
			#		datosInfo=ventanaIrArticulaciones.obtenerDatos()
			#		listaInfo[indice]=datosInfo
			#		ventanaIrArticulaciones.accept()
			#		self.actualizarTablaOrdenes()
			#	else:
			#		ventanaIrArticulaciones.close()
			#elif orden=="XYZ":
			#	ventanaIrXYZ=ordenIrXYZ(listaInfo=info)
			#	if ventanaIrXYZ.exec_():
			#		datosInfo=ventanaIrXYZ.obtenerDatos()
			#		listaInfo[indice]=datosInfo
			#		ventanaIrXYZ.accept()
			#		self.actualizarTablaOrdenes()
			#	else:
			#		ventanaIrXYZ.close()
			elif orden=="LINEAL":
				ventanaLineal=ordenLineal(listaInfo=info)
				if ventanaLineal.exec_():
					datosInfo=ventanaLineal.obtenerDatos()
					listaInfo[indice]=datosInfo
					ventanaLineal.accept()
					self.actualizarTablaOrdenes()
				else:
					ventanaLineal.close()
			elif orden=="CIRCULAR":
				ventanaCircular=ordenCircular(listaInfo=info)
				if ventanaCircular.exec_():
					datosInfo=ventanaCircular.obtenerDatos()
					listaInfo[indice]=datosInfo
					ventanaCircular.accept()
					self.actualizarTablaOrdenes()
				else:
					ventanaCircular.close()
			elif orden=="MEMORIZAR":
				ventanaMemorizar=ordenMemorizar(listaInfo=info)
				if ventanaMemorizar.exec_():
					datosInfo=ventanaMemorizar.obtenerDatos()
					listaInfo[indice]=datosInfo
					ventanaMemorizar.accept()
					self.actualizarTablaOrdenes()
				else:
					ventanaMemorizar.close()
			elif orden=="ESPERAR":
				ventanaEsperar=ordenEsperar(listaInfo=info)
				if ventanaEsperar.exec_():
					datosInfo=ventanaEsperar.obtenerDatos()
					listaInfo[indice]=datosInfo
					ventanaEsperar.accept()
					self.actualizarTablaOrdenes()
				else:
					ventanaEsperar.close()
			elif orden=="SALTAR":
				ventanaSaltar=ordenSaltar(listaInfo=info)
				if ventanaSaltar.exec_():
					datosInfo=ventanaSaltar.obtenerDatos()
					listaInfo[indice]=datosInfo
					ventanaSaltar.accept()
					self.actualizarTablaOrdenes()
				else:
					ventanaSaltar.close()
			elif orden=="COMENTARIO":
				ventanaComentario=ordenComentario(listaInfo=info)
				if ventanaComentario.exec_():
					datosInfo=ventanaComentario.obtenerDatos()
					listaInfo[indice]=datosInfo
					ventanaComentario.accept()
					self.actualizarTablaOrdenes()
				else:
					ventanaComentario.close()
			elif orden=="VARIABLE":
				ventanaVariable=ordenVariable(listaInfo=info)
				if ventanaVariable.exec_():
					datosInfo=ventanaVariable.obtenerDatos()
					listaInfo[indice]=datosInfo
					ventanaVariable.accept()
					self.actualizarTablaOrdenes()
				else:
					ventanaVariable.close()
			elif orden=="SALTAR_SI":
				ventanaSaltarSi=ordenSaltarSi(listaInfo=info)
				if ventanaSaltarSi.exec_():
					datosInfo=ventanaSaltarSi.obtenerDatos()
					listaInfo[indice]=datosInfo
					ventanaSaltarSi.accept()
					self.actualizarTablaOrdenes()
				else:
					ventanaSaltarSi.close()
			elif orden=="COMPROBAR_SENSOR":
				ventanaComprobarSensor=ordenSiSensores(listaInfo=info)
				if ventanaComprobarSensor.exec_():
					datosInfo=ventanaComprobarSensor.obtenerDatos()
					listaInfo[indice]=datosInfo
					ventanaComprobarSensor.accept()
					self.actualizarTablaOrdenes()
				else:
					ventanaComprobarSensor.close()
			elif orden=="INTERRUPCION":
				ventanaInterrupciones=ordenInterrupciones(listaInfo=info)
				if ventanaInterrupciones.exec_():
					datosInfo=ventanaInterrupciones.obtenerDatos()
					listaInfo[indice]=datosInfo
					ventanaInterrupciones.accept()
					self.actualizarTablaOrdenes()
				else:
					ventanaInterrupciones.close()
			elif orden=="LLAMAR_SUBRUTINA":
				ventanaLlamarSubrutina=ordenSeleccionarSubrutina(listaInfo=info)
				if ventanaLlamarSubrutina.exec_():
					datosInfo=ventanaLlamarSubrutina.obtenerDatos()
					listaInfo[indice]=datosInfo
					ventanaLlamarSubrutina.accept()
					self.actualizarTablaOrdenes()
				else:
					ventanaLlamarSubrutina.close()
			elif orden=="ETIQUETA":
				ventanaEtiqueta=ordenEtiqueta(listaInfo=info)
				if ventanaEtiqueta.exec_():
					datosInfo=ventanaEtiqueta.obtenerDatos()
					listaInfo[indice]=datosInfo
					ventanaEtiqueta.accept()
					self.actualizarTablaOrdenes()
				else:
					ventanaEtiqueta.close()

	def reiniciarInterrupciones(self):
		self.interrupciones={"contacto0":[False, None],"contacto1":[False, None], \
		"contacto2":[False, None],"contacto3":[False, None],"contacto4":[False, None], \
		"contacto5":[False, None],"contacto6":[False, None],"contacto7":[False, None], \
		"contacto8":[False, None],"contacto9":[False, None],"laser0":[False, None], \
		"laser1":[False, None],"laser2":[False, None],"laser3":[False, None], \
		"laser4":[False, None],"laser5":[False, None],"laser6":[False, None],\
		"laser7":[False, None],"laser8":[False, None],"laser9":[False, None]}

	''' boton Borrar '''
	def botonBorrarOrdenes(self):
		global indiceTabla, listaInfo, listaOrdenes
		if self.ui.botonBorrar.isChecked():
			self.mostrarCheckBorrar()
		elif not self.ui.botonBorrar.isChecked():
			indicesParaBorrar=self.ordenesMarcadasParaBorrar()
			self.borrarOrdenes(indicesParaBorrar)
			self.borrarTablaCompleta()
			self.guardarEnArchivoTemporal()
			self.cargarArchivoTemporal()

	def mostrarCheckBorrar(self):
		for i in range(len(listaOrdenes)):
			item=QtGui.QTableWidgetItem(str(self.ui.tablaOrdenes.item(0,i).text()))
			item.setCheckState(0)
			self.ui.tablaOrdenes.setItem(0,i,item)

	def ordenesMarcadasParaBorrar(self):
		borrarIndices=[]
		for i in range(len(listaOrdenes)):
			item=self.ui.tablaOrdenes.item(0,i)
			if(item.checkState()==2):
				borrarIndices.append(i)
		borrarIndices.reverse()
		return borrarIndices

	def borrarOrdenes(self,listaABorrar):
		global listaOrdenes, listaInfo
		for i in listaABorrar:
			listaOrdenes.pop(i)
			listaInfo.pop(i)

	def borrarTablaCompleta(self):
		global indiceTabla
		self.ui.tablaOrdenes.setRowCount(0)
		self.ui.tablaOrdenes.setRowCount(1)
		indiceTabla=0

	def guardarEnArchivoTemporal(self):
		self.guardarEnArchivo(direccionAbsoluta+'/data/temporal.txt')

	def cargarArchivoTemporal(self):
		lineasArchivoTemporal=self.cargarLineasArchivo(direccionAbsoluta+'/data/temporal.txt')
		if not self.cargarOrdenesEnTabla(lineasArchivoTemporal):
			self.borrarTablaCompleta()
			QtGui.QMessageBox.about(self, 'Error','El archivo contiene errores. No es valido')

	''' botones Guardar y Guardar Como '''
	def botonGuardar(self):
		global hayDireccion, direccionGuardar
		if hayDireccion==False:
			if not self.obtenerDireccion():
				return False
		self.guardarEnArchivo(direccionGuardar)

	def botonGuardarComo(self):
		global hayDireccion, direccionGuardar
		if not self.obtenerDireccion():
			return False
		self.guardarEnArchivo(direccionGuardar)

	def obtenerDireccion(self):
		global hayDireccion, direccionGuardar
		direccionGuardar=QtGui.QFileDialog.getSaveFileName(
			self,
			"Guardar como...",
			"/home/"+os.environ['LOGNAME'],
			'*.txt')
		if direccionGuardar=="":
			return False
		else:
			hayDireccion=True
			return True

	def queGuardarEnArchivo(self, archivoGuardar, listaOrdenes, listaInfo, i):
		if listaOrdenes[i]=="ABRIR_PINZA":
			archivoGuardar.write(listaOrdenes[i]+"\n")
		elif listaOrdenes[i]=="CERRAR_PINZA":
			archivoGuardar.write(listaOrdenes[i]+"\n")
		elif listaOrdenes[i]=="IR":
			archivoGuardar.write(listaOrdenes[i]+" "+textoAGuardar(listaInfo[i]))
		elif listaOrdenes[i]=="TR_ARTICULACIONES":
			archivoGuardar.write(listaOrdenes[i]+" "+str(listaInfo[i])+"\n")
		elif listaOrdenes[i]=="TR_XYZ":
			archivoGuardar.write(listaOrdenes[i]+" "+str(listaInfo[i])+"\n")
		elif listaOrdenes[i]=="ARTICULACIONES":
			archivoGuardar.write(listaOrdenes[i]+" "+textoAGuardar(listaInfo[i]))
		elif listaOrdenes[i]=="XYZ":
			archivoGuardar.write(listaOrdenes[i]+" "+textoAGuardar(listaInfo[i]))
		elif listaOrdenes[i]=="LINEAL":
			archivoGuardar.write(listaOrdenes[i]+" "+textoAGuardar(listaInfo[i]))
		elif listaOrdenes[i]=="CIRCULAR":
			archivoGuardar.write(listaOrdenes[i]+" "+textoAGuardar(listaInfo[i]))
		elif listaOrdenes[i]=="MEMORIZAR":
			archivoGuardar.write(listaOrdenes[i]+" "+str(listaInfo[i])+"\n")
		elif listaOrdenes[i]=="ESPERAR":
			archivoGuardar.write(listaOrdenes[i]+" "+str(listaInfo[i])+"\n")
		elif listaOrdenes[i]=="SALTAR":
			archivoGuardar.write(listaOrdenes[i]+" "+str(listaInfo[i])+"\n")
		elif listaOrdenes[i]=="SALTAR_SI":
			archivoGuardar.write(listaOrdenes[i]+" "+textoAGuardar(listaInfo[i]))
		elif listaOrdenes[i]=="COMENTARIO":
			archivoGuardar.write(listaOrdenes[i]+" "+str(listaInfo[i])+"\n")
		elif listaOrdenes[i]=="ETIQUETA":
			archivoGuardar.write(listaOrdenes[i]+" "+str(listaInfo[i])+"\n")
		elif listaOrdenes[i]=="VARIABLE":
			archivoGuardar.write(listaOrdenes[i]+" "+textoAGuardar(listaInfo[i]))
		elif listaOrdenes[i]=="LLAMAR_SUBRUTINA":
			archivoGuardar.write(listaOrdenes[i]+" "+str(listaInfo[i])+"\n")
		elif listaOrdenes[i]=="COMPROBAR_SENSOR":
			archivoGuardar.write(listaOrdenes[i]+" "+textoAGuardar(listaInfo[i]))
		elif listaOrdenes[i]=="INTERRUPCION":
			archivoGuardar.write(listaOrdenes[i]+" "+textoAGuardar(listaInfo[i]))

	def cargarLineasDeArchivoSubrutina(self,nombreSubrutina):
		direccionArchivo=direccionAbsoluta+'/data/subrutina_'+nombreSubrutina
		archivo=open(direccionArchivo,'r')
		lineasArchivo=archivo.readlines()
		archivo.close()
		return lineasArchivo

	def guardarEnArchivo(self,direccionGuardar):
		global listaInfo, listaOrdenes
		archivoGuardar=open(direccionGuardar,'w+')

		for i in range(len(listaOrdenes)):
			self.queGuardarEnArchivo(archivoGuardar, listaOrdenes, listaInfo, i)

		subrutinas=self.obtenerNombreSubrutinas()
		if len(subrutinas)!=0:
			for nombreSubrutina in subrutinas:
				lineasArchivo=self.cargarLineasDeArchivoSubrutina(nombreSubrutina)
				global listaOrdenesSub, listaInfoSub
				listaInfoSub, listaOrdenesSub = [], []
				self.cargarOrdenesEnListasSubrutinas(lineasArchivo)
				self.guardarSubrutinaEnArchivo(nombreSubrutina, archivoGuardar)

		listaPosiciones=obtenerNumeroPosicionesDisponibles()
		if listaPosiciones!=[]:
			for numeroPosicion in listaPosiciones:
				datos=self.obtenerPosicionSeleccionada(numeroPosicion)
				mensajePosicion=""
				for valorPosicion in datos:
					valorPosicionRedondeado=round(valorPosicion,2)
					mensajePosicion=mensajePosicion+str(valorPosicionRedondeado)+" "
				mensajeGuardar="POSICION "+str(numeroPosicion)+" "+mensajePosicion
				archivoGuardar.write(mensajeGuardar+"\n")

		archivoGuardar.close()

	def guardarSubrutinaEnArchivo(self,nombreSubrutina, archivoGuardar):
		global listaOrdenesSub, listaInfoSub
		archivoGuardar.write("NOMBRE "+nombreSubrutina+"\n")
		for i in range(len(listaOrdenesSub)):
			self.queGuardarEnArchivo(archivoGuardar, listaOrdenesSub, listaInfoSub, i)
		archivoGuardar.write("FIN SUBRUTINA "+nombreSubrutina+"\n")

	def obtenerPosicionSeleccionada(self, numeroPosicion):
		try:
			numeroPosicion=int(numeroPosicion)
		except Exception:
			QtGui.QMessageBox.about(self, 'Error','Solo se pueden introducir numeros')
		archivo=direccionAbsoluta+'/posiciones/P'+str(numeroPosicion)+".txt"
		return np.loadtxt(archivo) # abs_rel n_pos_ref x y z yaw pitch roll

	def guardarOrdenEnListaSubrutina(self,datos):
		global listaOrdenesSub, listaInfoSub
		listaOrdenesSub.append(datos[0])
		listaInfoSub.append(datos[1])

	def cargarOrdenesEnListasSubrutinas(self,lineasArchivo):
		global listaOrdenesSub, listaInfoSub
		indiceIntroducirTabla=0
		ok=False
		for i in range(len(lineasArchivo)):
			contenidoLinea=lineasArchivo[i]
			contenidoLinea=contenidoLinea.split()
			if contenidoLinea[0]=="ABRIR_PINZA":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarAbrirPinza(contenidoLinea)
			elif contenidoLinea[0]=="CERRAR_PINZA":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarCerrarPinza(contenidoLinea)
			elif contenidoLinea[0]=="IR":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarIr(contenidoLinea)
			elif contenidoLinea[0]=="TR_ARTICULACIONES":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarTrayectoriaArticulaciones(contenidoLinea)
			elif contenidoLinea[0]=="TR_XYZ":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarTrayectoriaCartesiana(contenidoLinea)
			#elif contenidoLinea[0]=="ARTICULACIONES":
			#	[ok, datosAGuardar, mensajeAMostrar]=comprobarIrArticulaciones(contenidoLinea)
			#elif contenidoLinea[0]=="XYZ":
			#	[ok, datosAGuardar, mensajeAMostrar]=comprobarIrXYZ(contenidoLinea)
			elif contenidoLinea[0]=="LINEAL":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarLineal(contenidoLinea)
			elif contenidoLinea[0]=="CIRCULAR":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarCircular(contenidoLinea)
			elif contenidoLinea[0]=="MEMORIZAR":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarMemorizar(contenidoLinea)
			elif contenidoLinea[0]=="ESPERAR":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarEsperar(contenidoLinea)
			elif contenidoLinea[0]=="SALTAR":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarSaltar(contenidoLinea)
			elif contenidoLinea[0]=="COMENTARIO":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarComentario(contenidoLinea)
			elif contenidoLinea[0]=="ETIQUETA":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarEtiqueta(contenidoLinea)
			elif contenidoLinea[0]=="COMPROBAR_SENSOR":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarSiSensor(contenidoLinea)
			elif contenidoLinea[0]=="INTERRUPCION":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarInterrupcion(contenidoLinea)
			else:
				ok=False
			if contenidoLinea[0]=="POSICION":
				[ok_posicion, numeroPosicion, valorPosicion]=comprobarPosicion(contenidoLinea)
			if ok:
				self.guardarOrdenEnListaSubrutina([contenidoLinea[0], datosAGuardar])
				indiceIntroducirTabla+=1
			if ok==False:
				return False
		return [True, indiceIntroducirTabla]

	def obtenerNombreSubrutinas(self):
		path = direccionAbsoluta+'/data/'
		subrutinas = []
		for i in os.listdir(path):
			if os.path.isfile(os.path.join(path,i)) and 'subrutina_' in i:
				subrutinas.append(i[10:])
		return subrutinas

	''' boton Ejecutar Linea '''
	def recibirOrdenFinalizada(self, i):
		global listaOrdenes
		if i==-1:
			self.activarBotones()
		elif i==-2:
			self.activarBotones()
			QtGui.QMessageBox.about(self, 'Error','Se ha superado el limite de velocidad')
		elif i==-3:
			archivo_nombre=open(direccionAbsoluta+'/data/nombre_cargar', 'r')
			nombreSubrutina=archivo_nombre.readlines()
			if type(nombreSubrutina)==list:
				nombreSubrutina=nombreSubrutina[0]
			archivo_nombre.close()
			self.ventanaMostrarSubrutina.show()
			self.ventanaMostrarSubrutina.cargarOrdenes(nombreSubrutina)
			self.ventanaMostrarSubrutina.marcarLineaAEjecutar(0)
			time.sleep(0.4)
		if i>999:
			k=i-1000
			self.ventanaMostrarSubrutina.marcarLineaAEjecutar(k)
		else:
			i+=1
			if len(listaOrdenes)==i:
				self.marcarLineaAEjecutar(0)
				self.activarBotones()
			else:
				self.marcarLineaAEjecutar(i)
				self.activarBotones()

	def hiloUnaOrden(self, indiceTabla):
		global objeto_linea
		self.ejecutandoseLinea=True
		indiceTabla, tiempo_espera=self.realizarAccion(indiceTabla)
		division_tiempo=0.25
		if tiempo_espera!=None:
			div_tiempo=int(float(tiempo_espera)/division_tiempo)
			for i in range(div_tiempo):
				time.sleep(division_tiempo)
				final=funcionParaRevisarSiSeHaPulsadoSTOP()
				if final:
					funcionParaPararElRobotYLaPinza()
					reiniciarIndiceSubrutinas()
					borrarVariables()
					escribirStop(False)
					self.ejecutandoseLinea=False
					objeto_linea.senal.emit(-1)
					return
		self.ejecutandoseLinea=False
		objeto_linea.senal.emit(indiceTabla)

	def ejecutarLinea(self):
		if ejecutandose():
			global listaOrdenes, listaInfo, indiceTabla
			if indiceTabla==len(listaOrdenes):
				indiceTabla=0
			isItemSelected=self.ui.tablaOrdenes.selectedItems()
			if len(isItemSelected)!=0:
				indiceTabla=indiceIntroducirTabla=isItemSelected[0].row()

			self.desactivarBotones()
			hiloEjecutarLinea=threading.Thread(target=self.hiloUnaOrden, args=(indiceTabla,), name='Daemon')
			hiloEjecutarLinea.start()
		else:
			QtGui.QMessageBox.about(self, 'Error','No hay conexion con robot/simulador')

	def marcarLineaAEjecutar(self, indice):
		numeroItems=self.ui.tablaOrdenes.rowCount()
		for i in range(numeroItems):
			itemPosible=self.ui.tablaOrdenes.item(0,i)
			if self.ui.tablaOrdenes.isItemSelected(itemPosible):
				self.ui.tablaOrdenes.setItemSelected(itemPosible,False)
		itemActual=self.ui.tablaOrdenes.item(0,indice)
		self.ui.tablaOrdenes.setItemSelected(itemActual,True)

	def obtenerPosicionSeleccionada(self, numeroPosicion):
		try:
			numeroPosicion=int(numeroPosicion)
		except Exception:
			QtGui.QMessageBox.about(self, 'Error','Solo se pueden introducir numeros')
		archivo=direccionAbsoluta+'/posiciones/P'+str(numeroPosicion)+".txt"
		return np.loadtxt(archivo) # abs_rel n_pos_ref x y z pitch roll

	def realizarAccion(self,indiceLocal):
		global listaOrdenes, listaInfo, objeto_linea
		if listaOrdenes[indiceLocal]=="ABRIR_PINZA":
			tiempo_espera=abrirPinza()
			return indiceLocal, tiempo_espera
		elif listaOrdenes[indiceLocal]=="CERRAR_PINZA":
			tiempo_espera=cerrarPinza()
			return indiceLocal, tiempo_espera
		elif listaOrdenes[indiceLocal]=="IR":
			numeroPosicion=listaInfo[indiceLocal][0]
			variable=listaInfo[indiceLocal][1]
			valor=listaInfo[indiceLocal][2]
			if variable=='tiempo':
				tiempo=int(valor)
				irPosicion(numeroPosicion, tiempo)
				tiempo_espera=tiempo
			else:
				tiempo_espera=irPosicion(numeroPosicion, valor, 'velocidad')
			return indiceLocal, tiempo_espera
		elif listaOrdenes[indiceLocal]=="TR_ARTICULACIONES":
			direccionArchivo=listaInfo[indiceLocal]
			datos=np.loadtxt(direccionArchivo)
			tiempo_espera=ejecutar_trayectoria_articulaciones(datos)
			return indiceLocal, tiempo_espera
		elif listaOrdenes[indiceLocal]=="TR_XYZ":
			direccionArchivo=listaInfo[indiceLocal]
			datos=np.loadtxt(direccionArchivo)
			tiempo_espera=ejecutar_trayectoria_cartesiana(datos)
			return indiceLocal, tiempo_espera
		elif listaOrdenes[indiceLocal]=="ARTICULACIONES":
			articulaciones=listaInfo[indiceLocal][:6]
			valor=listaInfo[indiceLocal][7]
			tipo=listaInfo[indiceLocal][6]
			tiempo_espera=irPorArticulaciones(articulaciones, valor, tipo)
			return indiceLocal, tiempo_espera
		elif listaOrdenes[indiceLocal]=="XYZ":
			datos_posicion=listaInfo[indiceLocal][:6]
			valor=listaInfo[indiceLocal][7]
			tipo=listaInfo[indiceLocal][6]
			tiempo_espera=irPorXYZ(datos_posicion, valor, tipo)
			return indiceLocal, tiempo_espera
		elif listaOrdenes[indiceLocal]=="LINEAL":
			posicionFinal=listaInfo[indiceLocal][0]
			variable=listaInfo[indiceLocal][1]
			valor=listaInfo[indiceLocal][2]
			if variable=='tiempo':
				tiempo=valor
			else:
				matHomogenea=np.zeros([4,4])
				x,y,z,yaw,pitch,roll=obtenerPosicionSeleccionada(posicionFinal)
				matHomogenea[:3,:3]=obtenerMatRotacionDesdeAngulos(roll,pitch,yaw)
				matHomogenea[0,3]=x
				matHomogenea[1,3]=y
				matHomogenea[2,3]=z
				th_actual=leerArticulacionesActuales()
				if comprobarHome(th_actual):
					th_actual=[0.1,0,0,0,0,0]
				thetas_rad=ikine(matHomogenea,th_actual)
				if len(thetas_rad)>0:
					thetas_rad=thetas_rad[:6]
				tiempo=obtenerTiempoDesdeVelocidad(thetas_rad, valor)
			limite_superado=movimientoLineal(posicionFinal,tiempo)
			if limite_superado:
				try:
					objeto_linea.senal.emit(-2)
					#QtGui.QMessageBox.about(self, 'Error','Se ha superado el limite de velocidad')
				except:
					objeto_ciclo.senal.emit(-2)
			time.sleep(tiempo)
			tiempo_espera=tiempo
			return indiceLocal, tiempo_espera
		elif listaOrdenes[indiceLocal]=="CIRCULAR":
			posicionFinal=listaInfo[indiceLocal][0]
			posicionMedia=listaInfo[indiceLocal][1]
			variable=listaInfo[indiceLocal][2]
			valor=listaInfo[indiceLocal][3]
			if variable=='tiempo':
				tiempo=valor
			else:
				matHomogenea=np.zeros([4,4])
				x,y,z,yaw,pitch,roll=obtenerPosicionSeleccionada(posicionFinal)
				matHomogenea[:3,:3]=obtenerMatRotacionDesdeAngulos(roll,pitch,yaw)
				matHomogenea[0,3]=x
				matHomogenea[1,3]=y
				matHomogenea[2,3]=z
				th_actual=leerArticulacionesActuales()
				if comprobarHome(th_actual):
					th_actual=[0.1,0,0,0,0,0]
				thetas_rad=ikine(matHomogenea,th_actual)
				if len(thetas_rad)>0:
					thetas_rad=thetas_rad[:6]
				tiempo=obtenerTiempoDesdeVelocidad(thetas_rad, valor)

			limite_superado=movimientoCircular(posicionFinal, posicionMedia, tiempo)
			if limite_superado:
				try:
					objeto_linea.senal.emit(-2)
					#QtGui.QMessageBox.about(self, 'Error','Se ha superado el limite de velocidad')
				except:
					objeto_ciclo.senal.emit(-2)
			tiempo_espera=tiempo
			return indiceLocal, tiempo_espera
		elif listaOrdenes[indiceLocal]=="MEMORIZAR":
			numeroPosicion=listaInfo[indiceLocal]
			memorizar(numeroPosicion)
			return indiceLocal, None
		elif listaOrdenes[indiceLocal]=="ESPERAR":
			numeroPosicion=listaInfo[indiceLocal]
			esperarRobot(numeroPosicion)
			return indiceLocal, None
		elif listaOrdenes[indiceLocal]=="SALTAR":
			etiquetaDestino=listaInfo[indiceLocal]
			indiceLocal=saltarA(etiquetaDestino,listaOrdenes, listaInfo)
			return indiceLocal, None
		elif listaOrdenes[indiceLocal]=="COMENTARIO":
			return indiceLocal, None
		elif listaOrdenes[indiceLocal]=="VARIABLE":
			nombre=listaInfo[indiceLocal][0]
			expresion=listaInfo[indiceLocal][1]
			# hago que devuelva el nuevo valor por si quiero mostrarlo en la tabla
			# pero la modificacion de la varible la realiza al completo en
			# la funcion 'ponerVariable'
			funciona, diccionarioVariables=ponerVariable(self.variables,nombre, expresion)
			if funciona:
				self.variables=diccionarioVariables
			return indiceLocal, None
		elif listaOrdenes[indiceLocal]=="ETIQUETA":
			return indiceLocal, None
		elif listaOrdenes[indiceLocal]=="SALTAR_SI":
			valorCondicion=listaInfo[indiceLocal][0]
			variableCondicion=listaInfo[indiceLocal][1]
			tipoCondicion=listaInfo[indiceLocal][2]
			etiquetaDestino=listaInfo[indiceLocal][3].rstrip()
			indiceLocal=saltar_si(indiceLocal, self.variables, valorCondicion,variableCondicion,tipoCondicion,etiquetaDestino, listaOrdenes, listaInfo)
			return indiceLocal, None
		elif listaOrdenes[indiceLocal]=="COMPROBAR_SENSOR":
			indiceLocal=comprobar_estado_sensor(listaInfo[indiceLocal], indiceLocal, listaOrdenes, listaInfo, self)
			return indiceLocal, None
		elif listaOrdenes[indiceLocal]=="INTERRUPCION":
			self.interrupciones=activar_desactivar_interrupciones(self.interrupciones, listaInfo[indiceLocal])
			return indiceLocal, None
		elif listaOrdenes[indiceLocal]=="LLAMAR_SUBRUTINA":
			if self.subrutina_iniciada==False:
				self.subrutina_iniciada=True
				nombreSubrutina=listaInfo[indiceLocal]
				lineasArchivo, self.numeroOrdenesSubrutina=self.lineasSubrutina(nombreSubrutina)
				if len(lineasArchivo)>1:
					for i in lineasArchivo:
						orden=i.rstrip()
						lista=orden.split()
						if len(lista)==1:
							self.listaOrdenesSub.append(lista[0])
							self.listaInfoSub.append(None)
						elif len(lista)>1:
							self.listaOrdenesSub.append(lista[0])
							self.listaInfoSub.append(lista[1:])
				else:
					orden=lineasArchivo[0].rstrip()
					lista=orden.split()
					if len(lista)==1:
						self.listaOrdenesSub.append(lista)
						self.listaInfoSub.append(None)
					elif len(lista)>1:
						self.listaOrdenesSub.append(lista[0])
						self.listaInfoSub.append(lista[1:])
				archivo_nombre=open(direccionAbsoluta+'/data/nombre_cargar', 'w+')
				archivo_nombre.write(str(nombreSubrutina))
				archivo_nombre.close()
				objeto_linea.senal.emit(-3)
				if self.listaOrdenesSub[0]=="TR_ARTICULACIONES" or self.listaOrdenesSub[0]=="TR_XYZ":
					self.listaInfoSub[0]=self.listaInfoSub[0][0]
				indice, tiempo_espera=realizarAccionSubrutina(self.listaOrdenesSub, self.listaInfoSub, 0)
				time.sleep(tiempo_espera)
				if self.numeroOrdenesSubrutina>1:
					#self.ventanaMostrarSubrutina.marcarLineaAEjecutar(1)
					objeto_linea.senal.emit(1001)
					self.ordenesSubrutinaEjecutadas+=1
					indiceLocal=indiceLocal-1
				else:
					indiceLocal=indiceLocal
					self.terminarSubrutina()
			else:
				if self.listaOrdenesSub[self.ordenesSubrutinaEjecutadas]=="TR_ARTICULACIONES" or self.listaOrdenesSub[self.ordenesSubrutinaEjecutadas]=="TR_XYZ":
					self.listaInfoSub[self.ordenesSubrutinaEjecutadas]=self.listaInfoSub[self.ordenesSubrutinaEjecutadas][0]
				indice, tiempo_espera=realizarAccionSubrutina(self.listaOrdenesSub, self.listaInfoSub, self.ordenesSubrutinaEjecutadas)
				time.sleep(tiempo_espera)
				self.ordenesSubrutinaEjecutadas+=1
				if self.numeroOrdenesSubrutina==self.ordenesSubrutinaEjecutadas:
					indiceLocal=indiceLocal
					self.terminarSubrutina()
				else:
					self.ventanaMostrarSubrutina.marcarLineaAEjecutar(self.ordenesSubrutinaEjecutadas)
					indiceLocal=indiceLocal-1
			return indiceLocal, None

	def desactivarBotones(self):
		self.ui.botonBorrar.setEnabled(False)
		self.ui.botonGuardar.setEnabled(False)
		self.ui.botonGuardarComo.setEnabled(False)
		self.ui.botonAbrir.setEnabled(False)
		self.ui.botonLinea.setEnabled(False)
		self.ui.botonCiclo.setEnabled(False)

	def activarBotones(self):
		self.ui.botonBorrar.setEnabled(True)
		self.ui.botonGuardar.setEnabled(True)
		self.ui.botonGuardarComo.setEnabled(True)
		self.ui.botonAbrir.setEnabled(True)
		self.ui.botonLinea.setEnabled(True)
		self.ui.botonCiclo.setEnabled(True)

	def terminarSubrutina(self):
		self.ventanaMostrarSubrutina.borrarTabla()
		self.ventanaMostrarSubrutina.hide()
		self.ordenesSubrutinaEjecutadas=0
		self.subrutina_iniciada=False
		self.listaOrdenesSub=[]
		self.listaInfoSub=[]

	def lineasSubrutina(self, nombreSubrutina):
		direccionArchivo=direccionAbsoluta+'/data/subrutina_'+nombreSubrutina
		archivo=open(direccionArchivo,'r')
		lineasArchivo=archivo.readlines()
		return lineasArchivo, len(lineasArchivo)

	''' boton Stop '''
	def stopOrdenes(self):
		if self.ejecutandoseCiclo==True or self.ejecutandoseLinea==True:
			escribirStop(True)
			self.ejecutandoseCiclo=False
			try:
				pararRobot()
				pararPinza()
			except:
				pass
		if not self.ui.botonAbrir.isEnabled():
			self.activarBotones()
			self.ejecutandoseCiclo=False

	''' boton Ejecutar Ciclo'''
	def ejecutarCiclo(self):
		if self.ejecutandoseCiclo==False:
			self.ejecutandoseCiclo=True
			threadCiclo=threading.Thread(target=self.auxiliarThreadCiclo, name='Daemon')
			threadCiclo.start()
		else:
			print "Ya hay una ciclo ejecutandose"
			print "Estado ejecutandoseCiclo: ", self.ejecutandoseCiclo

	def auxiliarThreadCiclo(self):
		global objeto_ciclo
		objeto_ciclo=claseEnviarIndice()
		objeto_ciclo.senal.connect(self.recibirDatos)
		indiceSiguiente=0
		while indiceSiguiente!=(len(listaOrdenes)):
			objeto_ciclo.senal.emit(indiceSiguiente)
			final=funcionParaRevisarSiSeHaPulsadoSTOP()
			if final:
				funcionParaPararElRobotYLaPinza()
				reiniciarIndiceSubrutinas()
				borrarVariables()
				self.ejecutandoseCiclo=False
				escribirStop(False)
				return
			self.comprobarInterrupciones(indiceSiguiente)
			final=funcionParaRevisarSiSeHaPulsadoSTOP()
			if final:
				funcionParaPararElRobotYLaPinza()
				reiniciarIndiceSubrutinas()
				borrarVariables()
				self.ejecutandoseCiclo=False
				escribirStop(False)
				return
			else:
				indiceSiguiente, tiempo_espera=self.realizarAccion(indiceSiguiente)
				if tiempo_espera!=None:
					iteraciones=int(float(tiempo_espera)/0.25)
					for i in range(iteraciones):
						time.sleep(0.25)
						self.comprobarInterrupciones(indiceSiguiente)
						final=funcionParaRevisarSiSeHaPulsadoSTOP()
						if final:
							funcionParaPararElRobotYLaPinza()
							reiniciarIndiceSubrutinas()
							borrarVariables()
							self.ejecutandoseCiclo=False
							escribirStop(False)
							return
				indiceSiguiente+=1
		reiniciarIndiceSubrutinas()
		borrarVariables()
		self.ejecutandoseCiclo=False

	def comprobarInterrupciones(self, indice):
		global listaOrdenes, listaInfo
		listaValues=self.interrupciones.values()
		listaKeys=self.interrupciones.keys()
		sensores=[]
		valores=[]
		subrutinas=[]
		for i in range(len(listaValues)):
			sensores.append(listaKeys[i])
			valores.append(listaValues[i][0])
			subrutinas.append(listaValues[i][1])
		listaComprobar=[]
		for i in range(len(valores)):
			if valores[i]:
				listaComprobar.append([sensores[i],subrutinas[i]])
		# en este punto tengo la lista de sensores que hay que revisar
		# y la subrutina asociada para ejecutarse
		if listaComprobar!=[]:
			for i in range(len(listaComprobar)):
				if listaComprobar[i][0][:5]=="conta":
					tipoSensor="contacto"
					numero=listaComprobar[i][0][8:]
				elif listaComprobar[i][0][:5]=="laser":
					tipoSensor="laser"
					numero=listaComprobar[i][0][5:]
				estadoDeseado="TRUE"
				tipoDestino="SUBRUTINA"
				destino=listaComprobar[i][1]
				lista=[tipoSensor, numero, estadoDeseado, tipoDestino, destino]
				comprobar_estado_sensor(lista, indice, listaOrdenes, listaInfo, self)
				return
		return

	@QtCore.pyqtSlot(list)
	def recibirComandos(self, listaRecibida):
		global listaOrdenes, listaInfo
		#mensajeAMostrar=crearMensaje(listaRecibida)
		indiceIntroducirTabla=len(listaOrdenes)-1
		indice=self.ui.tablaOrdenes.selectedItems()
		if len(indice)!=0:
			indiceIntroducirTabla=indice[0].row()
		self.guardarOrden(listaRecibida,indiceIntroducirTabla)
		self.guardarEnArchivoTemporal()
		self.borrarTablaCompleta()
		self.cargarArchivoTemporal()
		self.raise_()

	def recibirDatos(self,i):
		if i==-2:
			self.activarBotones()
			QtGui.QMessageBox.about(self, 'Error','Se ha superado el limite de velocidad')
			escribirStop(True)
		else:
			self.marcarLineaAEjecutar(i)

class claseEnviarIndice(QObject):
	senal=pyqtSignal(int)
	def __init__(self):
		QObject.__init__(self)
	def punch(self,i):
		self.senal.emit(i)

class claseEnviarInt(QObject):
	senal=pyqtSignal(int)
	def __init__(self):
		QObject.__init__(self)
	def punch(self,i):
		self.senal.emit(i)

if __name__ == '__main__':
	app = QtGui.QApplication(sys.argv)
	ventanaListadoPrograma=listadoPrograma()
	ventanaListadoPrograma.show()
	sys.exit(app.exec_())
