# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ventanaIr.ui'
#
# Created: Wed May 18 11:51:42 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_IrPosicion(object):
    def setupUi(self, IrPosicion):
        IrPosicion.setObjectName(_fromUtf8("IrPosicion"))
        IrPosicion.resize(192, 190)
        IrPosicion.setMinimumSize(QtCore.QSize(192, 190))
        IrPosicion.setMaximumSize(QtCore.QSize(192, 190))
        self.verticalLayoutWidget_3 = QtGui.QWidget(IrPosicion)
        self.verticalLayoutWidget_3.setGeometry(QtCore.QRect(10, 60, 168, 71))
        self.verticalLayoutWidget_3.setObjectName(_fromUtf8("verticalLayoutWidget_3"))
        self.verticalLayout_3 = QtGui.QVBoxLayout(self.verticalLayoutWidget_3)
        self.verticalLayout_3.setMargin(0)
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.horizontalLayout_4 = QtGui.QHBoxLayout()
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))
        self.radioTiempo = QtGui.QRadioButton(self.verticalLayoutWidget_3)
        self.radioTiempo.setChecked(True)
        self.radioTiempo.setObjectName(_fromUtf8("radioTiempo"))
        self.buttonGroup = QtGui.QButtonGroup(IrPosicion)
        self.buttonGroup.setObjectName(_fromUtf8("buttonGroup"))
        self.buttonGroup.addButton(self.radioTiempo)
        self.horizontalLayout_4.addWidget(self.radioTiempo)
        self.valorTiempo = QtGui.QLineEdit(self.verticalLayoutWidget_3)
        self.valorTiempo.setObjectName(_fromUtf8("valorTiempo"))
        self.horizontalLayout_4.addWidget(self.valorTiempo)
        self.verticalLayout_3.addLayout(self.horizontalLayout_4)
        self.horizontalLayout_7 = QtGui.QHBoxLayout()
        self.horizontalLayout_7.setObjectName(_fromUtf8("horizontalLayout_7"))
        self.radioVelocidad = QtGui.QRadioButton(self.verticalLayoutWidget_3)
        self.radioVelocidad.setObjectName(_fromUtf8("radioVelocidad"))
        self.buttonGroup.addButton(self.radioVelocidad)
        self.horizontalLayout_7.addWidget(self.radioVelocidad)
        self.valorVelocidad = QtGui.QSpinBox(self.verticalLayoutWidget_3)
        self.valorVelocidad.setMinimumSize(QtCore.QSize(63, 27))
        self.valorVelocidad.setMaximumSize(QtCore.QSize(63, 27))
        self.valorVelocidad.setMinimum(1)
        self.valorVelocidad.setMaximum(10)
        self.valorVelocidad.setObjectName(_fromUtf8("valorVelocidad"))
        self.horizontalLayout_7.addWidget(self.valorVelocidad)
        self.verticalLayout_3.addLayout(self.horizontalLayout_7)
        self.buttonIr = QtGui.QPushButton(IrPosicion)
        self.buttonIr.setGeometry(QtCore.QRect(50, 150, 91, 27))
        self.buttonIr.setObjectName(_fromUtf8("buttonIr"))
        self.horizontalLayoutWidget = QtGui.QWidget(IrPosicion)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(10, 10, 171, 31))
        self.horizontalLayoutWidget.setObjectName(_fromUtf8("horizontalLayoutWidget"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setMargin(0)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label = QtGui.QLabel(self.horizontalLayoutWidget)
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout.addWidget(self.label)
        self.numeroPosicion = QtGui.QComboBox(self.horizontalLayoutWidget)
        self.numeroPosicion.setMinimumSize(QtCore.QSize(60, 27))
        self.numeroPosicion.setMaximumSize(QtCore.QSize(60, 27))
        self.numeroPosicion.setEditable(False)
        self.numeroPosicion.setMaxVisibleItems(25)
        self.numeroPosicion.setMaxCount(25)
        self.numeroPosicion.setObjectName(_fromUtf8("numeroPosicion"))
        self.horizontalLayout.addWidget(self.numeroPosicion)

        self.retranslateUi(IrPosicion)
        self.numeroPosicion.setCurrentIndex(-1)
        QtCore.QMetaObject.connectSlotsByName(IrPosicion)

    def retranslateUi(self, IrPosicion):
        IrPosicion.setWindowTitle(_translate("IrPosicion", "Ir Posición", None))
        self.radioTiempo.setText(_translate("IrPosicion", "Tiempo [s]", None))
        self.radioVelocidad.setText(_translate("IrPosicion", "Velocidad", None))
        self.buttonIr.setText(_translate("IrPosicion", "Aceptar", None))
        self.label.setText(_translate("IrPosicion", "Posicion final:", None))

