# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ventanaCrearSubrutina.ui'
#
# Created: Sun Jun  5 02:13:30 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_crearSubrutina(object):
    def setupUi(self, crearSubrutina):
        crearSubrutina.setObjectName(_fromUtf8("crearSubrutina"))
        crearSubrutina.resize(718, 500)
        crearSubrutina.setMinimumSize(QtCore.QSize(718, 500))
        crearSubrutina.setMaximumSize(QtCore.QSize(718, 500))
        self.nombreSubrutina = QtGui.QComboBox(crearSubrutina)
        self.nombreSubrutina.setGeometry(QtCore.QRect(10, 10, 221, 27))
        self.nombreSubrutina.setEditable(True)
        self.nombreSubrutina.setObjectName(_fromUtf8("nombreSubrutina"))
        self.botonCargar = QtGui.QPushButton(crearSubrutina)
        self.botonCargar.setGeometry(QtCore.QRect(240, 10, 31, 26))
        self.botonCargar.setText(_fromUtf8(""))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8("interfaz/iconos/open-folder.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.botonCargar.setIcon(icon)
        self.botonCargar.setIconSize(QtCore.QSize(20, 20))
        self.botonCargar.setObjectName(_fromUtf8("botonCargar"))
        self.botonEliminarSubrutina = QtGui.QPushButton(crearSubrutina)
        self.botonEliminarSubrutina.setGeometry(QtCore.QRect(320, 10, 31, 26))
        self.botonEliminarSubrutina.setText(_fromUtf8(""))
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(_fromUtf8("interfaz/iconos/delete-file-symbol-of-paper-sheet-with-text.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.botonEliminarSubrutina.setIcon(icon1)
        self.botonEliminarSubrutina.setIconSize(QtCore.QSize(22, 22))
        self.botonEliminarSubrutina.setObjectName(_fromUtf8("botonEliminarSubrutina"))
        self.botonGuardar = QtGui.QPushButton(crearSubrutina)
        self.botonGuardar.setGeometry(QtCore.QRect(280, 10, 31, 26))
        self.botonGuardar.setText(_fromUtf8(""))
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(_fromUtf8("interfaz/iconos/save_simple2.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.botonGuardar.setIcon(icon2)
        self.botonGuardar.setIconSize(QtCore.QSize(20, 20))
        self.botonGuardar.setObjectName(_fromUtf8("botonGuardar"))
        self.arbolComandos = QtGui.QTreeWidget(crearSubrutina)
        self.arbolComandos.setGeometry(QtCore.QRect(440, 50, 270, 281))
        self.arbolComandos.setObjectName(_fromUtf8("arbolComandos"))
        item_0 = QtGui.QTreeWidgetItem(self.arbolComandos)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_0 = QtGui.QTreeWidgetItem(self.arbolComandos)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        item_1 = QtGui.QTreeWidgetItem(item_0)
        self.tablaOrdenes = QtGui.QTableWidget(crearSubrutina)
        self.tablaOrdenes.setGeometry(QtCore.QRect(10, 50, 421, 431))
        self.tablaOrdenes.setObjectName(_fromUtf8("tablaOrdenes"))
        self.tablaOrdenes.setColumnCount(1)
        self.tablaOrdenes.setRowCount(1)
        item = QtGui.QTableWidgetItem()
        self.tablaOrdenes.setVerticalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        font = QtGui.QFont()
        font.setPointSize(10)
        item.setFont(font)
        self.tablaOrdenes.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        item.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
        self.tablaOrdenes.setItem(0, 0, item)
        self.tablaOrdenes.horizontalHeader().setDefaultSectionSize(400)
        self.botonStop = QtGui.QPushButton(crearSubrutina)
        self.botonStop.setGeometry(QtCore.QRect(500, 10, 31, 27))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.botonStop.setFont(font)
        self.botonStop.setText(_fromUtf8(""))
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(_fromUtf8("interfaz/iconos/stop-hexagonal-signal.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.botonStop.setIcon(icon3)
        self.botonStop.setIconSize(QtCore.QSize(20, 20))
        self.botonStop.setObjectName(_fromUtf8("botonStop"))
        self.botonLinea = QtGui.QPushButton(crearSubrutina)
        self.botonLinea.setGeometry(QtCore.QRect(390, 10, 31, 27))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.botonLinea.setFont(font)
        self.botonLinea.setText(_fromUtf8(""))
        icon4 = QtGui.QIcon()
        icon4.addPixmap(QtGui.QPixmap(_fromUtf8("interfaz/iconos/play-1.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.botonLinea.setIcon(icon4)
        self.botonLinea.setIconSize(QtCore.QSize(20, 20))
        self.botonLinea.setObjectName(_fromUtf8("botonLinea"))
        self.botonBorrarOrden = QtGui.QPushButton(crearSubrutina)
        self.botonBorrarOrden.setGeometry(QtCore.QRect(460, 10, 31, 27))
        self.botonBorrarOrden.setText(_fromUtf8(""))
        icon5 = QtGui.QIcon()
        icon5.addPixmap(QtGui.QPixmap(_fromUtf8("interfaz/iconos/delete-1.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.botonBorrarOrden.setIcon(icon5)
        self.botonBorrarOrden.setIconSize(QtCore.QSize(20, 20))
        self.botonBorrarOrden.setCheckable(True)
        self.botonBorrarOrden.setObjectName(_fromUtf8("botonBorrarOrden"))

        self.retranslateUi(crearSubrutina)
        QtCore.QMetaObject.connectSlotsByName(crearSubrutina)

    def retranslateUi(self, crearSubrutina):
        crearSubrutina.setWindowTitle(_translate("crearSubrutina", "Crear Subrutina", None))
        self.arbolComandos.headerItem().setText(0, _translate("crearSubrutina", "Comandos", None))
        __sortingEnabled = self.arbolComandos.isSortingEnabled()
        self.arbolComandos.setSortingEnabled(False)
        self.arbolComandos.topLevelItem(0).setText(0, _translate("crearSubrutina", "CONTROL DE EJES", None))
        self.arbolComandos.topLevelItem(0).child(0).setText(0, _translate("crearSubrutina", "Abrir Pinza", None))
        self.arbolComandos.topLevelItem(0).child(1).setText(0, _translate("crearSubrutina", "Cerrar Pinza", None))
        self.arbolComandos.topLevelItem(0).child(2).setText(0, _translate("crearSubrutina", "Ir a la Posición #", None))
        self.arbolComandos.topLevelItem(0).child(3).setText(0, _translate("crearSubrutina", "Ir según articulaciones", None))
        self.arbolComandos.topLevelItem(0).child(4).setText(0, _translate("crearSubrutina", "Ir según trayectoria XYZ", None))
        self.arbolComandos.topLevelItem(0).child(5).setText(0, _translate("crearSubrutina", "Ir Linealmente a la Posición #", None))
        self.arbolComandos.topLevelItem(0).child(6).setText(0, _translate("crearSubrutina", "Ir Circularmente a la Posición #", None))
        self.arbolComandos.topLevelItem(0).child(7).setText(0, _translate("crearSubrutina", "Memorizar Posición #", None))
        self.arbolComandos.topLevelItem(1).setText(0, _translate("crearSubrutina", "PROGRAMA FLUJO", None))
        self.arbolComandos.topLevelItem(1).child(0).setText(0, _translate("crearSubrutina", "Esperar", None))
        self.arbolComandos.topLevelItem(1).child(1).setText(0, _translate("crearSubrutina", "Comentario", None))
        self.arbolComandos.topLevelItem(1).child(2).setText(0, _translate("crearSubrutina", "Poner Variable", None))
        self.arbolComandos.setSortingEnabled(__sortingEnabled)
        item = self.tablaOrdenes.verticalHeaderItem(0)
        item.setText(_translate("crearSubrutina", "1", None))
        item = self.tablaOrdenes.horizontalHeaderItem(0)
        item.setText(_translate("crearSubrutina", "Instrucciones", None))
        __sortingEnabled = self.tablaOrdenes.isSortingEnabled()
        self.tablaOrdenes.setSortingEnabled(False)
        self.tablaOrdenes.setSortingEnabled(__sortingEnabled)
