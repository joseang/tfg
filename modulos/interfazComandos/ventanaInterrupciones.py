# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ventanaInterrupciones.ui'
#
# Created: Mon May 30 07:38:02 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Interrupciones(object):
    def setupUi(self, Interrupciones):
        Interrupciones.setObjectName(_fromUtf8("Interrupciones"))
        Interrupciones.resize(232, 299)
        Interrupciones.setMinimumSize(QtCore.QSize(232, 299))
        Interrupciones.setMaximumSize(QtCore.QSize(232, 299))
        self.verticalLayoutWidget = QtGui.QWidget(Interrupciones)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(20, 100, 191, 62))
        self.verticalLayoutWidget.setObjectName(_fromUtf8("verticalLayoutWidget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setMargin(0)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.label_2 = QtGui.QLabel(self.verticalLayoutWidget)
        self.label_2.setMinimumSize(QtCore.QSize(130, 27))
        self.label_2.setMaximumSize(QtCore.QSize(130, 27))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.verticalLayout.addWidget(self.label_2)
        self.listaSubrutinas = QtGui.QComboBox(self.verticalLayoutWidget)
        self.listaSubrutinas.setMinimumSize(QtCore.QSize(0, 27))
        self.listaSubrutinas.setMaximumSize(QtCore.QSize(16777215, 27))
        self.listaSubrutinas.setObjectName(_fromUtf8("listaSubrutinas"))
        self.verticalLayout.addWidget(self.listaSubrutinas)
        self.botonAceptar = QtGui.QPushButton(Interrupciones)
        self.botonAceptar.setGeometry(QtCore.QRect(70, 250, 98, 27))
        self.botonAceptar.setObjectName(_fromUtf8("botonAceptar"))
        self.radioActivar = QtGui.QRadioButton(Interrupciones)
        self.radioActivar.setGeometry(QtCore.QRect(20, 180, 81, 22))
        self.radioActivar.setChecked(True)
        self.radioActivar.setObjectName(_fromUtf8("radioActivar"))
        self.radioDesactivar = QtGui.QRadioButton(Interrupciones)
        self.radioDesactivar.setGeometry(QtCore.QRect(20, 210, 101, 22))
        self.radioDesactivar.setObjectName(_fromUtf8("radioDesactivar"))
        self.verticalLayoutWidget_2 = QtGui.QWidget(Interrupciones)
        self.verticalLayoutWidget_2.setGeometry(QtCore.QRect(20, 20, 191, 66))
        self.verticalLayoutWidget_2.setObjectName(_fromUtf8("verticalLayoutWidget_2"))
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.verticalLayoutWidget_2)
        self.verticalLayout_2.setMargin(0)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.label = QtGui.QLabel(self.verticalLayoutWidget_2)
        self.label.setMinimumSize(QtCore.QSize(85, 27))
        self.label.setMaximumSize(QtCore.QSize(85, 27))
        self.label.setObjectName(_fromUtf8("label"))
        self.verticalLayout_2.addWidget(self.label)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.listaTipoSensores = QtGui.QComboBox(self.verticalLayoutWidget_2)
        self.listaTipoSensores.setMinimumSize(QtCore.QSize(0, 27))
        self.listaTipoSensores.setMaximumSize(QtCore.QSize(16777215, 27))
        self.listaTipoSensores.setObjectName(_fromUtf8("listaTipoSensores"))
        self.listaTipoSensores.addItem(_fromUtf8(""))
        self.listaTipoSensores.addItem(_fromUtf8(""))
        self.horizontalLayout.addWidget(self.listaTipoSensores)
        self.numeroSensor = QtGui.QLineEdit(self.verticalLayoutWidget_2)
        self.numeroSensor.setMinimumSize(QtCore.QSize(55, 29))
        self.numeroSensor.setMaximumSize(QtCore.QSize(55, 29))
        self.numeroSensor.setObjectName(_fromUtf8("numeroSensor"))
        self.horizontalLayout.addWidget(self.numeroSensor)
        self.verticalLayout_2.addLayout(self.horizontalLayout)

        self.retranslateUi(Interrupciones)
        QtCore.QMetaObject.connectSlotsByName(Interrupciones)

    def retranslateUi(self, Interrupciones):
        Interrupciones.setWindowTitle(_translate("Interrupciones", "Interrupciones", None))
        self.label_2.setText(_translate("Interrupciones", "Ejecutar subrutina:", None))
        self.botonAceptar.setText(_translate("Interrupciones", "Aceptar", None))
        self.radioActivar.setText(_translate("Interrupciones", "Activar", None))
        self.radioDesactivar.setText(_translate("Interrupciones", "Desactivar", None))
        self.label.setText(_translate("Interrupciones", "Activar con:", None))
        self.listaTipoSensores.setItemText(0, _translate("Interrupciones", "Contacto", None))
        self.listaTipoSensores.setItemText(1, _translate("Interrupciones", "Presencia", None))

