# -*- coding: utf-8 -*-
  
# Form implementation generated from reading ui file 'ventanaSeleccionarSubrutina.ui'
#
# Created: Tue Apr 12 23:40:55 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_SeleccionarSubrutina(object):
    def setupUi(self, SeleccionarSubrutina):
        SeleccionarSubrutina.setObjectName(_fromUtf8("SeleccionarSubrutina"))
        SeleccionarSubrutina.resize(232, 117)
        SeleccionarSubrutina.setMinimumSize(QtCore.QSize(232, 117))
        SeleccionarSubrutina.setMaximumSize(QtCore.QSize(232, 117))
        self.nombresSubrutinas = QtGui.QComboBox(SeleccionarSubrutina)
        self.nombresSubrutinas.setGeometry(QtCore.QRect(20, 20, 191, 27))
        self.nombresSubrutinas.setObjectName(_fromUtf8("nombresSubrutinas"))
        self.botonAceptar = QtGui.QPushButton(SeleccionarSubrutina)
        self.botonAceptar.setGeometry(QtCore.QRect(70, 70, 98, 27))
        self.botonAceptar.setObjectName(_fromUtf8("botonAceptar"))

        self.retranslateUi(SeleccionarSubrutina)
        QtCore.QMetaObject.connectSlotsByName(SeleccionarSubrutina)

    def retranslateUi(self, SeleccionarSubrutina):
        SeleccionarSubrutina.setWindowTitle(_translate("SeleccionarSubrutina", "Seleccionar Subrutina", None))
        self.botonAceptar.setText(_translate("SeleccionarSubrutina", "Aceptar", None))

