# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ventanaMemorizar.ui'
#
# Created: Mon Dec 14 11:32:12 2015
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MemorizarPosicion(object):
    def setupUi(self, MemorizarPosicion):
        MemorizarPosicion.setObjectName(_fromUtf8("MemorizarPosicion"))
        MemorizarPosicion.resize(293, 102)
        MemorizarPosicion.setMinimumSize(QtCore.QSize(293, 102))
        MemorizarPosicion.setMaximumSize(QtCore.QSize(293, 102))
        self.buttonAceptar = QtGui.QPushButton(MemorizarPosicion)
        self.buttonAceptar.setGeometry(QtCore.QRect(100, 60, 98, 27))
        self.buttonAceptar.setObjectName(_fromUtf8("buttonAceptar"))
        self.horizontalLayoutWidget = QtGui.QWidget(MemorizarPosicion)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(10, 10, 271, 41))
        self.horizontalLayoutWidget.setObjectName(_fromUtf8("horizontalLayoutWidget"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setMargin(0)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label = QtGui.QLabel(self.horizontalLayoutWidget)
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout.addWidget(self.label)
        self.numeroPosicion = QtGui.QComboBox(self.horizontalLayoutWidget)
        self.numeroPosicion.setMinimumSize(QtCore.QSize(70, 27))
        self.numeroPosicion.setMaximumSize(QtCore.QSize(70, 27))
        self.numeroPosicion.setEditable(True)
        self.numeroPosicion.setObjectName(_fromUtf8("numeroPosicion"))
        self.horizontalLayout.addWidget(self.numeroPosicion)

        self.retranslateUi(MemorizarPosicion)
        QtCore.QMetaObject.connectSlotsByName(MemorizarPosicion)

    def retranslateUi(self, MemorizarPosicion):
        MemorizarPosicion.setWindowTitle(_translate("MemorizarPosicion", "Memorizar Posición", None))
        self.buttonAceptar.setText(_translate("MemorizarPosicion", "Aceptar", None))
        self.label.setText(_translate("MemorizarPosicion", "Guardar en posición número:", None))

