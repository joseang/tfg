# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ventanaEtiqueta.ui'
#
# Created: Thu May 19 09:03:40 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Etiqueta(object):
    def setupUi(self, Etiqueta):
        Etiqueta.setObjectName(_fromUtf8("Etiqueta"))
        Etiqueta.resize(239, 121)
        Etiqueta.setMinimumSize(QtCore.QSize(239, 121))
        Etiqueta.setMaximumSize(QtCore.QSize(239, 121))
        self.label = QtGui.QLabel(Etiqueta)
        self.label.setGeometry(QtCore.QRect(90, 10, 64, 17))
        self.label.setObjectName(_fromUtf8("label"))
        self.nombreEtiqueta = QtGui.QLineEdit(Etiqueta)
        self.nombreEtiqueta.setGeometry(QtCore.QRect(20, 40, 201, 29))
        self.nombreEtiqueta.setObjectName(_fromUtf8("nombreEtiqueta"))
        self.buttonAceptar = QtGui.QPushButton(Etiqueta)
        self.buttonAceptar.setGeometry(QtCore.QRect(70, 80, 98, 27))
        self.buttonAceptar.setObjectName(_fromUtf8("buttonAceptar"))

        self.retranslateUi(Etiqueta)
        QtCore.QMetaObject.connectSlotsByName(Etiqueta)

    def retranslateUi(self, Etiqueta):
        Etiqueta.setWindowTitle(_translate("Etiqueta", "Etiqueta", None))
        self.label.setText(_translate("Etiqueta", "Etiqueta:", None))
        self.buttonAceptar.setText(_translate("Etiqueta", "Aceptar", None))

