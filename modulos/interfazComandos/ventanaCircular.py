# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ventanaCircular.ui'
#
# Created: Mon Dec 14 10:22:02 2015
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Circular(object):
    def setupUi(self, Circular):
        Circular.setObjectName(_fromUtf8("Circular"))
        Circular.resize(205, 261)
        Circular.setMinimumSize(QtCore.QSize(205, 261))
        Circular.setMaximumSize(QtCore.QSize(205, 261))
        self.horizontalLayoutWidget = QtGui.QWidget(Circular)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(20, 20, 161, 31))
        self.horizontalLayoutWidget.setObjectName(_fromUtf8("horizontalLayoutWidget"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setMargin(0)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label = QtGui.QLabel(self.horizontalLayoutWidget)
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout.addWidget(self.label)
        self.posicionFinal = QtGui.QComboBox(self.horizontalLayoutWidget)
        self.posicionFinal.setMinimumSize(QtCore.QSize(51, 27))
        self.posicionFinal.setMaximumSize(QtCore.QSize(51, 27))
        self.posicionFinal.setObjectName(_fromUtf8("posicionFinal"))
        self.horizontalLayout.addWidget(self.posicionFinal)
        self.horizontalLayoutWidget_2 = QtGui.QWidget(Circular)
        self.horizontalLayoutWidget_2.setGeometry(QtCore.QRect(20, 60, 161, 31))
        self.horizontalLayoutWidget_2.setObjectName(_fromUtf8("horizontalLayoutWidget_2"))
        self.horizontalLayout_2 = QtGui.QHBoxLayout(self.horizontalLayoutWidget_2)
        self.horizontalLayout_2.setMargin(0)
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.label_2 = QtGui.QLabel(self.horizontalLayoutWidget_2)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.horizontalLayout_2.addWidget(self.label_2)
        self.posicionMedia = QtGui.QComboBox(self.horizontalLayoutWidget_2)
        self.posicionMedia.setMinimumSize(QtCore.QSize(51, 27))
        self.posicionMedia.setMaximumSize(QtCore.QSize(51, 27))
        self.posicionMedia.setObjectName(_fromUtf8("posicionMedia"))
        self.horizontalLayout_2.addWidget(self.posicionMedia)
        self.buttonAceptar = QtGui.QPushButton(Circular)
        self.buttonAceptar.setGeometry(QtCore.QRect(50, 220, 98, 27))
        self.buttonAceptar.setObjectName(_fromUtf8("buttonAceptar"))
        self.horizontalLayoutWidget_3 = QtGui.QWidget(Circular)
        self.horizontalLayoutWidget_3.setGeometry(QtCore.QRect(20, 120, 161, 31))
        self.horizontalLayoutWidget_3.setObjectName(_fromUtf8("horizontalLayoutWidget_3"))
        self.horizontalLayout_3 = QtGui.QHBoxLayout(self.horizontalLayoutWidget_3)
        self.horizontalLayout_3.setMargin(0)
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.radioTiempo = QtGui.QRadioButton(self.horizontalLayoutWidget_3)
        self.radioTiempo.setChecked(True)
        self.radioTiempo.setObjectName(_fromUtf8("radioTiempo"))
        self.buttonGroup = QtGui.QButtonGroup(Circular)
        self.buttonGroup.setObjectName(_fromUtf8("buttonGroup"))
        self.buttonGroup.addButton(self.radioTiempo)
        self.horizontalLayout_3.addWidget(self.radioTiempo)
        self.valorTiempo = QtGui.QLineEdit(self.horizontalLayoutWidget_3)
        self.valorTiempo.setMinimumSize(QtCore.QSize(52, 27))
        self.valorTiempo.setMaximumSize(QtCore.QSize(52, 27))
        self.valorTiempo.setObjectName(_fromUtf8("valorTiempo"))
        self.horizontalLayout_3.addWidget(self.valorTiempo)
        self.horizontalLayoutWidget_4 = QtGui.QWidget(Circular)
        self.horizontalLayoutWidget_4.setGeometry(QtCore.QRect(20, 160, 166, 31))
        self.horizontalLayoutWidget_4.setObjectName(_fromUtf8("horizontalLayoutWidget_4"))
        self.horizontalLayout_4 = QtGui.QHBoxLayout(self.horizontalLayoutWidget_4)
        self.horizontalLayout_4.setMargin(0)
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))
        self.radioVelocidad = QtGui.QRadioButton(self.horizontalLayoutWidget_4)
        self.radioVelocidad.setObjectName(_fromUtf8("radioVelocidad"))
        self.buttonGroup.addButton(self.radioVelocidad)
        self.horizontalLayout_4.addWidget(self.radioVelocidad)
        self.valorVelocidad = QtGui.QSpinBox(self.horizontalLayoutWidget_4)
        self.valorVelocidad.setMinimumSize(QtCore.QSize(63, 27))
        self.valorVelocidad.setMaximumSize(QtCore.QSize(63, 27))
        self.valorVelocidad.setMinimum(1)
        self.valorVelocidad.setMaximum(10)
        self.valorVelocidad.setObjectName(_fromUtf8("valorVelocidad"))
        self.horizontalLayout_4.addWidget(self.valorVelocidad)

        self.retranslateUi(Circular)
        QtCore.QMetaObject.connectSlotsByName(Circular)

    def retranslateUi(self, Circular):
        Circular.setWindowTitle(_translate("Circular", "Circular", None))
        self.label.setText(_translate("Circular", "Posición Final:", None))
        self.label_2.setText(_translate("Circular", "Vía Posición:", None))
        self.buttonAceptar.setText(_translate("Circular", "Aceptar", None))
        self.radioTiempo.setText(_translate("Circular", "Tiempo [s]", None))
        self.radioVelocidad.setText(_translate("Circular", "Velocidad", None))
