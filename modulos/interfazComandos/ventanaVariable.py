# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ventanaVariable.ui'
#
# Created: Wed May 18 21:43:42 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Variable(object):
    def setupUi(self, Variable):
        Variable.setObjectName(_fromUtf8("Variable"))
        Variable.resize(255, 137)
        Variable.setMinimumSize(QtCore.QSize(255, 137))
        Variable.setMaximumSize(QtCore.QSize(255, 137))
        self.buttonAceptar = QtGui.QPushButton(Variable)
        self.buttonAceptar.setGeometry(QtCore.QRect(80, 100, 81, 27))
        self.buttonAceptar.setMinimumSize(QtCore.QSize(81, 27))
        self.buttonAceptar.setMaximumSize(QtCore.QSize(81, 27))
        self.buttonAceptar.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.buttonAceptar.setObjectName(_fromUtf8("buttonAceptar"))
        self.horizontalLayoutWidget = QtGui.QWidget(Variable)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(10, 50, 231, 41))
        self.horizontalLayoutWidget.setObjectName(_fromUtf8("horizontalLayoutWidget"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setMargin(0)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label = QtGui.QLabel(self.horizontalLayoutWidget)
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout.addWidget(self.label)
        self.valorVariable = QtGui.QLineEdit(self.horizontalLayoutWidget)
        self.valorVariable.setObjectName(_fromUtf8("valorVariable"))
        self.horizontalLayout.addWidget(self.valorVariable)
        self.horizontalLayoutWidget_2 = QtGui.QWidget(Variable)
        self.horizontalLayoutWidget_2.setGeometry(QtCore.QRect(10, 10, 231, 31))
        self.horizontalLayoutWidget_2.setObjectName(_fromUtf8("horizontalLayoutWidget_2"))
        self.horizontalLayout_2 = QtGui.QHBoxLayout(self.horizontalLayoutWidget_2)
        self.horizontalLayout_2.setMargin(0)
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.label_2 = QtGui.QLabel(self.horizontalLayoutWidget_2)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.horizontalLayout_2.addWidget(self.label_2)
        self.nombreVariable = QtGui.QLineEdit(self.horizontalLayoutWidget_2)
        self.nombreVariable.setObjectName(_fromUtf8("nombreVariable"))
        self.horizontalLayout_2.addWidget(self.nombreVariable)

        self.retranslateUi(Variable)
        QtCore.QMetaObject.connectSlotsByName(Variable)

    def retranslateUi(self, Variable):
        Variable.setWindowTitle(_translate("Variable", "Variable", None))
        self.buttonAceptar.setText(_translate("Variable", "Aceptar", None))
        self.label.setText(_translate("Variable", "Valor:", None))
        self.label_2.setText(_translate("Variable", "Nombre:", None))

