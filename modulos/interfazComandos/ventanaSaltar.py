# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ventanaSaltar.ui'
#
# Created: Wed Jul 15 23:24:58 2015
#      by: PyQt4 UI code generator 4.9.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_SaltarA(object):
    def setupUi(self, SaltarA):
        SaltarA.setObjectName(_fromUtf8("SaltarA"))
        SaltarA.resize(212, 134)
        SaltarA.setMinimumSize(QtCore.QSize(212, 134))
        SaltarA.setMaximumSize(QtCore.QSize(212, 134))
        self.buttonAceptar = QtGui.QPushButton(SaltarA)
        self.buttonAceptar.setGeometry(QtCore.QRect(60, 80, 91, 27))
        self.buttonAceptar.setObjectName(_fromUtf8("buttonAceptar"))
        self.nombreSalto = QtGui.QComboBox(SaltarA)
        self.nombreSalto.setGeometry(QtCore.QRect(30, 30, 151, 27))
        self.nombreSalto.setMouseTracking(False)
        self.nombreSalto.setAcceptDrops(False)
        self.nombreSalto.setEditable(False)
        self.nombreSalto.setObjectName(_fromUtf8("nombreSalto"))

        self.retranslateUi(SaltarA)
        QtCore.QMetaObject.connectSlotsByName(SaltarA)

    def retranslateUi(self, SaltarA):
        SaltarA.setWindowTitle(QtGui.QApplication.translate("SaltarA", "Saltar", None, QtGui.QApplication.UnicodeUTF8))
        self.buttonAceptar.setText(QtGui.QApplication.translate("SaltarA", "Aceptar", None, QtGui.QApplication.UnicodeUTF8))

