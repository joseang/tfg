# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'esperar.ui'
#
# Created: Wed Jul 15 20:34:57 2015
#      by: PyQt4 UI code generator 4.9.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_Esperar(object):
    def setupUi(self, Esperar):
        Esperar.setObjectName(_fromUtf8("Esperar"))
        Esperar.resize(222, 152)
        Esperar.setMinimumSize(QtCore.QSize(222, 152))
        Esperar.setMaximumSize(QtCore.QSize(222, 152))
        self.verticalLayoutWidget = QtGui.QWidget(Esperar)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(0, 0, 221, 151))
        self.verticalLayoutWidget.setObjectName(_fromUtf8("verticalLayoutWidget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setMargin(0)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label = QtGui.QLabel(self.verticalLayoutWidget)
        self.label.setMinimumSize(QtCore.QSize(129, 31))
        self.label.setMaximumSize(QtCore.QSize(129, 31))
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout.addWidget(self.label)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.valorTiempo = QtGui.QSpinBox(self.verticalLayoutWidget)
        self.valorTiempo.setMinimumSize(QtCore.QSize(60, 27))
        self.valorTiempo.setMaximumSize(QtCore.QSize(60, 27))
        self.valorTiempo.setObjectName(_fromUtf8("valorTiempo"))
        self.horizontalLayout_2.addWidget(self.valorTiempo)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.buttonAceptar = QtGui.QPushButton(self.verticalLayoutWidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.buttonAceptar.sizePolicy().hasHeightForWidth())
        self.buttonAceptar.setSizePolicy(sizePolicy)
        self.buttonAceptar.setMinimumSize(QtCore.QSize(81, 27))
        self.buttonAceptar.setMaximumSize(QtCore.QSize(81, 27))
        self.buttonAceptar.setObjectName(_fromUtf8("buttonAceptar"))
        self.horizontalLayout_3.addWidget(self.buttonAceptar)
        self.verticalLayout.addLayout(self.horizontalLayout_3)

        self.retranslateUi(Esperar)
        QtCore.QMetaObject.connectSlotsByName(Esperar)

    def retranslateUi(self, Esperar):
        Esperar.setWindowTitle(QtGui.QApplication.translate("Esperar", "Esperar", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("Esperar", "Tiempo [segundos]", None, QtGui.QApplication.UnicodeUTF8))
        self.buttonAceptar.setText(QtGui.QApplication.translate("Esperar", "Aceptar", None, QtGui.QApplication.UnicodeUTF8))

