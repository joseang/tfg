# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ventanaLineal.ui'
#
# Created: Mon Dec 14 10:19:12 2015
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Lineal(object):
    def setupUi(self, Lineal):
        Lineal.setObjectName(_fromUtf8("Lineal"))
        Lineal.resize(205, 217)
        Lineal.setMinimumSize(QtCore.QSize(205, 217))
        Lineal.setMaximumSize(QtCore.QSize(205, 217))
        self.horizontalLayoutWidget = QtGui.QWidget(Lineal)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(20, 20, 161, 31))
        self.horizontalLayoutWidget.setObjectName(_fromUtf8("horizontalLayoutWidget"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setMargin(0)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label = QtGui.QLabel(self.horizontalLayoutWidget)
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout.addWidget(self.label)
        self.numeroPosicion = QtGui.QComboBox(self.horizontalLayoutWidget)
        self.numeroPosicion.setMinimumSize(QtCore.QSize(51, 27))
        self.numeroPosicion.setMaximumSize(QtCore.QSize(51, 27))
        self.numeroPosicion.setObjectName(_fromUtf8("numeroPosicion"))
        self.horizontalLayout.addWidget(self.numeroPosicion)
        self.buttonAceptar = QtGui.QPushButton(Lineal)
        self.buttonAceptar.setGeometry(QtCore.QRect(50, 170, 98, 27))
        self.buttonAceptar.setObjectName(_fromUtf8("buttonAceptar"))
        self.horizontalLayoutWidget_3 = QtGui.QWidget(Lineal)
        self.horizontalLayoutWidget_3.setGeometry(QtCore.QRect(20, 70, 166, 31))
        self.horizontalLayoutWidget_3.setObjectName(_fromUtf8("horizontalLayoutWidget_3"))
        self.horizontalLayout_3 = QtGui.QHBoxLayout(self.horizontalLayoutWidget_3)
        self.horizontalLayout_3.setMargin(0)
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.radioTiempo = QtGui.QRadioButton(self.horizontalLayoutWidget_3)
        self.radioTiempo.setChecked(True)
        self.radioTiempo.setObjectName(_fromUtf8("radioTiempo"))
        self.buttonGroup = QtGui.QButtonGroup(Lineal)
        self.buttonGroup.setObjectName(_fromUtf8("buttonGroup"))
        self.buttonGroup.addButton(self.radioTiempo)
        self.horizontalLayout_3.addWidget(self.radioTiempo)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem)
        self.valorTiempo = QtGui.QLineEdit(self.horizontalLayoutWidget_3)
        self.valorTiempo.setMinimumSize(QtCore.QSize(52, 27))
        self.valorTiempo.setMaximumSize(QtCore.QSize(52, 27))
        self.valorTiempo.setObjectName(_fromUtf8("valorTiempo"))
        self.horizontalLayout_3.addWidget(self.valorTiempo)
        self.horizontalLayoutWidget_4 = QtGui.QWidget(Lineal)
        self.horizontalLayoutWidget_4.setGeometry(QtCore.QRect(20, 110, 166, 31))
        self.horizontalLayoutWidget_4.setObjectName(_fromUtf8("horizontalLayoutWidget_4"))
        self.horizontalLayout_4 = QtGui.QHBoxLayout(self.horizontalLayoutWidget_4)
        self.horizontalLayout_4.setMargin(0)
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))
        self.radioVelocidad = QtGui.QRadioButton(self.horizontalLayoutWidget_4)
        self.radioVelocidad.setObjectName(_fromUtf8("radioVelocidad"))
        self.buttonGroup.addButton(self.radioVelocidad)
        self.horizontalLayout_4.addWidget(self.radioVelocidad)
        self.valorVelocidad = QtGui.QSpinBox(self.horizontalLayoutWidget_4)
        self.valorVelocidad.setMinimumSize(QtCore.QSize(63, 27))
        self.valorVelocidad.setMaximumSize(QtCore.QSize(63, 27))
        self.valorVelocidad.setMinimum(1)
        self.valorVelocidad.setMaximum(10)
        self.valorVelocidad.setObjectName(_fromUtf8("valorVelocidad"))
        self.horizontalLayout_4.addWidget(self.valorVelocidad)

        self.retranslateUi(Lineal)
        QtCore.QMetaObject.connectSlotsByName(Lineal)

    def retranslateUi(self, Lineal):
        Lineal.setWindowTitle(_translate("Lineal", "Lineal", None))
        self.label.setText(_translate("Lineal", "Posición Final:", None))
        self.buttonAceptar.setText(_translate("Lineal", "Aceptar", None))
        self.radioTiempo.setText(_translate("Lineal", "Tiempo [s]", None))
        self.radioVelocidad.setText(_translate("Lineal", "Velocidad", None))

