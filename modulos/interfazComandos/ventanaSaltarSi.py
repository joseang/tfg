# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ventanaSaltarSi.ui'
#
# Created: Wed May 18 11:13:25 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_SiSalta(object):
    def setupUi(self, SiSalta):
        SiSalta.setObjectName(_fromUtf8("SiSalta"))
        SiSalta.resize(328, 154)
        SiSalta.setMinimumSize(QtCore.QSize(328, 154))
        SiSalta.setMaximumSize(QtCore.QSize(328, 154))
        self.horizontalLayoutWidget = QtGui.QWidget(SiSalta)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(10, 10, 301, 31))
        self.horizontalLayoutWidget.setObjectName(_fromUtf8("horizontalLayoutWidget"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setMargin(0)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label = QtGui.QLabel(self.horizontalLayoutWidget)
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout.addWidget(self.label)
        self.nombreVariable = QtGui.QLineEdit(self.horizontalLayoutWidget)
        self.nombreVariable.setMinimumSize(QtCore.QSize(100, 27))
        self.nombreVariable.setMaximumSize(QtCore.QSize(100, 27))
        self.nombreVariable.setObjectName(_fromUtf8("nombreVariable"))
        self.horizontalLayout.addWidget(self.nombreVariable)
        self.condicion = QtGui.QComboBox(self.horizontalLayoutWidget)
        self.condicion.setMinimumSize(QtCore.QSize(55, 27))
        self.condicion.setMaximumSize(QtCore.QSize(55, 27))
        self.condicion.setObjectName(_fromUtf8("condicion"))
        self.condicion.addItem(_fromUtf8(""))
        self.condicion.addItem(_fromUtf8(""))
        self.condicion.addItem(_fromUtf8(""))
        self.condicion.addItem(_fromUtf8(""))
        self.condicion.addItem(_fromUtf8(""))
        self.condicion.addItem(_fromUtf8(""))
        self.horizontalLayout.addWidget(self.condicion)
        self.valorVariable = QtGui.QLineEdit(self.horizontalLayoutWidget)
        self.valorVariable.setMinimumSize(QtCore.QSize(100, 27))
        self.valorVariable.setMaximumSize(QtCore.QSize(100, 27))
        self.valorVariable.setObjectName(_fromUtf8("valorVariable"))
        self.horizontalLayout.addWidget(self.valorVariable)
        self.horizontalLayoutWidget_2 = QtGui.QWidget(SiSalta)
        self.horizontalLayoutWidget_2.setGeometry(QtCore.QRect(10, 60, 201, 31))
        self.horizontalLayoutWidget_2.setObjectName(_fromUtf8("horizontalLayoutWidget_2"))
        self.horizontalLayout_2 = QtGui.QHBoxLayout(self.horizontalLayoutWidget_2)
        self.horizontalLayout_2.setMargin(0)
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.label_2 = QtGui.QLabel(self.horizontalLayoutWidget_2)
        self.label_2.setMinimumSize(QtCore.QSize(60, 27))
        self.label_2.setMaximumSize(QtCore.QSize(60, 27))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.horizontalLayout_2.addWidget(self.label_2)
        self.nombreSalto = QtGui.QComboBox(self.horizontalLayoutWidget_2)
        self.nombreSalto.setMinimumSize(QtCore.QSize(55, 27))
        self.nombreSalto.setMaximumSize(QtCore.QSize(200, 27))
        self.nombreSalto.setObjectName(_fromUtf8("nombreSalto"))
        self.horizontalLayout_2.addWidget(self.nombreSalto)
        self.buttonAceptar = QtGui.QPushButton(SiSalta)
        self.buttonAceptar.setGeometry(QtCore.QRect(120, 110, 98, 27))
        self.buttonAceptar.setObjectName(_fromUtf8("buttonAceptar"))

        self.retranslateUi(SiSalta)
        QtCore.QMetaObject.connectSlotsByName(SiSalta)

    def retranslateUi(self, SiSalta):
        SiSalta.setWindowTitle(_translate("SiSalta", "Si...Saltar", None))
        self.label.setText(_translate("SiSalta", "Si", None))
        self.condicion.setItemText(0, _translate("SiSalta", "==", None))
        self.condicion.setItemText(1, _translate("SiSalta", "!=", None))
        self.condicion.setItemText(2, _translate("SiSalta", ">", None))
        self.condicion.setItemText(3, _translate("SiSalta", ">=", None))
        self.condicion.setItemText(4, _translate("SiSalta", "<", None))
        self.condicion.setItemText(5, _translate("SiSalta", "<=", None))
        self.label_2.setText(_translate("SiSalta", "Saltar a: ", None))
        self.buttonAceptar.setText(_translate("SiSalta", "Aceptar", None))

