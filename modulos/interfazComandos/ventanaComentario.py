# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ventanaComentario.ui'
#
# Created: Mon Dec 14 11:46:01 2015
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Comentario(object):
    def setupUi(self, Comentario):
        Comentario.setObjectName(_fromUtf8("Comentario"))
        Comentario.resize(344, 124)
        Comentario.setMinimumSize(QtCore.QSize(344, 124))
        Comentario.setMaximumSize(QtCore.QSize(344, 124))
        self.buttonAceptar = QtGui.QPushButton(Comentario)
        self.buttonAceptar.setGeometry(QtCore.QRect(130, 80, 91, 27))
        self.buttonAceptar.setObjectName(_fromUtf8("buttonAceptar"))
        self.verticalLayoutWidget = QtGui.QWidget(Comentario)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(10, 10, 321, 61))
        self.verticalLayoutWidget.setObjectName(_fromUtf8("verticalLayoutWidget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setMargin(0)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.label = QtGui.QLabel(self.verticalLayoutWidget)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName(_fromUtf8("label"))
        self.verticalLayout.addWidget(self.label)
        self.textoComentario = QtGui.QLineEdit(self.verticalLayoutWidget)
        self.textoComentario.setObjectName(_fromUtf8("textoComentario"))
        self.verticalLayout.addWidget(self.textoComentario)

        self.retranslateUi(Comentario)
        QtCore.QMetaObject.connectSlotsByName(Comentario)

    def retranslateUi(self, Comentario):
        Comentario.setWindowTitle(_translate("Comentario", "Comentario", None))
        self.buttonAceptar.setText(_translate("Comentario", "Aceptar", None))
        self.label.setText(_translate("Comentario", "Comentario:", None))

