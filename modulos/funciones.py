#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os, sys, glob, time
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from control_msgs.msg import FollowJointTrajectoryActionGoal
import numpy as np
sys.path.append('../cinematica')
from cinematica.kinematic_functions import *
import rospy
from operator import sub
from cinematica.auxiliar_cinematica import *
from std_srvs.srv import Trigger
from rly02 import send_command
from gazebo_msgs.msg import ModelStates, ContactsState

from path_absoluto import obtenerDireccionAbsoluta
direccionAbsoluta=obtenerDireccionAbsoluta()

def enviarPosicion(articulaciones,tiempo):
	tiempo=round(tiempo,2)
	pub = rospy.Publisher('/arm/joint_trajectory_controller/follow_joint_trajectory/goal',FollowJointTrajectoryActionGoal,queue_size=15)
	final=FollowJointTrajectoryActionGoal()
	final.goal.trajectory.joint_names=['arm_1_joint', 'arm_2_joint', 'arm_3_joint', 'arm_4_joint', 'arm_5_joint', 'arm_6_joint']
	final.goal.trajectory.points.append(JointTrajectoryPoint(positions = articulaciones, velocities = [0, 0, 0, 0, 0, 0],time_from_start = rospy.Duration(tiempo)))
	pub.publish(final)

''' revisa si esta el boton arrancarRobot pulsado '''
def ejecutandose():
	archivo=open(direccionAbsoluta+'/data/ejecutandose','r')
	estado=archivo.read()
	if estado=="True":
		return True
	elif estado=="False":
		return False
	archivo.close()

def funcionParaPararElRobotYLaPinza():
	pararPinza()
	pararRobot()

''' Ordenes para resetear todo '''
def borrarTodo():
	reiniciarArticulacionesActuales()
	borrarVariables()
	borrarSubrutinas()
	borrarEtiquetas()
	reiniciarIndiceSubrutinas()
	borrarPosiciones()
	escribirStop(False)

def reiniciarArticulacionesActuales():
	archivoArticulaciones=open(direccionAbsoluta+"/data/articulacionesActuales","w")
	archivoArticulaciones.write("0 0 0 0 0 0")
	archivoArticulaciones.close()

def borrarPosiciones():
	posiciones=obtenerNumeroPosicionesDisponibles()
	for i in posiciones:
		os.remove(direccionAbsoluta+'/posiciones/P'+str(i)+'.txt')

def borrarEtiquetas():
	try:
		archivo=open(direccionAbsoluta+"/data/etiquetasDisponibles",'w')
		archivo.truncate()
		archivo.close()
	except:
		return

def reiniciarIndiceSubrutinas():
	indiceSub=0
	direccionIndice=direccionAbsoluta+'/data/indice_subrutina'
	archivoIndice=open(direccionIndice,'w')
	archivoIndice.write(str(indiceSub))

def borrarVariables():
	try:
		archivo=open(direccionAbsoluta+"/data/variables",'w+')
		archivo.truncate()
		archivo.close()
	except:
		return

def borrarSubrutinas():
	for filename in glob.glob(direccionAbsoluta+"/data/subrutina_*"):
		os.remove(filename)

''' Evita que sea th=[0,0,0,0,0,0] '''
def comprobarHome(th):
	count_th=0
	for i in range(len(th)):
		if round(th[i],2)==0:
			count_th+=1
	if count_th==6:
		return True
	else:
		return False

''' indica el estado del robot '''
def esRobotSimulacion():
	tipo=open(direccionAbsoluta+"/data/tipoEjecucion","r")
	estado=tipo.readlines()[0]
	if estado=='simulacion':
		return True
	else:
		return False

def esRobotReal():
	tipo=open(direccionAbsoluta+"/data/tipoEjecucion", "r")
	estado=tipo.readlines()[0]
	if estado=='robot_real':
		return True
	else:
		return False

def obtenerNumeroPosicionesDisponibles():
	listaPosiciones=[]
	for file in os.listdir(direccionAbsoluta+"/posiciones"):
		if file.endswith(".txt"):
			indice=file[1:].index('.')+1
			listaPosiciones.append((int(file[1:indice])))
	listaPosiciones.sort()
	listaPosiciones=map(str,listaPosiciones)
	return listaPosiciones

def obtenerDatosDePosicion(numeroPosicion):
	try:
		numeroPosicion=int(numeroPosicion)
	except Exception:
		mensaje=u'Solo se pueden introducir números'
		QtGui.QMessageBox.about(self, 'Error',mensaje)
	archivo=direccionAbsoluta+'/posiciones/P'+str(numeroPosicion)+".txt"
	return np.loadtxt(archivo) # abs_rel n_pos_ref x y z roll pitch yaw

def calcularPosicionConArticulacion(articulaciones):
	matHomo=fkineArm(articulaciones)
	[x, y, z]=list(np.array(matHomo[0:3,3]))
	matRotacion=matHomo[0:3,0:3]
	[roll, pitch, yaw]=obtenerAngulosDesdeMatRot(matRotacion)
	roll, pitch, yaw = np.rad2deg(roll), np.rad2deg(pitch), np.rad2deg(yaw)
	return [x, y, z, roll, pitch, yaw]

''' devuelve los datos de la posicion introducida '''
def obtenerPosicionSeleccionada(numeroPosicion):
	numeroPosicion=int(numeroPosicion)
	archivo=direccionAbsoluta+'/posiciones/P'+str(numeroPosicion)+".txt"
	datos=np.loadtxt(archivo)
	if datos[0]==1:
		return datos[2:] # abs_rel n_pos_ref x y z pitch roll
	elif datos[0]:
		if numeroPosicion==posicionReferencia:
			QtGui.QMessageBox.about(self, 'Error','Las posiciones no pueden ser las mismas')
		else:
			archivoPosicion=direccionAbsoluta+"/posiciones/P"+numeroPosicion+".txt"
			archivoReferencia=str(direccionAbsoluta+"/posiciones/P"+posicionReferencia+".txt")
			datosReferencia=np.loadtxt(archivoReferencia)
			x_total=datosReferencia[2]+x
			y_total=datosReferencia[3]+y
			z_total=datosReferencia[4]+z
			roll_total=datosReferencia[5]+roll
			pitch_total=datosReferencia[6]+pitch
			yaw_total=datosReferencia[7]+yaw
			return x_total, y_total, z_total, roll_total, pitch_total, yaw_total

def escribirPosicionEnArchivo(direccion, tipo, posicion, posicionReferencia=0): #datos:[abs/rel,x,y,z,pitch,roll]
	fo=open(direccion,"w+")
	fo.truncate()
	if tipo=='absoluta' or tipo=='absoluto':
		tipoAbs=1
	else:
		tipoAbs=0
	posicion=str(tipoAbs)+" "+str(posicionReferencia)+" "+str(posicion[0])+" "+str(posicion[1])+" "+str(posicion[2])+" "+str(posicion[3])+" "+str(posicion[4])+" "+str(posicion[5])
	fo.write(posicion)
	fo.close()

def funcionParaRevisarSiSeHaPulsadoSTOP():
	fileStopRev=open(direccionAbsoluta+"/data/stop.txt",'r')
	contenidoStop=fileStopRev.readlines()
	if contenidoStop[0]=='True':
		fileStopRev.close()
		fileStopRev=open(direccionAbsoluta+"/data/stop.txt",'w')
		fileStopRev.write("false")
		fileStopRev.close()
		return True
	return False

def escribirStop(estado):
	print "Nuevo estado de STOP: ", estado
	fileStop=open(direccionAbsoluta+"/data/stop.txt",'w+')
	fileStop.write(str(estado))
	fileStop.close()


# TODO: en el robot de Gazebo esto no funciona (creo)
def pararRobot():
	if esRobotReal():
		print "Parar esRobotReal"
		service_halt=rospy.ServiceProxy('/arm/driver/halt', Trigger)
		service_halt()
		time.sleep(0.5)
		service_recover=rospy.ServiceProxy('/arm/driver/recover', Trigger)
		service_recover()
	elif esRobotSimulacion():
		th=leerArticulacionesActuales()
		enviarPosicion(th,1)

# TODO: en el robot de Gazebo esto no funciona (creo)
def pararPinza():
	if esRobotReal():
		try:
			send_command(0x6F)
			send_command(0x70)
		except:
			print "No debe estar conectada"
	if esRobotSimulacion():
		service_halt=rospy.ServiceProxy('/gripper/driver/halt', Trigger)
		service_halt()
		time.sleep(0.2)
		service_recover=rospy.ServiceProxy('/gripper/driver/recover', Trigger)
		service_recover()

def joint_actual(data):
	escribirArticulacionesEnArchivo(data.position)

def escribirArticulacionesEnArchivo(th):
	archivoArticulaciones=open(direccionAbsoluta+"/data/articulacionesActuales","w")
	for i in range(len(th)):
		archivoArticulaciones.write(str(th[i])+" ")
	archivoArticulaciones.close()

def leerArticulacionesActuales():
	th=np.loadtxt(direccionAbsoluta+"/data/articulacionesActuales")
	try:
		if len(th)==6:
			return th
		else:
			return leerArticulacionesActuales()
	except:
		return [0,0,0,0,0,0]

def obtenerTiempoDesdeVelocidad(articulacionesFinales, rango_velocidad):
	if rango_velocidad==0:
		return False
	if rango_velocidad<1:
		rango_velocidad=1
	th_inicial=leerArticulacionesActuales()
	th_final=articulacionesFinales
	th_diferencia=map(sub, th_final,th_inicial)
	th_diferencia_absoluta=map(abs, th_diferencia)
	desplazamiento_maximo=max(th_diferencia_absoluta)

	velocidad_maxima=1.5 #rad/s
	tramos=10
	rango_velocidad=int(rango_velocidad)
	velocidad_robot=(velocidad_maxima/tramos)*rango_velocidad
	#k=22.0
	k=1
	tiempo=k*(desplazamiento_maximo/velocidad_robot)
	if tiempo<1:
		tiempo=1
	return tiempo

''' obtener que sensores hay '''
def queSensoresHay():
	global listaObjetosGazebo, leidoModelStates
	leidoModelStates=False
	while leidoModelStates==False:
		rospy.Subscriber('/gazebo/model_states', ModelStates, objetosEnGazebo)
	listaSensores=seleccionarSoloSensores(leidoModelStates)
	return listaSensores

def seleccionarSoloSensores(listaModelos):
	listaSensores=[]
	for i in listaObjetosGazebo:
		if len(i)>11:
			try:
				if i[:15]=="sensor_contacto":
					listaSensores.append(i)
				elif i[:12]=="sensor_laser":
					listaSensores.append(i)
			except:
				pass
	return listaSensores

def objetosEnGazebo(data):
	global listaObjetosGazebo, leidoModelStates
	listaObjetosGazebo=data.name
	leidoModelStates=True
