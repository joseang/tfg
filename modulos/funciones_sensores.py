#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import sys, os, time, math, glob, random
import rospy
from rosgraph_msgs.msg import Clock, Log
from std_msgs.msg import Float64
from control_msgs.msg import FollowJointTrajectoryActionGoal
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from sensor_msgs.msg import JointState
from gazebo_msgs.srv import SpawnModel
from geometry_msgs.msg import Pose
from actionlib_msgs.msg import GoalID
from cinematica.kinematic_functions import *
from cinematica.powerball_constants import *
from gazebo_msgs.srv import GetModelState
from gazebo_msgs.msg import ModelStates


sys.path.insert(0, '../')

from path_absoluto import obtenerDireccionAbsoluta
direccionAbsoluta=obtenerDireccionAbsoluta()

mesa="lwa4p::superficie_mesa::superficie_mesa_collision"

def chequear_sensor_laser(data):
	rango=data.ranges
	intensidad=data.intensities
	intensidadAbs=[]
	intensidadAbs.append(max(abs(i) for i in intensidad))
	'''
	if (max(intensidadAbs)>300) and (min(rango)<0.15):
		archivo.write('True')
	else:
		archivo.write('False')
	'''
	nombre=data.header.frame_id[7:]
	print "nombre: ",nombre
	archivo=open('./data/sensores/'+nombre,'w+')
	if min(rango)<0.15:
		archivo.write('True')
	else:
		archivo.write('False')
	archivo.close()

def chequear_sensor_contacto(data):
	print "Hay contacto"
	nombre_sensor=""
	hayContacto=0
	for colision in data.states:
		if colision.collision1_name[:6]=="sensor":
			nombre_sensor=colision.collision1_name
			if colision.collision2_name==mesa:
				print "Es un sensor contactando con la mesa. FALSE"
			else:
				print "Es un sensor contactando con algo que no es la mesa. TRUE"
				hayContacto+=1
		elif colision.collision2_name[:6]=="sensor":
			nombre_sensor=colision.collision2_name
			if colision.collision1_name==mesa:
				print "Es un sensor contactando con la mesa. FALSE"
			else:
				print "Es un sensor contactando con algo que no es la mesa. TRUE"
				hayContacto+=1
		else:
			print "No es un sensor. Le pueden dar por culo"

	if nombre_sensor[:12]=="sensor_laser":
		indice_aux=nombre_sensor.find("::")
		numero_sensor=nombre_sensor[12:indice_aux]
		tipo_sensor="laser"
	elif nombre_sensor[:12]=="sensor_conta":
		indice_aux=nombre_sensor.find("::")
		numero_sensor=nombre_sensor[15:indice_aux]
		tipo_sensor="contacto"
	else:
		tipo_sensor="-"
		numero_sensor="-"
	archivo=open(direccionAbsoluta+"/data/sensores/"+tipo_sensor+numero_sensor,'w+')
	if hayContacto>0:
		hayContacto=0
		archivo.write("True")
	elif hayContacto==0:
		archivo.write("False")
	archivo.close()


def cargarURDFCubo(seleccionarColor, indiceColorObjeto, contenido_final, dimensiones):
	archivo_primero=direccionAbsoluta+"/gazebo/piezas/prisma_primera.urdf"
	abierto_primero=open(archivo_primero,'r')
	urdf_primero=abierto_primero.read()
	archivo_segunda=direccionAbsoluta+"/gazebo/piezas/prisma_segunda.urdf"
	abierto_segunda=open(archivo_segunda,'r')
	urdf_segunda=abierto_segunda.read()
	archivo_tercera=direccionAbsoluta+"/gazebo/piezas/prisma_tercera.urdf"
	abierto_tercera=open(archivo_tercera,'r')
	urdf_tercera=abierto_tercera.read()
	x, y, z=dimensiones[0], dimensiones[1], dimensiones[2]
	x, y, z=str(x), str(y), str(z)
	contenido_urdf=urdf_primero+'<box size="'+x+' '+y+' '+z+'" />\n'+urdf_segunda+'<box size="'+x+' '+y+' '+z+'" />\n'+urdf_tercera+seleccionarColor[indiceColorObjeto]+contenido_final
	return contenido_urdf

def cargarURDFCilindro(seleccionarColor, indiceColorObjeto, contenido_final, dimensiones):
	archivo_primero=direccionAbsoluta+"/gazebo/piezas/cilindro_primero.urdf"
	abierto_primero=open(archivo_primero,'r')
	urdf_primero=abierto_primero.read()
	archivo_segunda=direccionAbsoluta+"/gazebo/piezas/cilindro_segundo.urdf"
	abierto_segunda=open(archivo_segunda,'r')
	urdf_segunda=abierto_segunda.read()
	archivo_tercera=direccionAbsoluta+"/gazebo/piezas/cilindro_tercero.urdf"
	abierto_tercera=open(archivo_tercera,'r')
	urdf_tercera=abierto_tercera.read()
	print "dimensiones: ", dimensiones
	alto, radio=dimensiones[0], dimensiones[1]
	alto, radio=str(alto), str(radio)
	contenido_urdf=urdf_primero+'<cylinder length="'+alto+'" radius="'+radio+'" />\n'+urdf_segunda+'<cylinder length="'+alto+'" radius="'+radio+'" />\n'+urdf_tercera+seleccionarColor[indiceColorObjeto]+contenido_final
	return contenido_urdf

def cargarURDFEsfera(seleccionarColor, indiceColorObjeto, contenido_final, dimensiones):
	archivo_primero=direccionAbsoluta+"/gazebo/piezas/esfera_primera.urdf"
	abierto_primero=open(archivo_primero,'r')
	urdf_primero=abierto_primero.read()
	archivo_segunda=direccionAbsoluta+"/gazebo/piezas/esfera_segunda.urdf"
	abierto_segunda=open(archivo_segunda,'r')
	urdf_segunda=abierto_segunda.read()
	archivo_tercera=direccionAbsoluta+"/gazebo/piezas/esfera_tercera.urdf"
	abierto_tercera=open(archivo_tercera,'r')
	urdf_tercera=abierto_tercera.read()
	radio=dimensiones[0]
	radio=str(radio)
	contenido_urdf=urdf_primero+'<sphere radius="'+radio+'" />\n'+urdf_segunda+'<sphere radius="'+radio+'" />\n'+urdf_tercera+seleccionarColor[indiceColorObjeto]+contenido_final
	return contenido_urdf

def anadir_objeto(nombre, dimensiones, numeroAlimentador):
	seleccionarColor={0:"Gazebo/Red", 1:"Gazebo/Blue", 2:"Gazebo/Green", 3:"Gazebo/Yellow", 4:"Gazebo/White", 5:"Gazebo/Black", 6:"Gazebo/Grey"}
	indiceColorObjeto=random.randint(0,6)
	numero=int(random.random()*100000)
	final_urdf=open(direccionAbsoluta+'/gazebo/piezas/final.urdf','r')
	contenido_final=final_urdf.read()
	if nombre=="caja":
		contenido_urdf=cargarURDFCubo(seleccionarColor, indiceColorObjeto, contenido_final, dimensiones)
	elif nombre=="cili":
		contenido_urdf=cargarURDFCilindro(seleccionarColor, indiceColorObjeto, contenido_final, dimensiones)
	elif nombre=="esfe":
		contenido_urdf=cargarURDFEsfera(seleccionarColor, indiceColorObjeto, contenido_final, dimensiones)
	nombre=nombre+str(numero)
	cargar_modelo=rospy.ServiceProxy('/gazebo/get_model_state', GetModelState)
	datos_modelo=cargar_modelo('alimentador'+str(numeroAlimentador),'world')
	X=datos_modelo.pose.position.x
	Y=datos_modelo.pose.position.y

	pose_inicial=Pose()
	pose_inicial.position.x=X
	pose_inicial.position.y=Y
	pose_inicial.position.z=0.761
	pose_inicial.orientation.x=0
	pose_inicial.orientation.y=0
	pose_inicial.orientation.z=0
	pose_inicial.orientation.w=0

	servicio_modelo=rospy.ServiceProxy('/gazebo/spawn_urdf_model', SpawnModel)
	servicio_modelo(nombre,contenido_urdf,"gazebo",pose_inicial,"world")

	time.sleep(3)

# [False-True, 0-inf, 0-3, 0-inf]
# [	Se han cargado los valores? True: si, False: no,
#	numero de objetos a introducir,
#	objeto a introducir (0:cubo, 1:prisma, 2:cilindro, 3:esfera),
#	objetos introducidos ya,
#	dimensiones del objeto]
configuracionAlimentador0Cargada=[False, None, None, 0, 0]
configuracionAlimentador1Cargada=[False, None, None, 0, 0]
configuracionAlimentador2Cargada=[False, None, None, 0, 0]
configuracionAlimentador3Cargada=[False, None, None, 0, 0]
configuracionAlimentador4Cargada=[False, None, None, 0, 0]
configuracionAlimentador5Cargada=[False, None, None, 0, 0]
configuracionAlimentador7Cargada=[False, None, None, 0, 0]
configuracionAlimentador6Cargada=[False, None, None, 0, 0]
configuracionAlimentador8Cargada=[False, None, None, 0, 0]
configuracionAlimentador9Cargada=[False, None, None, 0, 0]

datos_alimentador={}
numeros_alimentadores=[]

def modelos_disponibles(data):
	global sub_model
	nombres=data.name
	archivo=open(direccionAbsoluta+'/data/numeros_alimentadores','w+')
	archivo.truncate()
	archivo.close()
	try:
		nombres.remove('lwa4p')
		nombres.remove('ground_plane')
	except:
		print "no hay nada"
	numeros_alimentadores=[]
	for i in nombres:
		if len(i)>11 and i[:11]=="alimentador":
			numeros_alimentadores.append(i[11:])
			archivo=open(direccionAbsoluta+'/data/numeros_alimentadores','a')
			archivo.write(str(i[11:]))
	archivo.close()
	sub_model.unregister()

def obtenerModelosGazebo():
	global sub_model
	sub_model=rospy.Subscriber("/gazebo/model_states",ModelStates,modelos_disponibles)

def receptor_alimentador(data):
	obtenerModelosGazebo()
	numeros_alimentadores=np.loadtxt(direccionAbsoluta+'/data/numeros_alimentadores', dtype="string")
	numeros_alimentadores=numeros_alimentadores.tolist()
	if numeros_alimentadores==[]:
		print "HAY UN PROBLEMA"
	else:
		for i in numeros_alimentadores:
			datos=np.loadtxt(direccionAbsoluta+'/data/alimentador/configuracion/alimentador'+str(i), dtype="string")
			dicObjetos={0:'caja', 1:'cili', 2:'esfe'}
			numeroObjetos=int(datos[0])
			tipoObjeto=int(datos[1])
			dimensiones=datos[2:]
			dicObjetos={0:'caja', 1:'cili', 2:'esfe'}
			objeto=dicObjetos[tipoObjeto]
			if not i in datos_alimentador.keys():
				lista_alimentador=[False, None, None, 0, 0]
				lista_alimentador[0]=True
				lista_alimentador[1]=numeroObjetos
				lista_alimentador[2]=objeto
				lista_alimentador[4]=dimensiones

				datos_alimentador[i]=lista_alimentador
			else:
				objeto=datos_alimentador[i][2]
				alimentador="alimentador"#+str(i)
				count=0
				for k in data.states:

					nombre_alimentador=""
					if k.collision1_name[:11]==alimentador:
						indice=k.collision1_name.find(":")
						nombre_alimentador=k.collision1_name[:indice]
					if k.collision2_name[:11]==alimentador:
						indice=k.collision2_name.find(":")
						nombre_alimentador=k.collision2_name[:indice]

					if k.collision1_name[:11]==nombre_alimentador or k.collision2_name[:11]==nombre_alimentador:
						if k.collision1_name[:4]==objeto or k.collision2_name[:4]==objeto:
							print "CONTACTO"
							print
							print
							print
							count+=1

				if count==0 and nombre_alimentador!="":
					#print "toca anadir"
					cantidad=datos_alimentador[i][1]
					cantidadCargada=datos_alimentador[i][3]
					if cantidadCargada<cantidad:
						dimensiones=datos_alimentador[i][4]
						anadir_objeto(datos_alimentador[i][2],dimensiones,i)
						cantidadCargada+=1
						datos_alimentador[i][3]=cantidadCargada
						time.sleep(2)
					nombre_alimentador=""
				elif count>0:
					#print "count>0"
					count=0


def alimentador(data):
	if not numeroAlimentador in datos_alimentador.keys():
		lista_alimentador=[]
		lista_alimentador[0]=True
		lista_alimentador[1]=numeroObjetos
		lista_alimentador[2]=objeto
		lista_alimentador[4]=dimensiones
		datos_alimentador[numeroAlimentador]=lista_alimentador
	else:
		objeto=datos_alimentador[0][2]
		count=0
		for i in data.states:
			if i.collision1_name[:11]==alimentador or i.collision2_name[:11]==alimentador:
				if i.collision1_name[:4]==objeto or i.collision2_name[:4]==objeto:
					count+=1
		if count==0:
			#print "toca anadir"
			cantidad=configuracionAlimentador0Cargada[1]
			cantidadCargada=configuracionAlimentador0Cargada[3]
			if cantidadCargada<cantidad:
				dimensiones=configuracionAlimentador0Cargada[4]
				anadir_objeto(configuracionAlimentador0Cargada[2],dimensiones,0)
				cantidadCargada+=1
				configuracionAlimentador0Cargada[3]=cantidadCargada
				time.sleep(2)
		elif count>0:
			#print "count>0"
			count=0






def alimentador0(data):
	global configuracionAlimentador0Cargada
	dicObjetos={0:'caja', 1:'cili', 2:'esfe'}
	alimentador="alimentador"
	if not configuracionAlimentador0Cargada[0]:
		datos=np.loadtxt(direccionAbsoluta+'/data/alimentador/configuracion/alimentador0', dtype="string")
		if len(datos)>1:
			numeroObjetos=int(datos[0])
			tipoObjeto=int(datos[1]) #0:prisma, 1:cilindro, 2:esfera
			dimensiones=datos[2:]
			objeto=dicObjetos[tipoObjeto]
			configuracionAlimentador0Cargada[0]=True
			configuracionAlimentador0Cargada[1]=numeroObjetos
			configuracionAlimentador0Cargada[2]=objeto
			configuracionAlimentador0Cargada[4]=dimensiones
	if configuracionAlimentador0Cargada[0]:
		objeto=configuracionAlimentador0Cargada[2]
		count=0
		for i in data.states:
			if i.collision1_name[:11]==alimentador or i.collision2_name[:11]==alimentador:
				if i.collision1_name[:4]==objeto or i.collision2_name[:4]==objeto:
					count+=1
		if count==0:
			#print "toca anadir"
			cantidad=configuracionAlimentador0Cargada[1]
			cantidadCargada=configuracionAlimentador0Cargada[3]
			if cantidadCargada<cantidad:
				dimensiones=configuracionAlimentador0Cargada[4]
				anadir_objeto(configuracionAlimentador0Cargada[2],dimensiones,0)
				cantidadCargada+=1
				configuracionAlimentador0Cargada[3]=cantidadCargada
				time.sleep(2)
		elif count>0:
			#print "count>0"
			count=0

def alimentador1(data):
	global configuracionAlimentador1Cargada
	dicObjetos={0:'caja', 1:'cili', 2:'esfe'}
	alimentador="alimentador"
	if not configuracionAlimentador1Cargada[0]:
		datos=np.loadtxt(direccionAbsoluta+'/data/alimentador/configuracion/alimentador1', dtype="string")
		if len(datos)>1:
			numeroObjetos=int(datos[0])
			tipoObjeto=int(datos[1]) #0:prisma, 1:cilindro, 2:esfera
			dimensiones=datos[2:]
			print "datos: ", datos
			objeto=dicObjetos[tipoObjeto]
			configuracionAlimentador1Cargada[0]=True
			configuracionAlimentador1Cargada[1]=numeroObjetos
			configuracionAlimentador1Cargada[2]=objeto
			configuracionAlimentador1Cargada[4]=dimensiones
	if configuracionAlimentador1Cargada[0]:
		objeto=configuracionAlimentador1Cargada[2]
		count=0
		for i in data.states:
			if i.collision1_name[:11]==alimentador or i.collision2_name[:11]==alimentador:
				if i.collision1_name[:4]==objeto or i.collision2_name[:4]==objeto:
					count+=1
		if count==0:
			#print "toca anadir"
			cantidad=configuracionAlimentador1Cargada[1]
			cantidadCargada=configuracionAlimentador1Cargada[3]
			if cantidadCargada<cantidad:
				dimensiones=configuracionAlimentador1Cargada[4]
				anadir_objeto(configuracionAlimentador1Cargada[2],dimensiones,1)
				cantidadCargada+=1
				configuracionAlimentador1Cargada[3]=cantidadCargada
				time.sleep(2)
		elif count>0:
			#print "count>0"
			count=0

def alimentador2(data):
	global configuracionAlimentador2Cargada
	dicObjetos={0:'caja', 1:'cili', 2:'esfe'}
	alimentador="alimentador"
	if not configuracionAlimentador2Cargada[0]:
		datos=np.loadtxt(direccionAbsoluta+'/data/alimentador/configuracion/alimentador2', dtype="string")
		if len(datos)>1:
			numeroObjetos=int(datos[0])
			tipoObjeto=int(datos[1]) #0:prisma, 1:cilindro, 2:esfera
			dimensiones=datos[2:]
			print "datos: ", datos
			objeto=dicObjetos[tipoObjeto]
			configuracionAlimentador2Cargada[0]=True
			configuracionAlimentador2Cargada[1]=numeroObjetos
			configuracionAlimentador2Cargada[2]=objeto
			configuracionAlimentador2Cargada[4]=dimensiones
	if configuracionAlimentador2Cargada[0]:
		objeto=configuracionAlimentador2Cargada[2]
		count=0
		for i in data.states:
			if i.collision1_name[:11]==alimentador or i.collision2_name[:11]==alimentador:
				if i.collision1_name[:4]==objeto or i.collision2_name[:4]==objeto:
					count+=1
		if count==0:
			#print "toca anadir"
			cantidad=configuracionAlimentador2Cargada[1]
			cantidadCargada=configuracionAlimentador2Cargada[3]
			if cantidadCargada<cantidad:
				dimensiones=configuracionAlimentador2Cargada[4]
				anadir_objeto(configuracionAlimentador2Cargada[2],dimensiones,2)
				cantidadCargada+=1
				configuracionAlimentador2Cargada[3]=cantidadCargada
				time.sleep(2)
		elif count>0:
			#print "count>0"
			count=0

def alimentador3(data):
	global configuracionAlimentador3Cargada
	dicObjetos={0:'caja', 1:'cili', 2:'esfe'}
	alimentador="alimentador"
	if not configuracionAlimentador3Cargada[0]:
		datos=np.loadtxt(direccionAbsoluta+'/data/alimentador/configuracion/alimentador3', dtype="string")
		if len(datos)>1:
			numeroObjetos=int(datos[0])
			tipoObjeto=int(datos[1]) #0:prisma, 1:cilindro, 2:esfera
			dimensiones=datos[2:]
			print "datos: ", datos
			objeto=dicObjetos[tipoObjeto]
			configuracionAlimentador3Cargada[0]=True
			configuracionAlimentador3Cargada[1]=numeroObjetos
			configuracionAlimentador3Cargada[2]=objeto
			configuracionAlimentador3Cargada[4]=dimensiones
	if configuracionAlimentador3Cargada[0]:
		objeto=configuracionAlimentador3Cargada[2]
		count=0
		for i in data.states:
			if i.collision1_name[:11]==alimentador or i.collision2_name[:11]==alimentador:
				if i.collision1_name[:4]==objeto or i.collision2_name[:4]==objeto:
					count+=1
		if count==0:
			#print "toca anadir"
			cantidad=configuracionAlimentador3Cargada[1]
			cantidadCargada=configuracionAlimentador3Cargada[3]
			if cantidadCargada<cantidad:
				dimensiones=configuracionAlimentador3Cargada[4]
				anadir_objeto(configuracionAlimentador3Cargada[2],dimensiones,3)
				cantidadCargada+=1
				configuracionAlimentador3Cargada[3]=cantidadCargada
				time.sleep(2)
		elif count>0:
			#print "count>0"
			count=0

def alimentador4(data):
	global configuracionAlimentador4Cargada
	dicObjetos={0:'caja', 1:'cili', 2:'esfe'}
	alimentador="alimentador"
	if not configuracionAlimentador4Cargada[0]:
		datos=np.loadtxt(direccionAbsoluta+'/data/alimentador/configuracion/alimentador4', dtype="string")
		if len(datos)>1:
			numeroObjetos=int(datos[0])
			tipoObjeto=int(datos[1]) #0:prisma, 1:cilindro, 2:esfera
			dimensiones=datos[2:]
			print "datos: ", datos
			objeto=dicObjetos[tipoObjeto]
			configuracionAlimentador4Cargada[0]=True
			configuracionAlimentador4Cargada[1]=numeroObjetos
			configuracionAlimentador4Cargada[2]=objeto
			configuracionAlimentador4Cargada[4]=dimensiones
	if configuracionAlimentador4Cargada[0]:
		objeto=configuracionAlimentador4Cargada[2]
		count=0
		for i in data.states:
			if i.collision1_name[:11]==alimentador or i.collision2_name[:11]==alimentador:
				if i.collision1_name[:4]==objeto or i.collision2_name[:4]==objeto:
					count+=1
		if count==0:
			#print "toca anadir"
			cantidad=configuracionAlimentador4Cargada[1]
			cantidadCargada=configuracionAlimentador4Cargada[3]
			if cantidadCargada<cantidad:
				dimensiones=configuracionAlimentador4Cargada[4]
				anadir_objeto(configuracionAlimentador4Cargada[2],dimensiones,4)
				cantidadCargada+=1
				configuracionAlimentador4Cargada[3]=cantidadCargada
				time.sleep(2)
		elif count>0:
			#print "count>0"
			count=0

def alimentador5(data):
	global configuracionAlimentador5Cargada
	dicObjetos={0:'caja', 1:'cili', 2:'esfe'}
	alimentador="alimentador"
	if not configuracionAlimentador5Cargada[0]:
		datos=np.loadtxt(direccionAbsoluta+'/data/alimentador/configuracion/alimentador4', dtype="string")
		if len(datos)>1:
			numeroObjetos=int(datos[0])
			tipoObjeto=int(datos[1]) #0:prisma, 1:cilindro, 2:esfera
			dimensiones=datos[2:]
			print "datos: ", datos
			objeto=dicObjetos[tipoObjeto]
			configuracionAlimentador5Cargada[0]=True
			configuracionAlimentador5Cargada[1]=numeroObjetos
			configuracionAlimentador5Cargada[2]=objeto
			configuracionAlimentador5Cargada[4]=dimensiones
	if configuracionAlimentador5Cargada[0]:
		objeto=configuracionAlimentador5Cargada[2]
		count=0
		for i in data.states:
			if i.collision1_name[:11]==alimentador or i.collision2_name[:11]==alimentador:
				if i.collision1_name[:4]==objeto or i.collision2_name[:4]==objeto:
					count+=1
		if count==0:
			#print "toca anadir"
			cantidad=configuracionAlimentador5Cargada[1]
			cantidadCargada=configuracionAlimentador5Cargada[3]
			if cantidadCargada<cantidad:
				dimensiones=configuracionAlimentador5Cargada[4]
				anadir_objeto(configuracionAlimentador5Cargada[2],dimensiones,5)
				cantidadCargada+=1
				configuracionAlimentador5Cargada[3]=cantidadCargada
				time.sleep(2)
		elif count>0:
			#print "count>0"
			count=0

def alimentador6(data):
	global configuracionAlimentador6Cargada
	dicObjetos={0:'caja', 1:'cili', 2:'esfe'}
	alimentador="alimentador"
	if not configuracionAlimentador6Cargada[0]:
		datos=np.loadtxt(direccionAbsoluta+'/data/alimentador/configuracion/alimentador6', dtype="string")
		if len(datos)>1:
			numeroObjetos=int(datos[0])
			tipoObjeto=int(datos[1]) #0:prisma, 1:cilindro, 2:esfera
			dimensiones=datos[2:]
			print "datos: ", datos
			objeto=dicObjetos[tipoObjeto]
			configuracionAlimentador6Cargada[0]=True
			configuracionAlimentador6Cargada[1]=numeroObjetos
			configuracionAlimentador6Cargada[2]=objeto
			configuracionAlimentador6Cargada[4]=dimensiones
	if configuracionAlimentador6Cargada[0]:
		objeto=configuracionAlimentador6Cargada[2]
		count=0
		for i in data.states:
			if i.collision1_name[:11]==alimentador or i.collision2_name[:11]==alimentador:
				if i.collision1_name[:4]==objeto or i.collision2_name[:4]==objeto:
					count+=1
		if count==0:
			#print "toca anadir"
			cantidad=configuracionAlimentador6Cargada[1]
			cantidadCargada=configuracionAlimentador6Cargada[3]
			if cantidadCargada<cantidad:
				dimensiones=configuracionAlimentador6Cargada[4]
				anadir_objeto(configuracionAlimentador6Cargada[2],dimensiones,6)
				cantidadCargada+=1
				configuracionAlimentador6Cargada[3]=cantidadCargada
				time.sleep(2)
		elif count>0:
			#print "count>0"
			count=0

def alimentador7(data):
	global configuracionAlimentador7Cargada
	dicObjetos={0:'caja', 1:'cili', 2:'esfe'}
	alimentador="alimentador"
	if not configuracionAlimentador7Cargada[0]:
		datos=np.loadtxt(direccionAbsoluta+'/data/alimentador/configuracion/alimentador7', dtype="string")
		if len(datos)>1:
			numeroObjetos=int(datos[0])
			tipoObjeto=int(datos[1]) #0:prisma, 1:cilindro, 2:esfera
			dimensiones=datos[2:]
			print "datos: ", datos
			objeto=dicObjetos[tipoObjeto]
			configuracionAlimentador7Cargada[0]=True
			configuracionAlimentador7Cargada[1]=numeroObjetos
			configuracionAlimentador7Cargada[2]=objeto
			configuracionAlimentador7Cargada[4]=dimensiones
	if configuracionAlimentador7Cargada[0]:
		objeto=configuracionAlimentador7Cargada[2]
		count=0
		for i in data.states:
			if i.collision1_name[:11]==alimentador or i.collision2_name[:11]==alimentador:
				if i.collision1_name[:4]==objeto or i.collision2_name[:4]==objeto:
					count+=1
		if count==0:
			#print "toca anadir"
			cantidad=configuracionAlimentador7Cargada[1]
			cantidadCargada=configuracionAlimentador7Cargada[3]
			if cantidadCargada<cantidad:
				dimensiones=configuracionAlimentador7Cargada[4]
				anadir_objeto(configuracionAlimentador7Cargada[2],dimensiones,7)
				cantidadCargada+=1
				configuracionAlimentador7Cargada[3]=cantidadCargada
				time.sleep(2)
		elif count>0:
			#print "count>0"
			count=0

def alimentador8(data):
	global configuracionAlimentador8Cargada
	dicObjetos={0:'caja', 1:'cili', 2:'esfe'}
	alimentador="alimentador"
	if not configuracionAlimentador8Cargada[0]:
		datos=np.loadtxt(direccionAbsoluta+'/data/alimentador/configuracion/alimentador8', dtype="string")
		if len(datos)>1:
			numeroObjetos=int(datos[0])
			tipoObjeto=int(datos[1]) #0:prisma, 1:cilindro, 2:esfera
			dimensiones=datos[2:]
			print "datos: ", datos
			objeto=dicObjetos[tipoObjeto]
			configuracionAlimentador8Cargada[0]=True
			configuracionAlimentador8Cargada[1]=numeroObjetos
			configuracionAlimentador8Cargada[2]=objeto
			configuracionAlimentador8Cargada[4]=dimensiones
	if configuracionAlimentador8Cargada[0]:
		objeto=configuracionAlimentador8Cargada[2]
		count=0
		for i in data.states:
			if i.collision1_name[:11]==alimentador or i.collision2_name[:11]==alimentador:
				if i.collision1_name[:4]==objeto or i.collision2_name[:4]==objeto:
					count+=1
		if count==0:
			#print "toca anadir"
			cantidad=configuracionAlimentador8Cargada[1]
			cantidadCargada=configuracionAlimentador8Cargada[3]
			if cantidadCargada<cantidad:
				dimensiones=configuracionAlimentador8Cargada[4]
				anadir_objeto(configuracionAlimentador8Cargada[2],dimensiones,8)
				cantidadCargada+=1
				configuracionAlimentador8Cargada[3]=cantidadCargada
				time.sleep(2)
		elif count>0:
			#print "count>0"
			count=0

def alimentador9(data):
	global configuracionAlimentador9Cargada
	dicObjetos={0:'caja', 1:'cili', 2:'esfe'}
	alimentador="alimentador"
	if not configuracionAlimentador9Cargada[0]:
		datos=np.loadtxt(direccionAbsoluta+'/data/alimentador/configuracion/alimentador9', dtype="string")
		if len(datos)>1:
			numeroObjetos=int(datos[0])
			tipoObjeto=int(datos[1]) #0:prisma, 1:cilindro, 2:esfera
			dimensiones=datos[2:]
			print "datos: ", datos
			objeto=dicObjetos[tipoObjeto]
			configuracionAlimentador9Cargada[0]=True
			configuracionAlimentador9Cargada[1]=numeroObjetos
			configuracionAlimentador9Cargada[2]=objeto
			configuracionAlimentador9Cargada[4]=dimensiones
	if configuracionAlimentador9Cargada[0]:
		objeto=configuracionAlimentador9Cargada[2]
		count=0
		for i in data.states:
			if i.collision1_name[:11]==alimentador or i.collision2_name[:11]==alimentador:
				if i.collision1_name[:4]==objeto or i.collision2_name[:4]==objeto:
					count+=1
		if count==0:
			#print "toca anadir"
			cantidad=configuracionAlimentador9Cargada[1]
			cantidadCargada=configuracionAlimentador9Cargada[3]
			if cantidadCargada<cantidad:
				dimensiones=configuracionAlimentador9Cargada[4]
				anadir_objeto(configuracionAlimentador9Cargada[2],dimensiones,9)
				cantidadCargada+=1
				configuracionAlimentador9Cargada[3]=cantidadCargada
				time.sleep(2)
		elif count>0:
			#print "count>0"
			count=0
