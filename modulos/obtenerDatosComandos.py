#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os, sys
from PyQt4 import QtCore, QtGui
from interfazComandos.ventanaIr import *
from interfazComandos.ventanaLineal import *
from interfazComandos.ventanaCircular import *
from interfazComandos.ventanaMemorizar import *
from interfazComandos.ventanaEtiqueta import *
from interfazComandos.ventanaSaltar import *
from interfazComandos.ventanaSaltarSi import *
from interfazComandos.ventanaVariable import *
from interfazComandos.ventanaEsperar import *
from interfazComandos.ventanaComentario import *
from interfazComandos.ventanaCrearSubrutina import *
from interfazComandos.ventanaSeleccionarSubrutina import *
from interfazComandos.ventanaSensores import *
from interfazComandos.ventanaInterrupciones import *
#from interfazComandos.ventanaIrArticulaciones import *
#from interfazComandos.ventanaIrXYZ import *
from interfaz.ventanaTrayectoriaArticulaciones import *
from interfaz.ventanaTrayectoriaCartesiana import *
import numpy as np
from crearMensaje import *
from comprobarArchivo import *
from funciones import *
from ejecutarOrdenes import *
from guardarEnArchivo import *

class ordenIr(QtGui.QDialog):
	def __init__(self,parent=None, listaInfo=[]):
		QtGui.QWidget.__init__(self, parent,QtCore.Qt.WindowStaysOnTopHint)
		self.ui = Ui_IrPosicion()
		self.ui.setupUi(self)
		QtCore.QObject.connect(self.ui.buttonIr, QtCore.SIGNAL('clicked()'), self.aceptar)
		self.cargarListaPosiciones()
		self.listaInfo=listaInfo
		self.cargarValores()

	def cargarValores(self):
		if self.listaInfo!=[]:
			try:
				posicion=int(self.listaInfo[0])
				tipoVelocidadTiempo=self.listaInfo[1]
				valorVelocidadTiempo=self.listaInfo[2]
				indice=self.ui.numeroPosicion.findText(str(posicion))
				self.ui.numeroPosicion.setCurrentIndex(indice)
				if tipoVelocidadTiempo=='tiempo':
					self.ui.radioTiempo.setChecked(True)
					self.ui.valorTiempo.setText(str(valorVelocidadTiempo))
				elif tipoVelocidadTiempo=='velocidad':
					self.ui.radioVelocidad.setChecked(True)
					self.ui.valorVelocidad.setValue(int(valorVelocidadTiempo))
			except:
				self.close()


	def aceptar(self):
		global numeroPosicion, tipo, valor
		numeroPosicion=str(self.ui.numeroPosicion.currentText())
		if self.ui.radioVelocidad.isChecked():
			valor=self.ui.valorVelocidad.value()
			tipo="velocidad"
		elif self.ui.radioTiempo.isChecked():
			valor=self.ui.valorTiempo.text()
			tipo="tiempo"
		try:
			valor=int(valor)
			valor=str(valor)
			self.accept()
		except Exception:
			QtGui.QMessageBox.about(self, 'Error','Solo numeros enteros')

	def obtenerDatos(self):
		global numeroPosicion, tipo, valor
		return [numeroPosicion, tipo, valor]

	def cargarListaPosiciones(self):
		self.ui.numeroPosicion.clear()
		listaPosiciones=obtenerNumeroPosicionesDisponibles()
		for i in listaPosiciones:
			self.ui.numeroPosicion.insertItem(0,i)



class ordenTrayectoriaArticulaciones(QtGui.QDialog):
	def __init__(self,parent=None,listaInfo=[]):
		global direccionArchivo
		QtGui.QWidget.__init__(self, parent,QtCore.Qt.WindowStaysOnTopHint)
		self.ui = Ui_TrayectoriaArticulacion()
		self.ui.setupUi(self)
		self.listaInfo=listaInfo
		self.connect(self.ui.botonAceptar, QtCore.SIGNAL('clicked()'), self.aceptar)
		self.connect(self.ui.botonCargar, QtCore.SIGNAL('clicked()'), self.cargar)
		self.connect(self.ui.botonEjecutar, QtCore.SIGNAL('clicked()'), self.ejecutar)
		direccionArchivo=""
		self.cargarValores()

	def cargarValores(self):
		if self.listaInfo!=[]:
			try:
				direccion=self.listaInfo
				self.ui.textoDireccion.setText(str(direccion))
			except:
				self.close()

	def ejecutar(self):
		global direccionArchivo
		direccionArchivo=self.ui.textoDireccion.text()
		print "direccionArchivo: ", direccionArchivo
		if direccionArchivo!="":
			try:
				datos=np.loadtxt(str(direccionArchivo))
				print "datos: \n", datos
				ejecutar_trayectoria_articulaciones(datos)
			except Exception,e:
				print str(e)
				QtGui.QMessageBox.about(self, 'Error','Hay un error en el archivo')
				return

	def cargar(self):
		global direccionArchivo
		direccionArchivo = QtGui.QFileDialog.getOpenFileName(
			self,
			"Cargar archivo",
			"~/",
			'*.txt')
		if direccionArchivo=="":
			return
		print "direccionArchivo: ", direccionArchivo
		self.ui.textoDireccion.setText(direccionArchivo)

	def aceptar(self):
		global direccionArchivo
		direccionArchivo=self.ui.textoDireccion.text()
		if direccionArchivo!="":
			try:
				datos=np.loadtxt(str(direccionArchivo))
				for fila in datos:
					for i in fila:
						float(i)
				try:
					for fila in datos:
						if len(fila)!=7:
							QtGui.QMessageBox.about(self, 'Error','Hay un error en el archivo')
							return
				except:
					QtGui.QMessageBox.about(self, 'Error','Hay un error en el archivo')
					return
			except:
				QtGui.QMessageBox.about(self, 'Error','Hay un error en el archivo')
				return
		self.accept()

	def obtenerDatos(self):
		global direccionArchivo
		return str(direccionArchivo)


class ordenTrayectoriaCartesiana(QtGui.QDialog):
	def __init__(self,parent=None,listaInfo=[]):
		global direccionArchivo
		QtGui.QWidget.__init__(self, parent,QtCore.Qt.WindowStaysOnTopHint)
		self.ui = Ui_TrayectoriaCartesiana()
		self.ui.setupUi(self)
		self.listaInfo=listaInfo
		self.connect(self.ui.botonAceptar, QtCore.SIGNAL('clicked()'), self.aceptar)
		self.connect(self.ui.botonCargar, QtCore.SIGNAL('clicked()'), self.cargar)
		self.connect(self.ui.botonEjecutar, QtCore.SIGNAL('clicked()'), self.ejecutar)
		direccionArchivo=""
		self.cargarValores()

	def cargarValores(self):
		if self.listaInfo!=[]:
			try:
				direccion=self.listaInfo
				self.ui.textoDireccion.setText(str(direccion))
			except:
				self.close()

	def ejecutar(self):
		global direccionArchivo
		direccionArchivo=self.ui.textoDireccion.text()
		print "direccionArchivo: ", direccionArchivo
		if direccionArchivo!="":
			try:
				datos=np.loadtxt(str(direccionArchivo))
				print "datos: \n", datos
				ejecutar_trayectoria_cartesiana(datos)
			except Exception,e:
				print str(e)
				QtGui.QMessageBox.about(self, 'Error','Hay un error en el archivo')
				return

	def cargar(self):
		global direccionArchivo
		direccionArchivo = QtGui.QFileDialog.getOpenFileName(
			self,
			"Cargar archivo",
			"~/",
			'*.txt')
		if direccionArchivo=="":
			return
		print "direccionArchivo: ", direccionArchivo
		self.ui.textoDireccion.setText(direccionArchivo)

	def aceptar(self):
		global direccionArchivo
		direccionArchivo=self.ui.textoDireccion.text()
		if direccionArchivo!="":
			try:
				datos=np.loadtxt(str(direccionArchivo))
				for fila in datos:
					for i in fila:
						float(i)
				try:
					for fila in datos:
						if len(fila)!=7:
							QtGui.QMessageBox.about(self, 'Error','Hay un error en el archivo')
							return
				except:
					QtGui.QMessageBox.about(self, 'Error','Hay un error en el archivo')
					return
			except:
				QtGui.QMessageBox.about(self, 'Error','Hay un error en el archivo')
				return
		self.accept()

	def obtenerDatos(self):
		global direccionArchivo
		return str(direccionArchivo)


class ordenLineal(QtGui.QDialog):
	def __init__(self,parent=None, listaInfo=[]):
		QtGui.QWidget.__init__(self, parent)
		self.ui = Ui_Lineal()
		self.ui.setupUi(self)
		self.listaInfo=listaInfo
		self.cargarListaPosiciones()
		QtCore.QObject.connect(self.ui.buttonAceptar, QtCore.SIGNAL('clicked()'), self.aceptar)
		self.cargarValores()

	def cargarValores(self):
		if self.listaInfo!=[]:
			try:
				posicion=int(self.listaInfo[0])
				tipoVelocidadTiempo=self.listaInfo[1]
				valorVelocidadTiempo=self.listaInfo[2]
				indice=self.ui.numeroPosicion.findText(str(posicion))
				self.ui.numeroPosicion.setCurrentIndex(indice)
				if tipoVelocidadTiempo=='tiempo':
					self.ui.radioTiempo.setChecked(True)
					self.ui.valorTiempo.setText(str(valorVelocidadTiempo))
				elif tipoVelocidadTiempo=='velocidad':
					self.ui.radioVelocidad.setChecked(True)
					self.ui.valorVelocidad.setValue(int(valorVelocidadTiempo))
			except:
				self.close()


	def cargarListaPosiciones(self):
		self.ui.numeroPosicion.clear()
		listaPosiciones=obtenerNumeroPosicionesDisponibles()
		for i in listaPosiciones:
			self.ui.numeroPosicion.insertItem(0,i)

	def aceptar(self):
		global numeroPosicion, tipo, valor
		numeroPosicion=str(self.ui.numeroPosicion.currentText())
		if self.ui.radioVelocidad.isChecked():
			valor=float(self.ui.valorVelocidad.value())
			tipo="velocidad"
		elif self.ui.radioTiempo.isChecked():
			valor=float(self.ui.valorTiempo.text())
			tipo="tiempo"
		try:
			valor=int(valor)
			valor=str(valor)
			self.accept()
		except Exception:
			mensaje=u'Solo números enteros'
			QtGui.QMessageBox.about(self, 'Error',mensaje)

	def obtenerDatos(self):
		global numeroPosicion, tipo, valor
		return [numeroPosicion, tipo, valor]

class ordenCircular(QtGui.QDialog):
	def __init__(self,parent=None, listaInfo=[]):
		QtGui.QWidget.__init__(self, parent,QtCore.Qt.WindowStaysOnTopHint)
		self.ui = Ui_Circular()
		self.ui.setupUi(self)
		self.listaInfo=listaInfo
		QtCore.QObject.connect(self.ui.buttonAceptar, QtCore.SIGNAL('clicked()'), self.principal)
		self.cargarListaPosiciones()
		self.cargarValores()

	def cargarValores(self):
		if self.listaInfo!=[]:
			try:
				posicionFinal=int(self.listaInfo[0])
				posicionMedia=int(self.listaInfo[1])
				tipoVelocidadTiempo=self.listaInfo[2]
				valorVelocidadTiempo=self.listaInfo[3]
				indiceFinal=self.ui.posicionFinal.findText(str(posicionFinal))
				self.ui.posicionFinal.setCurrentIndex(indiceFinal)
				indiceMedia=self.ui.posicionMedia.findText(str(posicionMedia))
				self.ui.posicionMedia.setCurrentIndex(indiceMedia)
				if tipoVelocidadTiempo=='tiempo':
					self.ui.radioTiempo.setChecked(True)
					self.ui.valorTiempo.setText(str(valorVelocidadTiempo))
				elif tipoVelocidadTiempo=='velocidad':
					self.ui.radioVelocidad.setChecked(True)
					self.ui.valorVelocidad.setValue(int(valorVelocidadTiempo))
			except:
				self.close()


	def principal(self):
		global posicionFinal, posicionMedia, tipo, valor
		posicionFinal=str(self.ui.posicionFinal.currentText())
		posicionMedia=str(self.ui.posicionMedia.currentText())
		if posicionMedia==posicionFinal:
			QtGui.QMessageBox.about(self, 'Error','Las posiciones no pueden ser las mismas')
			return
		if self.ui.radioVelocidad.isChecked():
			valor=float(self.ui.valorVelocidad.value())
			tipo="velocidad"
		elif self.ui.radioTiempo.isChecked():
			valor=self.ui.valorTiempo.text()
			tipo="tiempo"
		try:
			valor=int(valor)
			valor=str(valor)
			self.accept()
		except Exception:
			mensaje=u'Solo números enteros'
			QtGui.QMessageBox.about(self, 'Error',mensaje)


	def obtenerDatos(self):
		global posicionFinal, posicionMedia, tipo, valor
		return [posicionFinal, posicionMedia, tipo, valor]

	def cargarListaPosiciones(self):
		self.ui.posicionFinal.clear()
		self.ui.posicionMedia.clear()
		listaPosiciones=obtenerNumeroPosicionesDisponibles()
		for i in listaPosiciones:
			self.ui.posicionFinal.insertItem(0,i)
			self.ui.posicionMedia.insertItem(0,i)
		'''
		if lista=="posicionFinal":
			self.ui.posicionFinal.clear()
		elif lista=="posicionMedia":
			self.ui.posicionMedia.clear()
		for file in os.listdir("./posiciones"):
			if file.endswith(".txt"):
				indice=file[1:].index('.')+1
				if lista=="posicionFinal":
					self.ui.posicionFinal.insertItem(0,str(int(file[1:indice])))
				elif lista=="posicionMedia":
					self.ui.posicionMedia.insertItem(0,str(int(file[1:indice])))
		'''

class ordenMemorizar(QtGui.QDialog):
	def __init__(self,parent=None, listaInfo=[]):
		QtGui.QWidget.__init__(self,parent,QtCore.Qt.WindowStaysOnTopHint)
		self.ui = Ui_MemorizarPosicion()
		self.ui.setupUi(self)
		self.listaInfo=listaInfo
		self.cargarListaPosiciones()
		QtCore.QObject.connect(self.ui.buttonAceptar, QtCore.SIGNAL('clicked()'), self.aceptar)
		self.cargarValores()

	def cargarValores(self):
		if self.listaInfo!=[]:
			try:
				posicionMemorizar=int(self.listaInfo)
				indiceFinal=self.ui.numeroPosicion.findText(str(posicionMemorizar))
				if indiceFinal==-1:
					indice=self.ui.numeroPosicion.count()
					self.ui.numeroPosicion.insertItem(indice,str(posicionMemorizar))
					indiceFinal=self.ui.numeroPosicion.findText(str(posicionMemorizar))
					self.ui.numeroPosicion.setCurrentIndex(indiceFinal)
				else:
					self.ui.numeroPosicion.setCurrentIndex(indiceFinal)
			except:
				self.close()

	def aceptar(self):
		global numeroPosicion
		try:
			numeroPosicion=int(self.ui.numeroPosicion.currentText())
			numeroPosicion=str(numeroPosicion)
			self.accept()
		except:
			mensaje=u'Solo números enteros'
			QtGui.QMessageBox.about(self, 'Error',mensaje)


	def obtenerDatos(self):
		return numeroPosicion

	def cargarListaPosiciones(self):
		for file in os.listdir("./posiciones"):
			if file.endswith(".txt"):
				k=file[1:].index('.')+1
				self.ui.numeroPosicion.insertItem(0,str(int(file[1:k])))

class ordenEtiqueta(QtGui.QDialog):
	def __init__(self,parent=None, listaInfo=[]):
		QtGui.QWidget.__init__(self,parent,QtCore.Qt.WindowStaysOnTopHint)
		self.ui = Ui_Etiqueta()
		self.ui.setupUi(self)
		self.listaInfo=listaInfo
		QtCore.QObject.connect(self.ui.buttonAceptar, QtCore.SIGNAL('clicked()'), self.aceptar)
		self.cargarValores()

	def cargarValores(self):
		if self.listaInfo!=[]:
			try:
				etiquetaDestino=str(self.listaInfo)
				self.ui.nombreEtiqueta.setText(etiquetaDestino)
			except:
				self.close()

	def aceptar(self):
		global nombreEtiqueta
		nombreEtiqueta=str(self.ui.nombreEtiqueta.text())
		if nombreEtiqueta=="":
			self.close()
		else:
			archivoEtiquetas=open('./data/etiquetasDisponibles', 'a')
			archivoEtiquetas.write(nombreEtiqueta+"\n")
			self.accept()

	def obtenerDatos(self):
		global nombreEtiqueta
		return nombreEtiqueta

class ordenSaltar(QtGui.QDialog):
	def __init__(self,parent=None, listaInfo=[]):
		QtGui.QWidget.__init__(self,parent,QtCore.Qt.WindowStaysOnTopHint)
		self.ui = Ui_SaltarA()
		self.ui.setupUi(self)
		self.listaInfo=listaInfo
		self.cargarEtiquetas()
		QtCore.QObject.connect(self.ui.buttonAceptar, QtCore.SIGNAL('clicked()'), self.aceptar)
		self.cargarValores()

	def cargarValores(self):
		if self.listaInfo!=[]:
			try:
				etiquetaDestino=str(self.listaInfo)
				indiceFinal=self.ui.nombreSalto.findText(etiquetaDestino)
				self.ui.nombreSalto.setCurrentIndex(indiceFinal)
			except:
				self.close()

	def aceptar(self):
		global nombreEtiqueta
		nombreEtiqueta=self.ui.nombreSalto.currentText()
		print nombreEtiqueta
		if nombreEtiqueta=="":
			self.close()
		else:
			self.accept()

	def obtenerDatos(self):
		global nombreEtiqueta
		return str(nombreEtiqueta)

	def cargarEtiquetas(self):
		archivo=open("./data/etiquetasDisponibles", 'r')
		lineas=archivo.readlines()
		lineas=list(set(lineas))
		for i in lineas:
			indice=self.ui.nombreSalto.count()
			self.ui.nombreSalto.insertItem(indice,i.rstrip())

		#self.ui.nombreSalto.insertItems(0, lineas.rstrip())
		#archivo=list(archivo)
		#self.ui.nombreSalto.insertItems(0, archivo)

class ordenSaltarSi(QtGui.QDialog):
	def __init__(self,parent=None, listaInfo=[]):
		QtGui.QWidget.__init__(self,parent,QtCore.Qt.WindowStaysOnTopHint)
		self.ui = Ui_SiSalta()
		self.ui.setupUi(self)
		self.listaInfo=listaInfo
		self.cargarEtiquetas()
		QtCore.QObject.connect(self.ui.buttonAceptar, QtCore.SIGNAL('clicked()'), self.aceptar)
		self.cargarValores()

	def cargarValores(self):
		if self.listaInfo!=[]:
			try:
				valorVariable=self.listaInfo[0]
				nombreVariable=self.listaInfo[1]
				condicion=self.listaInfo[2]
				nombreSalto=self.listaInfo[3]
				indiceEtiqueta=self.ui.nombreSalto.findText(nombreSalto)
				self.ui.nombreSalto.setCurrentIndex(indiceEtiqueta)
				indiceCondicion=self.ui.condicion.findText(condicion)
				self.ui.condicion.setCurrentIndex(indiceCondicion)
				self.ui.valorVariable.setText(valorVariable)
				self.ui.nombreVariable.setText(nombreVariable)
			except:
				self.close()

	def cargarEtiquetas(self):
		archivoAux=open("data/etiquetasDisponibles","r")
		lineasEtiqueta=archivoAux.readlines()
		lineasEtiqueta=list(set(lineasEtiqueta))
		for i in lineasEtiqueta:
			indice=self.ui.nombreSalto.count()
			self.ui.nombreSalto.insertItem(indice,i.rstrip())

	def aceptar(self):
		global valorVariable, nombreVariable, condicion, nombreSalto
		try:
			valorVariable=float(self.ui.valorVariable.text())
			valorVariable=str(valorVariable)
		except:
			mensaje=u'Solo números enteros'
			QtGui.QMessageBox.about(self, 'Error',mensaje)
			return
		try:
			nombreVariable=self.ui.nombreVariable.text()
			condicion=self.ui.condicion.currentText()
			nombreSalto=self.ui.nombreSalto.currentText()
			if nombreVariable=="" or condicion=="" or nombreSalto=="":
				return
			self.accept()
		except Exception:
			self.close()

	def obtenerDatos(self):
		global valorVariable, nombreVariable, condicion, nombreSalto
		listaFinal=[valorVariable, nombreVariable, condicion, nombreSalto]
		listaFinal=map(str, listaFinal)
		return listaFinal

class ordenVariable(QtGui.QDialog):
	def __init__(self,parent=None, listaInfo=[]):
		QtGui.QWidget.__init__(self,parent,QtCore.Qt.WindowStaysOnTopHint)
		self.ui = Ui_Variable()
		self.ui.setupUi(self)
		self.listaInfo=listaInfo
		QtCore.QObject.connect(self.ui.buttonAceptar, QtCore.SIGNAL('clicked()'), self.aceptar)
		self.cargarValores()

	def cargarValores(self):
		if self.listaInfo!=[]:
			try:
				nombre=self.listaInfo[0]
				expresion=self.listaInfo[1]
				self.ui.valorVariable.setText(expresion)
				self.ui.nombreVariable.setText(nombre)
			except:
				self.close()


	def aceptar(self):
		global valorExpresionVariable, nombreVariable
		try:
			valorExpresionVariable=str(self.ui.valorVariable.text())
			nombreVariable=str(self.ui.nombreVariable.text())
			if valorExpresionVariable!="" and nombreVariable!="":
				self.accept()
			else:
				self.close()
		except:
			self.close()

	def obtenerDatos(self):
		global valorExpresionVariable, nombreVariable
		return [nombreVariable, valorExpresionVariable]

class ordenEsperar(QtGui.QDialog):
	def __init__(self,parent=None, listaInfo=[]):
		QtGui.QWidget.__init__(self,parent,QtCore.Qt.WindowStaysOnTopHint)
		self.ui = Ui_Esperar()
		self.ui.setupUi(self)
		self.listaInfo=listaInfo
		QtCore.QObject.connect(self.ui.buttonAceptar, QtCore.SIGNAL('clicked()'), self.aceptar)
		self.cargarValores()

	def cargarValores(self):
		if self.listaInfo!=[]:
			try:
				tiempoEspera=int(self.listaInfo)
				self.ui.valorTiempo.setValue(tiempoEspera)
			except:
				self.close()

	def aceptar(self):
		global tiempoEspera
		try:
			tiempoEspera=int(self.ui.valorTiempo.value())
			tiempoEspera=str(tiempoEspera)
			self.accept()
		except:
			self.close()

	def obtenerDatos(self):
		global tiempoEspera
		return tiempoEspera

class ordenComentario(QtGui.QDialog):
	def __init__(self,parent=None, listaInfo=[]):
		QtGui.QWidget.__init__(self,parent,QtCore.Qt.WindowStaysOnTopHint)
		self.ui = Ui_Comentario()
		self.ui.setupUi(self)
		self.listaInfo=listaInfo
		QtCore.QObject.connect(self.ui.buttonAceptar, QtCore.SIGNAL('clicked()'), self.aceptar)
		self.cargarValores()

	def cargarValores(self):
		if self.listaInfo!=[]:
			try:
				comentario=str(self.listaInfo)
				self.ui.textoComentario.setText(comentario)
			except:
				self.close()

	def aceptar(self):
		global textoComentario
		textoComentario=str(self.ui.textoComentario.text())
		self.accept()

	def obtenerDatos(self):
		global textoComentario
		return textoComentario

class ordenCrearSubrutina(QtGui.QDialog):
	def __init__(self,parent=None):
		global indiceTabla, listaOrdenes, listaInfo
		indiceTabla, listaOrdenes, listaInfo=0,[],[]
		QtGui.QWidget.__init__(self,parent,QtCore.Qt.WindowStaysOnTopHint)
		self.ui = Ui_crearSubrutina()
		self.ui.setupUi(self)
		self.cargarSubrutinasDisponibles()
		self.connect(self.ui.botonGuardar, QtCore.SIGNAL('clicked()'), self.guardar)
		self.connect(self.ui.botonEliminarSubrutina, QtCore.SIGNAL('clicked()'), self.eliminarSubrutina)
		self.connect(self.ui.botonCargar, QtCore.SIGNAL('clicked()'), self.cargarSubrutina)
		self.connect(self.ui.botonStop, QtCore.SIGNAL('clicked()'), self.botonStop)
		self.connect(self.ui.botonBorrarOrden, QtCore.SIGNAL('clicked()'), self.botonBorrarOrdenes)
		self.connect(self.ui.botonLinea, QtCore.SIGNAL('clicked()'), self.ejecutarLinea)

		self.ui.arbolComandos.doubleClicked.connect(self.seleccionadaOrden)

	def cargarSubrutinasDisponibles(self):
		path = './data/'
		subrutinas = []
		for i in os.listdir(path):
			if os.path.isfile(os.path.join(path,i)) and 'subrutina_' in i:
				indice=self.ui.nombreSubrutina.count()
				self.ui.nombreSubrutina.insertItem(indice,i[10:])

	def botonBorrarOrdenes(self):
		global listaOrdenes, listaInfo
		if self.ui.botonBorrarOrden.isChecked():
			self.mostrarCheck()

		elif not self.ui.botonBorrarOrden.isChecked():
			indicesParaBorrar=self.ordenesMarcadasParaBorrar()
			if indicesParaBorrar!=0:
				self.borrarOrdenes(indicesParaBorrar)
				#FIXME no puedo borrar la tabla con una funcion, debo hacerlo con
				#las ordenes escritas aqui directamente
				self.ui.tablaOrdenes.setRowCount(0)
				self.ui.tablaOrdenes.setRowCount(1)
				indiceTabla=0
				self.guardarEnArchivoTemporal()
				self.cargarArchivoTemporal()

	def mostrarCheck(self):
		for i in range(len(listaOrdenes)):
			item=QtGui.QTableWidgetItem(str(self.ui.tablaOrdenes.item(0,i).text()))
			item.setCheckState(0)
			self.ui.tablaOrdenes.setItem(0,i,item)

	def ordenesMarcadasParaBorrar(self):
		global listaOrdenes
		borrarIndices=[]
		for i in range(len(listaOrdenes)):
			item=self.ui.tablaOrdenes.item(0,i)
			if(item.checkState()==2):
				borrarIndices.append(i)
		borrarIndices.reverse()
		return borrarIndices

	def borrarOrdenes(self,listaABorrar):
		global listaOrdenes, listaInfo
		for i in listaABorrar:
			listaOrdenes.pop(i)
			listaInfo.pop(i)

	def guardarEnArchivoTemporal(self):
		self.guardarEnArchivo('data/temporal_subrutina.txt')

	def cargarArchivoTemporal(self):
		global indiceTabla, listaInfo, listaOrdenes
		listaInfo, listaOrdenes=[],[]
		lineasArchivoTemporal=self.cargarLineasArchivo('data/temporal_subrutina.txt')
		estado=self.cargarOrdenesEnTabla(lineasArchivoTemporal)
		if estado[0]:
			indiceTabla=estado[1]
		else:
			self.borrarTablaCompleta()
			mensaje=u'El archivo no es válido'
			QtGui.QMessageBox.about(self, 'Error',mensaje)

	''' boton STOP '''
	def botonStop(self):
		#fileStop=open("../data/stop.txt",'w+')
		#fileStop.write("true")
		#fileStop.close()
		pararRobot()
		pararPinza()

	def guardarOrden(self,datos, indice):
		global indiceTabla, listaOrdenes, listaInfo
		listaOrdenes.insert(indice, datos[0])
		listaInfo.insert(indice, datos[1])
		print "listaOrdenes: ", listaOrdenes
		print "listaInfo: ", listaInfo

	def seleccionadaOrden(self):
		global indiceTabla, listaOrdenes, listaInfo
		# Esto es para saber que orden se ha seleccionado
		item=self.ui.arbolComandos.currentItem()     #item actual seleccionado
		item_parent=item.parent()           # item de orden superior al seleccionado
		model_parent=self.ui.arbolComandos.indexFromItem(item_parent,0)
		parent=model_parent.row()           # 0=Control ejes, 1=programa flujo
		model=self.ui.arbolComandos.indexFromItem(item,0)
		fila=model.row()                    # orden selecconionada dentro de cada 'parent'

		indiceTabla=len(listaOrdenes)-1
		indice=self.ui.tablaOrdenes.selectedItems()
		if indice!=-1 and len(indice)!=0:
			indiceTabla=indice[0].row()
		indiceTabla+=1

		print "indiceTabla: ",indiceTabla
		print

		if parent==0:
			if fila==0: #abrir pinza
				datos=['ABRIR_PINZA',None]
				self.guardarOrden(datos,indiceTabla)
				#self.mostrarMensaje('Abrir Pinza',indiceTabla)
				#indiceTabla+=1
			elif fila==1: #cerrar pinza
				datos=['CERRAR_PINZA',None]
				self.guardarOrden(datos,indiceTabla)
				#self.mostrarMensaje('Cerrar Pinza',indiceTabla)
				#indiceTabla+=1
			elif fila==2: #ir
				ventanaIr=ordenIr()
				if ventanaIr.exec_():
					datosObtenidos=ventanaIr.obtenerDatos()
					ventanaIr.accept()
					datos=["IR", datosObtenidos]
					self.guardarOrden(datos,indiceTabla)
					#mensaje=crearMensaje(datos)
					#self.mostrarMensaje(mensaje,indiceTabla)
					#indiceTabla+=1
				else:
					ventanaIr.close()
					print "Rechazada"


			elif fila==3: #ir segun articulaciones
				ventanaTrayectoriaArticulaciones=ordenTrayectoriaArticulaciones()
				if ventanaTrayectoriaArticulaciones.exec_():
					datosInfo=ventanaTrayectoriaArticulaciones.obtenerDatos()
					datosObtenidos=["TR_ARTICULACIONES", datosInfo]
					ventanaTrayectoriaArticulaciones.accept()
					self.guardarOrden(datosObtenidos,indiceTabla)
				else:
					ventanaTrayectoriaArticulaciones.close()
				'''
				ventanaIrArticulaciones=ordenIrArticulaciones()
				if ventanaIrArticulaciones.exec_():
					datosInfo=ventanaIrArticulaciones.obtenerDatos()
					datosObtenidos=["ARTICULACIONES", datosInfo]
					ventanaIrArticulaciones.accept()
					self.guardarOrden(datosObtenidos,indiceTabla)
				else:
					ventanaIrArticulaciones.close()
				'''

			elif fila==4: #ir segun xyz
				ventanaTrayectoriaCartesiana=ordenTrayectoriaCartesiana()
				if ventanaTrayectoriaCartesiana.exec_():
					datosInfo=ventanaTrayectoriaCartesiana.obtenerDatos()
					datosObtenidos=["TR_XYZ", datosInfo]
					ventanaTrayectoriaCartesiana.accept()
					self.guardarOrden(datosObtenidos,indiceTabla)
				else:
					ventanaTrayectoriaCartesiana.close()
				'''
				ventanaIrXYZ=ordenIrXYZ()
				if ventanaIrXYZ.exec_():
					datosInfo=ventanaIrXYZ.obtenerDatos()
					datosObtenidos=["XYZ", datosInfo]
					ventanaIrXYZ.accept()
					self.guardarOrden(datosObtenidos,indiceTabla)
				else:
					ventanaIrXYZ.close()
				'''


			elif fila==5: #ir linealmente
				ventanaLineal=ordenLineal()
				if ventanaLineal.exec_():
					datosObtenidos=ventanaLineal.obtenerDatos()
					ventanaLineal.accept()
					datos=["LINEAL", datosObtenidos]
					self.guardarOrden(datos,indiceTabla)
					#mensaje=crearMensaje(datos)
					#self.mostrarMensaje(mensaje,indiceTabla)
					#indiceTabla+=1
				else:
					ventanaLineal.close()
					print "Rechazada"
			elif fila==6: #ir circularmente
				ventanaCircular=ordenCircular()
				if ventanaCircular.exec_():
					datosObtenidos=ventanaCircular.obtenerDatos()
					ventanaCircular.accept()
					datos=["CIRCULAR", datosObtenidos]
					self.guardarOrden(datos,indiceTabla)
					#mensaje=crearMensaje(datos)
					#self.mostrarMensaje(mensaje,indiceTabla)
					#indiceTabla+=1
				else:
					ventanaCircular.close()
					print "Rechazada"
			elif fila==7: #memorizar posicion
				ventanaMemorizar=ordenMemorizar()
				if ventanaMemorizar.exec_():
					datosObtenidos=ventanaMemorizar.obtenerDatos()
					ventanaMemorizar.accept()
					datos=["MEMORIZAR", datosObtenidos]
					self.guardarOrden(datos,indiceTabla)
					#mensaje=crearMensaje(datos)
					#self.mostrarMensaje(mensaje,indiceTabla)
					#indiceTabla+=1
				else:
					ventanaMemorizar.close()
					print "Rechazada"
		elif parent==1:
			if fila==0: #esperar
				ventanaEsperar=ordenEsperar()
				if ventanaEsperar.exec_():
					datosObtenidos=ventanaEsperar.obtenerDatos()
					ventanaEsperar.accept()
					datos=["ESPERAR", datosObtenidos]
					self.guardarOrden(datos,indiceTabla)
					#mensaje=crearMensaje(datos)
					#self.mostrarMensaje(mensaje,indiceTabla)
					#indiceTabla+=1
				else:
					ventanaEsperar.close()
					print "Rechazada"
			elif fila==1: #comentario
				ventanaComentario=ordenComentario()
				if ventanaComentario.exec_():
					datosObtenidos=ventanaComentario.obtenerDatos()
					ventanaComentario.accept()
					datos=["COMENTARIO", datosObtenidos]
					self.guardarOrden(datos,indiceTabla)
					#mensaje=crearMensaje(datos)
					#self.mostrarMensaje(mensaje,indiceTabla)
					#indiceTabla+=1
				else:
					ventanaComentario.close()
					print "Rechazada"
			elif fila==2: #cambiar valor variable
				ventanaVariable=ordenVariable()
				if ventanaVariable.exec_():
					datosObtenidos=ventanaVariable.obtenerDatos()
					ventanaVariable.accept()
					print "VARIABLE"
					print "datosObtenidos: ", datosObtenidos
					datos=["VARIABLE", datosObtenidos]
					self.guardarOrden(datos,indiceTabla)
					#mensaje=crearMensaje(datos)
					#self.mostrarMensaje(mensaje,indiceTabla)
					#indiceTabla+=1
				else:
					ventanaVariable.close()
					print "Rechazada"

		self.guardarEnArchivoTemporal()
		self.borrarTablaCompleta()
		self.cargarArchivoTemporal()


	def mostrarMensaje(self, texto, indice):
		valorCasillaMensaje=QtGui.QTableWidgetItem(texto)
		self.ui.tablaOrdenes.setItem(indice,0,valorCasillaMensaje)
		#valorCasillaMensaje.setFlags(QtCore.Qt.ItemIsSelectable)
		self.ui.tablaOrdenes.insertRow(indice+1)

	''' boton Guardar '''
	def guardar(self):
		global listaInfo, listaOrdenes
		nombreSubrutina=self.ui.nombreSubrutina.currentText()
		if self.guardarEnArchivo('./data/subrutina_'+nombreSubrutina):
			# guardado con exito
			indice=self.ui.nombreSubrutina.count()
			self.ui.nombreSubrutina.insertItem(indice,nombreSubrutina)
			print "Ha salido bien"
		else:
			print "Ha salido mal"

	def guardarEnArchivo(self,direccionGuardar):
		try:
			archivoGuardar=open(direccionGuardar,'w+')
			for i in range(len(listaOrdenes)):
				if listaOrdenes[i]=="ABRIR_PINZA":
					archivoGuardar.write(listaOrdenes[i]+"\n")
				elif listaOrdenes[i]=="CERRAR_PINZA":
					archivoGuardar.write(listaOrdenes[i]+"\n")
				elif listaOrdenes[i]=="IR":
					archivoGuardar.write(listaOrdenes[i]+" "+textoAGuardar(listaInfo[i]))
				elif listaOrdenes[i]=="TR_ARTICULACIONES":
					archivoGuardar.write(listaOrdenes[i]+" "+str(listaInfo[i])+"\n")
				elif listaOrdenes[i]=="TR_XYZ":
					archivoGuardar.write(listaOrdenes[i]+" "+str(listaInfo[i])+"\n")
				elif listaOrdenes[i]=="ARTICULACIONES":
					archivoGuardar.write(listaOrdenes[i]+" "+textoAGuardar(listaInfo[i]))
				elif listaOrdenes[i]=="XYZ":
					archivoGuardar.write(listaOrdenes[i]+" "+textoAGuardar(listaInfo[i]))
				elif listaOrdenes[i]=="LINEAL":
					archivoGuardar.write(listaOrdenes[i]+" "+textoAGuardar(listaInfo[i]))
				elif listaOrdenes[i]=="CIRCULAR":
					archivoGuardar.write(listaOrdenes[i]+" "+textoAGuardar(listaInfo[i]))
				elif listaOrdenes[i]=="MEMORIZAR":
					archivoGuardar.write(listaOrdenes[i]+" "+str(listaInfo[i])+"\n")
				elif listaOrdenes[i]=="ESPERAR":
					archivoGuardar.write(listaOrdenes[i]+" "+str(listaInfo[i])+"\n")
				elif listaOrdenes[i]=="SALTAR":
					archivoGuardar.write(listaOrdenes[i]+" "+str(listaInfo[i])+"\n")
				elif listaOrdenes[i]=="COMENTARIO":
					archivoGuardar.write(listaOrdenes[i]+" "+str(listaInfo[i])+"\n")
				elif listaOrdenes[i]=="ETIQUETA":
					archivoGuardar.write(listaOrdenes[i]+" "+str(listaInfo[i])+"\n")
				elif listaOrdenes[i]=="VARIABLE":
					archivoGuardar.write(listaOrdenes[i]+" "+textoAGuardar(listaInfo[i]))
			archivoGuardar.close()
			return True
		except:
			return False

	''' boton Eliminar Subrutina '''
	def eliminarSubrutina(self):
		nombreSubrutina=self.ui.nombreSubrutina.currentText()
		indiceSubrutina=self.ui.nombreSubrutina.currentIndex()
		try:
			os.remove('./data/subrutina_'+nombreSubrutina)
			self.ui.nombreSubrutina.removeItem(indiceSubrutina)
		except:
			pass

	''' boton Cargar '''
	def cargarSubrutina(self):
		global listaOrdenes, listaInfo, indiceTabla

		nombreSubrutina=self.ui.nombreSubrutina.currentText()
		direccionArchivo='./data/subrutina_'+nombreSubrutina

		lineasArchivo=self.cargarLineasArchivo(direccionArchivo)
		self.borrarTablaCompleta()
		estado=self.cargarOrdenesEnTabla(lineasArchivo)
		if estado[0]:
			indiceTabla=estado[1]
			#realizado con exito
		else:
			self.borrarTablaCompleta()
			mensaje=u'El archivo no es válido'
			QtGui.QMessageBox.about(self, 'Error',mensaje)

	def cargarLineasArchivo(self,direccionArchivo):
		archivo=open(direccionArchivo,'r')
		return archivo.readlines()

	def borrarTablaCompleta(self):
		global indiceTabla
		self.ui.tablaOrdenes.setRowCount(0)
		self.ui.tablaOrdenes.setRowCount(1)
		indiceTabla=0

	def cargarOrdenesEnTabla(self,lineasArchivo):
		indiceIntroducirTabla=0
		#[ok, ok_posicion]=[False, False]
		ok=False
		for i in range(len(lineasArchivo)):
			contenidoLinea=lineasArchivo[i]
			contenidoLinea=contenidoLinea.split()
			if contenidoLinea[0]=="ABRIR_PINZA":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarAbrirPinza(contenidoLinea)
			elif contenidoLinea[0]=="CERRAR_PINZA":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarCerrarPinza(contenidoLinea)
			elif contenidoLinea[0]=="IR":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarIr(contenidoLinea)
			elif contenidoLinea[0]=="TR_ARTICULACIONES":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarTrayectoriaArticulaciones(contenidoLinea)
			elif contenidoLinea[0]=="TR_XYZ":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarTrayectoriaCartesiana(contenidoLinea)
			elif contenidoLinea[0]=="ARTICULACIONES":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarIrArticulaciones(contenidoLinea)
			elif contenidoLinea[0]=="XYZ":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarIrXYZ(contenidoLinea)
			elif contenidoLinea[0]=="LINEAL":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarLineal(contenidoLinea)
			elif contenidoLinea[0]=="CIRCULAR":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarCircular(contenidoLinea)
			elif contenidoLinea[0]=="MEMORIZAR":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarMemorizar(contenidoLinea)
			elif contenidoLinea[0]=="ESPERAR":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarEsperar(contenidoLinea)
			elif contenidoLinea[0]=="SALTAR":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarSaltar(contenidoLinea)
			elif contenidoLinea[0]=="COMENTARIO":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarComentario(contenidoLinea)
			elif contenidoLinea[0]=="ETIQUETA":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarEtiqueta(contenidoLinea)
			elif contenidoLinea[0]=="VARIABLE":
				[ok, datosAGuardar, mensajeAMostrar]=comprobarVariable(contenidoLinea)
			else:
				ok=False
			if contenidoLinea[0]=="POSICION":
				[ok_posicion, numeroPosicion, valorPosicion]=comprobarPosicion(contenidoLinea)
				print valorPosicion
			if ok:
				self.guardarOrden([contenidoLinea[0], datosAGuardar],i)
				self.mostrarMensaje(mensajeAMostrar, indiceIntroducirTabla)
				indiceIntroducirTabla+=1
			'''
			if ok_posicion:
				print i
				archivoPosiciones=open('./posiciones/P'+str(numeroPosicion),'w+')
				archivoPosiciones.write("POSICION "+str(numeroPosicion)+" "+' '.join(str(e) for e in valorPosicion))
			if ok==False and ok_posicion==False:
				return False
			'''
			if ok==False:
				return False
		return [True, indiceIntroducirTabla]

	''' boton Ejecutar Linea '''
	def ejecutarLinea(self):
		global listaOrdenes, listaInfo, indiceTabla
		if indiceTabla==len(listaOrdenes):
			indiceTabla=0
		self.marcarLineaAEjecutar(indiceTabla)
		self.realizarAccion(indiceTabla)
		indiceTabla+=1

	def marcarLineaAEjecutar(self, indice):
		itemUltimo=self.ui.tablaOrdenes.item(0,len(listaOrdenes)-1)
		if indice==0 and self.ui.tablaOrdenes.isItemSelected(itemUltimo):
			self.ui.tablaOrdenes.setItemSelected(itemUltimo,False)
		if not indice==0:
			itemAnterior=self.ui.tablaOrdenes.item(0,indice-1)
			self.ui.tablaOrdenes.setItemSelected(itemAnterior,False)
		itemActual=self.ui.tablaOrdenes.item(0,indice)
		self.ui.tablaOrdenes.setItemSelected(itemActual,True)

	def realizarAccion(self,indiceLocal):
		global listaOrdenes, listaInfo, indice
		if listaOrdenes[indiceLocal]=="ABRIR_PINZA":
			print "Abre la pinza"
			abrirPinza()
		elif listaOrdenes[indiceLocal]=="CERRAR_PINZA":
			print "Cierra la pinza"
			cerrarPinza()
		elif listaOrdenes[indiceLocal]=="IR":
			print "Se mueve a una posicion"
			numeroPosicion, tipo, valor=listaInfo[indiceLocal]
			if tipo=='tiempo':
				tiempo=valor
			elif tipo=='velocidad':
				th_inicial=leerArticulacionesActuales()
				datos=obtenerDatosDePosicion(numeroPosicion)
				[x,y,z,roll,pitch,yaw]=datos[2:]
				matH=matHomo([x,y,z,roll,pitch,yaw])
				th_final=ikine(matH,th_inicial)
				if len(th_final)!=0:
					th=th_final[:6]
				tiempo=obtenerTiempoDesdeVelocidad(th,valor)
			irPosicion(numeroPosicion, tiempo)
		elif listaOrdenes[indiceLocal]=="TR_ARTICULACIONES":
			direccionArchivo=listaInfo[indiceLocal]
			datos=np.loadtxt(direccionArchivo)
			tiempo_espera=ejecutar_trayectoria_articulaciones(datos)
			return indiceLocal, tiempo_espera
		elif listaOrdenes[indiceLocal]=="TR_XYZ":
			direccionArchivo=listaInfo[indiceLocal]
			datos=np.loadtxt(direccionArchivo)
			tiempo_espera=ejecutar_trayectoria_cartesiana(datos)
			return indiceLocal, tiempo_espera
		elif listaOrdenes[indiceLocal]=="ARTICULACIONES":
			articulaciones=listaInfo[indiceLocal][:6]
			valor=listaInfo[indiceLocal][7]
			tipo=listaInfo[indiceLocal][6]
			tiempo_espera=irPorArticulaciones(articulaciones, int(valor), tipo)
			return indiceLocal, tiempo_espera
		elif listaOrdenes[indiceLocal]=="XYZ":
			datos_posicion=listaInfo[indiceLocal][:6]
			valor=listaInfo[indiceLocal][7]
			tipo=listaInfo[indiceLocal][6]
			tiempo_espera=irPorXYZ(datos_posicion, int(valor), tipo)
			return indiceLocal, tiempo_espera
		elif listaOrdenes[indiceLocal]=="LINEAL":
			print "Se mueve linealmente a una posicion"
			numeroPosicion, tipo, valor=listaInfo[indiceLocal]
			if tipo=='tiempo':
				tiempo=valor
			elif tipo=='velocidad':
				tiempo=obtenerTiempoDesdeVelocidad(valor)
			movimientoLineal(numeroPosicion,tiempo)
		elif listaOrdenes[indiceLocal]=="CIRCULAR":
			print "Se mueve circularmente a una posicion"
		elif listaOrdenes[indiceLocal]=="MEMORIZAR":
			print "Memoriza una posicion"
			numeroPosicion=memorizar(listaInfo[indiceLocal])
		elif listaOrdenes[indiceLocal]=="ESPERAR":
			print "Esperar un tiempo"
			esperarRobot(listaInfo[indiceLocal])
		elif listaOrdenes[indiceLocal]=="SALTAR":
			print "Saltar a una etiqueta"
			etiqueta=listaInfo[indiceLocal]
			indice=saltaA(etiqueta, listaOrdenes, listaInfo)
		elif listaOrdenes[indiceLocal]=="COMENTARIO":
			#
			print "Comentario"
		elif listaOrdenes[indiceLocal]=="PONER_VARIABLE":
			print "Modificar el valor de una variable"
			#diccionarioVariables[nombreVariable]=ponerVariable(expresion)
		elif listaOrdenes[indiceLocal]=="ETIQUETA":
			#
			print "Anadir una etiqueta"





class ordenSeleccionarSubrutina(QtGui.QDialog):
	def __init__(self,parent=None, listaInfo=[]):
		QtGui.QWidget.__init__(self,parent,QtCore.Qt.WindowStaysOnTopHint)
		self.ui = Ui_SeleccionarSubrutina()
		self.ui.setupUi(self)
		self.listaInfo=listaInfo
		QtCore.QObject.connect(self.ui.botonAceptar, QtCore.SIGNAL('clicked()'), self.aceptar)
		self.cargarSubrutinasDisponibles()
		self.cargarValores()

	def cargarValores(self):
		if self.listaInfo!=[]:
			try:
				subrutina=str(self.listaInfo)
				indiceSubrutina=self.ui.nombresSubrutinas.findText(subrutina)
				self.ui.nombresSubrutinas.setCurrentIndex(indiceSubrutina)
			except:
				self.close()

	def cargarSubrutinasDisponibles(self):
		path = './data/'
		subrutinas = []
		for i in os.listdir(path):
			if os.path.isfile(os.path.join(path,i)) and 'subrutina_' in i:
				indice=self.ui.nombresSubrutinas.count()
				self.ui.nombresSubrutinas.insertItem(indice,i[10:])

	def aceptar(self):
		global nombreSubrutina
		nombreSubrutina=str(self.ui.nombresSubrutinas.currentText())
		self.accept()

	def obtenerDatos(self):
		global nombreSubrutina
		return nombreSubrutina

class ordenSiSensores(QtGui.QDialog):
	def __init__(self,parent=None, listaInfo=[]):
		QtGui.QWidget.__init__(self,parent,QtCore.Qt.WindowStaysOnTopHint)
		self.ui = Ui_Sensores()
		self.ui.setupUi(self)
		self.listaInfo=listaInfo
		self.cargarEtiquetas()
		self.cargarSubrutinas()
		QtCore.QObject.connect(self.ui.buttonAceptar, QtCore.SIGNAL('clicked()'), self.aceptar)
		self.cargarValores()

	def cargarValores(self):
		if self.listaInfo!=[]:
			try:
				tipo=self.listaInfo[0]
				if tipo=="contacto":
					tipo="Contacto"
				elif tipo=="laser":
					tipo="Presencia"
				numero=self.listaInfo[1]
				estadoSensor=self.listaInfo[2]
				tipoDestino=self.listaInfo[3]
				destino=self.listaInfo[4]

				indiceTipo=self.ui.listaTipoSensores.findText(tipo)
				print "indiceTipo: ", indiceTipo
				self.ui.listaTipoSensores.setCurrentIndex(indiceTipo)
				indiceNumero=self.ui.listaNumeroSensor.findText(numero)
				print "indiceNumero: ", indiceNumero
				self.ui.listaNumeroSensor.setCurrentIndex(indiceNumero)
				indiceEstado=self.ui.estadoSensor.findText(estadoSensor)
				print "indiceEstado: ", indiceEstado
				self.ui.estadoSensor.setCurrentIndex(indiceEstado)
				if destino=="ETIQUETA":
					self.ui.radioEtiqueta.setChecked(True)
					indiceDestino=self.ui.listaEtiquetas.findText(destino)
					self.ui.listaEtiquetas.setCurrentIndex(indiceDestino)
				elif destino=="SUBRUTINA":
					self.ui.radioSubrutina.setChecked(True)
					indiceDestino=self.ui.listaSubrutinas.findText(destino)
					self.ui.listaSubrutinas.setCurrentIndex(indiceDestino)
			except:
				self.close()

	def cargarEtiquetas(self):
		archivo=open("./data/etiquetasDisponibles", 'r')
		lineas=archivo.readlines()
		lineas=list(set(lineas))
		for i in lineas:
			indice=self.ui.listaEtiquetas.count()
			self.ui.listaEtiquetas.insertItem(indice,i.rstrip())

	def cargarSubrutinas(self):
		path = './data/'
		subrutinas = []
		for i in os.listdir(path):
			if os.path.isfile(os.path.join(path,i)) and 'subrutina_' in i:
				indice=self.ui.listaSubrutinas.count()
				self.ui.listaSubrutinas.insertItem(indice,i[10:])

	def aceptar(self):
		global tipoSensor, numero, estado, tipoOpcion, destinoFinal
		if self.ui.radioEtiqueta.isChecked():
			tipoOpcion="ETIQUETA"
			destinoFinal=self.ui.listaEtiquetas.currentText()
		elif self.ui.radioSubrutina.isChecked():
			tipoOpcion="SUBRUTINA"
			destinoFinal=self.ui.listaSubrutinas.currentText()

		nombreSensor=self.ui.listaTipoSensores.currentText()
		if nombreSensor=="Contacto":
			tipoSensor="contacto"
		elif nombreSensor=="Presencia":
			tipoSensor="laser"
		numero=self.ui.listaNumeroSensor.currentText()

		estado=self.ui.estadoSensor.currentText()
		if destinoFinal=="" or numero=="" or tipoSensor=="":
			self.close()
		else:
			self.accept()
	def obtenerDatos(self):
		global tipoSensor, numero, estado, tipoOpcion, destinoFinal
		return [str(tipoSensor), str(numero), str(estado), str(tipoOpcion), str(destinoFinal)]

class ordenInterrupciones(QtGui.QDialog):
	def __init__(self,parent=None, listaInfo=[]):
		QtGui.QWidget.__init__(self,parent,QtCore.Qt.WindowStaysOnTopHint)
		self.ui = Ui_Interrupciones()
		self.ui.setupUi(self)
		self.listaInfo=listaInfo
		#self.cargarSensores()
		self.cargarSubrutinas()
		QtCore.QObject.connect(self.ui.botonAceptar, QtCore.SIGNAL('clicked()'), self.aceptar)
		self.cargarValores()

	def cargarValores(self):
		if self.listaInfo!=[]:
			try:
				tipo=self.listaInfo[0]
				if tipo=="contacto":
					tipo="Contacto"
				elif tipo=="laser":
					tipo="Presencia"
				numero=self.listaInfo[1]
				subrutina=self.listaInfo[2]
				estado=self.listaInfo[3]
				indiceTipo=self.ui.listaTipoSensores.findText(tipo)
				self.ui.listaTipoSensores.setCurrentIndex(indiceTipo)
				#indiceNumero=self.ui.listaNumeroSensor.findText(numero)
				#self.ui.listaNumeroSensor.setCurrentIndex(indiceNumero)
				self.ui.numeroSensor.setText(numero)
				indiceSubrutina=self.ui.listaSubrutinas.findText(subrutina)
				self.ui.listaSubrutinas.setCurrentIndex(indiceSubrutina)
			except:
				self.close()

	def cargarSubrutinas(self):
		path = './data/'
		subrutinas = []
		for i in os.listdir(path):
			if os.path.isfile(os.path.join(path,i)) and 'subrutina_' in i:
				indice=self.ui.listaSubrutinas.count()
				self.ui.listaSubrutinas.insertItem(indice,i[10:])

	def aceptar(self):
		global sensor, subrutina, estadoDeseado, numero
		nombreSensor=self.ui.listaTipoSensores.currentText()
		if nombreSensor=="Contacto":
			sensor="contacto"
		elif nombreSensor=="Presencia":
			sensor="laser"
		#numero=self.ui.listaNumeroSensor.currentText()
		try:
			numero=self.ui.numeroSensor.text()
			int(numero)
		except:
			print "Debe ser un numero entero!"
		subrutina=self.ui.listaSubrutinas.currentText()
		if nombreSensor!="" and subrutina!="" and numero!="":
			if self.ui.radioActivar.isChecked():
				estadoDeseado="activar"
			elif self.ui.radioDesactivar.isChecked():
				estadoDeseado="desactivar"
			self.accept()
		else:
			self.close()

	def obtenerDatos(self):
		global sensor, subrutina, estadoDeseado, numero
		return [str(sensor), str(numero), str(subrutina), str(estadoDeseado)]
