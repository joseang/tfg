#!/usr/bin/env python
# -*- coding: utf-8 -*-
from crearMensaje import *
import numpy as np
import sys
sys.path.insert(0, '../')

from path_absoluto import obtenerDireccionAbsoluta
direccionAbsoluta=obtenerDireccionAbsoluta()

def comprobarAbrirPinza(contenido):
	if contenido[0]=="ABRIR_PINZA":
		mensaje=crearMensaje(contenido)
		return [True,None,mensaje]
	else:
		return [False,None,None]

def comprobarCerrarPinza(contenido):
	if contenido[0]=="CERRAR_PINZA":
		mensaje=crearMensaje(contenido)
		return [True,None,mensaje]
	else:
		return [False,None,None]

def comprobarIr(contenido):
	try:
		if len(contenido)>2:
			posicion=int(contenido[1])
			valor=int(contenido[3])
			if contenido[0]=="IR" and len(contenido)==4 and contenido[2]=="tiempo" or contenido[2]=="velocidad":
				mensaje=crearMensaje(contenido)
				datos=[posicion, contenido[2], valor]
				#print "mensaje: ", mensaje
				return [True, datos, mensaje]
			else:
				return [False,None,None]
		else:
			posicion=int(contenido[1][0])
			valor=int(contenido[1][2])
			if contenido[0]=="IR" and contenido[1]=="tiempo" or contenido[1]=="velocidad":
				mensaje=crearMensaje(contenido)
				datos=[posicion, contenido[1], valor]
				#print "mensaje: ", mensaje
				return [True, datos, mensaje]
			else:
				return [False,None,None]
	except:
		return [False,None,None]

def comprobarTrayectoriaArticulaciones(contenido):
	try:
		orden=contenido[0]
		direccionArchivo=contenido[1]
		if orden=="TR_ARTICULACIONES":
			datos=np.loadtxt(str(direccionArchivo))
			print "Datos: \n", datos
			for fila in datos:
				for i in fila:
					float(i)
			try:
				print "segundo try"
				for fila in datos:
					if len(fila)!=7:
						return [False, None, None]
				print "pre-texto"
				texto="Ir segun trayectoria de articulaciones. Archivo: "+str(direccionArchivo)
				return [True, direccionArchivo, texto]
			except:
				return [False, None, None]
		else:
			return [False, None, None]
	except:
		return [False, None, None]

def comprobarTrayectoriaCartesiana(contenido):
	try:
		orden=contenido[0]
		direccionArchivo=contenido[1]
		if orden=="TR_XYZ":
			datos=np.loadtxt(str(direccionArchivo))
			for fila in datos:
				for i in fila:
					float(i)
			try:
				for fila in datos:
					if len(fila)!=7:
						return [False, None, None]
				texto="Ir segun trayectoria XYZ. Archivo: "+str(direccionArchivo)
				return [True, direccionArchivo, texto]
			except:
				return [False, None, None]
		else:
			return [False, None, None]
	except:
		return [False, None, None]

def comprobarLineal(contenido):
	try:
		if len(contenido)!=2:
			posicion=int(contenido[1])
			valor=int(contenido[3])
			if contenido[0]=="LINEAL" and len(contenido)==4 and contenido[2]=="tiempo" or contenido[2]=="velocidad":
				mensaje=crearMensaje(contenido)
				datos=[posicion, contenido[2], valor]
				return [True, datos, mensaje]
			else:
				return [False,None,None]
		else:
			posicion=int(contenido[0][0])
			valor=int(contenido[0][2])
			if contenido[0]=="LINEAL" and contenido[1]=="tiempo" or contenido[1]=="velocidad":
				mensaje=crearMensaje(contenido)
				datos=[posicion, contenido[1], valor]
				return [True, datos, mensaje]
			else:
				return [False,None,None]
	except:
		return [False,None,None]

def comprobarCircular(contenido):
	try:
		if len(contenido)>2:
			posicionFinal=int(contenido[1])
			posicionMedia=int(contenido[2])
			valor=int(contenido[4])
			if contenido[0]=="CIRCULAR" and len(contenido)==5 and contenido[3]=="tiempo" or contenido[3]=="velocidad":
				mensaje=crearMensaje(contenido)
				datos=[posicionFinal, posicionMedia, contenido[3], valor]
				return [True, datos , mensaje]
			else:
				return [False, None, None]
		else:
			posicionFinal=int(contenido[0][0])
			posicionMedia=int(contenido[0][1])
			valor=int(contenido[0][3])
			if contenido[0]=="CIRCULAR" and contenido[0][2]=="tiempo" or contenido[0][2]=="velocidad":
				mensaje=crearMensaje(contenido)
				datos=[posicionFinal, posicionMedia, contenido[2], valor]
				return [True, datos , mensaje]
			else:
				return [False, None, None]
	except Exception, e:
		#print str(e)
		return [False, None, None]

def comprobarMemorizar(contenido):
	try:
		posicion=int(contenido[1])
		if contenido[0]=="MEMORIZAR" and len(contenido)==2:
			mensaje=crearMensaje(contenido)
			return [True, posicion, mensaje]
		else:
			return [False, None]
	except:
		return [False, None]

def comprobarEsperar(contenido):
	try:
		tiempo=int(contenido[1])
		if contenido[0]=="ESPERAR" and len(contenido)==2:
			mensaje=crearMensaje(contenido)
			return [True, tiempo, mensaje]
		else:
			return [False, None, None]
	except Exception, e:
		#print str(e)
		return [False, None, None]

def comprobarSaltar(contenido):
	try:
		#print "contenido[1]: ", contenido[1]
		etiqueta=contenido[1]
		if contenido[0]=="SALTAR" and len(contenido)==2:
			mensaje=crearMensaje(contenido)
			return [True, etiqueta, mensaje]
		else:
			return [False, None, None]
	except:
		return [False, None, None]

def comprobarSaltarSi(contenido):
	try:
		#print "contenido: "
		#print contenido
		valorReferencia=contenido[1]
		nombreVariable=contenido[2]
		comparacion=contenido[3]
		etiqueta=contenido[4]
		texto="Si "+nombreVariable+" "+comparacion+" "+valorReferencia+", "+"saltar a: "+etiqueta
		#print texto
		return [True, contenido[1:], texto]
	except:
		#print "ERROR"
		return [False, None, None]

def comprobarComentario(contenido):
	try:
		if contenido[0]=="COMENTARIO":
			#print "contenido comentario: ", contenido
			mensaje=crearMensaje(contenido)
			textoComentario=" ".join(contenido[1:])
			return [True, textoComentario, mensaje]
		else:
			return [False, None, None]
	except:
		return [False, None, None]

def comprobarEtiqueta(contenido):
	try:
		nombreEtiqueta=contenido[1]
		if contenido[0]=="ETIQUETA" and len(contenido)==2:
			mensaje=crearMensaje(contenido)
			archivo=open(direccionAbsoluta+'/data/etiquetasDisponibles', 'a')
			archivo.write(nombreEtiqueta+"\n")
			archivo.close()
			return [True, nombreEtiqueta, mensaje]
		else:
			return [False, None, None]
	except:
		return [False, None, None]

def comprobarVariable(contenido):
	try:
		print "contenido: ", contenido
		nombreVariable=contenido[1]
		expresion=contenido[2]
		texto=nombreVariable+" = "+expresion
		infoGuardar=[nombreVariable, expresion]
		return [True, infoGuardar, texto]
	except:
		#print [False, None, None]
		return [False, None, None]

def comprobarLlamarSubrutina(contenido):
	try:
		nombreSubrutina=contenido[1]
		if contenido[0]=="LLAMAR_SUBRUTINA" and len(contenido)==2:
			mensaje=crearMensaje(contenido)
			return [True, nombreSubrutina, mensaje]
		else:
			#print "return: false"
			return [False, None, None]
	except Exception, e:
		#print str(e)
		return [False, None, None]

def comprobarPosicion(contenido):
	try:
		numeroPosicion=int(contenido[1])
		valorPosicion=[]
		for i in range(2,10):
			valorPosicion.append(float(contenido[i]))
		if contenido[0]=="POSICION":
			return [True, numeroPosicion, valorPosicion]
		else:
			return [False, None, None]
	except Exception, e:
		#print str(e)
		return [False, None, None]

def comprobarSiSensor(contenido):
	try:
		if contenido[0]=="COMPROBAR_SENSOR" and len(contenido)==2:
			if contenido[1][0]=="contacto" or contenido[1][0]=="laser" \
			and contenido[1][2]=="TRUE" or contenido[1][2]=="FALSE" \
			and contenido[1][3]=="ETIQUETA" or contenido[1][3]=="SUBRUTINA" \
			and len(contenido[1])==5:
				tipoSensor=contenido[1][0]
				numero=contenido[1][1]
				estado=contenido[1][2]
				tipo=contenido[1][3]
				destino=contenido[1][4]
				infoGuardar=contenido[1]
				if tipo=="ETIQUETA":
					texto="Si Sensor "+tipoSensor+" "+numero+"="+estado+" saltar a: "+destino
				elif tipo=="SUBRUTINA":
					texto="Si Sensor "+tipoSensor+" "+numero+"="+estado+" ejecutar subrutina: "+destino
			return [True, infoGuardar, texto]

		elif contenido[0]=="COMPROBAR_SENSOR" and len(contenido)==6:
			if contenido[1]=="contacto" or contenido[1]=="laser" \
			and contenido[3]=="TRUE" or contenido[3]=="FALSE" \
			and contenido[4]=="ETIQUETA" or contenido[4]=="SUBRUTINA":
				tipoSensor=contenido[1]
				numero=contenido[2]
				estado=contenido[3]
				tipo=contenido[4]
				destino=contenido[5]
				infoGuardar=contenido[1:]
				if tipo=="ETIQUETA":
					texto="Si Sensor "+tipoSensor+" "+numero+"="+estado+" saltar a: "+destino
				elif tipo=="SUBRUTINA":
					texto="Si Sensor "+tipoSensor+" "+numero+"="+estado+" ejecutar subrutina: "+destino
			return [True, infoGuardar, texto]

		else:
			return [False,None,None]

	except:
		return [False,None,None]

def comprobarInterrupcion(contenido):
	try:
		if contenido[0]=="INTERRUPCION" and len(contenido)==2:
			sensor=contenido[1][0]
			numero=contenido[1][1]
			subrutina=contenido[1][2]
			estado=contenido[1][3]
			texto="Interrupcion de "+sensor+" "+numero
			if estado=='activar':
				texto=texto+" Activada"
			elif estado=='desactivar':
				texto=texto+" Desactivada"
			datosAGuardar=[sensor, numero, subrutina, estado]
			return [True, datosAGuardar, texto]
		elif contenido[0]=="INTERRUPCION" and len(contenido)==5:
			sensor=contenido[1]
			numero=contenido[2]
			subrutina=contenido[3]
			estado=contenido[4]
			texto="Interrupcion "
			if estado=='activar':
				texto=texto+" de "+sensor+" "+numero+" Activada"
			elif estado=='desactivar':
				texto=texto+" de "+sensor+" "+numero+" Desactivada"
			datosAGuardar=[sensor, numero, subrutina, estado]
			return [True, datosAGuardar, texto]
		else:
			return [False, None, None]
	except:
		return [False, None, None]
