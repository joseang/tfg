#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
contenido = [NOMBRE_ORDEN, [INFORMACION_ORDEN]]
'''
def crearMensaje(contenido):
	if contenido[0]=="ABRIR_PINZA":
		return "Abrir pinza"

	elif contenido[0]=="CERRAR_PINZA":
		return "Cerrar pinza"

	elif contenido[0]=="IR":
		try:
			posicion=int(contenido[1])
			valor=int(contenido[3])
			if contenido[2]=="tiempo":
				mensaje="Ir a la posicion "+str(posicion)+" en "+str(valor)+" segundos"
			elif contenido[2]=="velocidad":
				mensaje="Ir a la posicion "+str(posicion)+" con velocidad "+str(valor)
			return mensaje
		except:
			return False
	elif contenido[0]=="LINEAL":
		try:
			posicion=int(contenido[1])
			valor=int(contenido[3])
			if contenido[2]=="tiempo":
				mensaje="Ir linealmente a la posicion "+str(posicion)+" en "+str(valor)+" segundos"
			elif contenido[2]=="velocidad":
				mensaje="Ir linealmente a la posicion "+str(posicion)+" con velocidad "+str(valor)
			return mensaje
		except:
			return False
	elif contenido[0]=="CIRCULAR":
		try:
			posicionFinal=int(contenido[1])
			posicionMedia=int(contenido[2])
			valor=int(contenido[4])
			if contenido[3]=="tiempo":
				mensaje="Ir circulamente a la posicion "+str(posicionFinal)+" via posicion "+str(posicionMedia)+" en "+str(valor)+" segundos"
			elif contenido[3]=="velocidad":
				mensaje="Ir circulamente a la posicion "+str(posicionFinal)+" via posicion "+str(posicionMedia)+" con velocidad "+str(valor)
			return mensaje
		except Exception, e:
			return False
	elif contenido[0]=="MEMORIZAR":
		try:
			posicion=int(contenido[1])
			mensaje="Memorizar en posicion "+str(posicion)
			return mensaje
		except:
			return [False, None]
	elif contenido[0]=="ESPERAR":
		try:
			tiempo=int(contenido[1])
			mensaje="Esperar "+str(tiempo)+" segundos"
			return mensaje
		except Exception, e:
			return False
	elif contenido[0]=="SALTAR":
		try:
			etiqueta=contenido[1]
			if contenido[0]=="SALTAR" and len(contenido)==2:
				mensaje="Saltar a: "+etiqueta
				return mensaje
			else:
				return None
		except:
			return [False, None, None]

	elif contenido[0]=="COMENTARIO":
		try:
			textoComentario=" ".join(contenido[1:])
			mensaje="# "+textoComentario
			return mensaje
		except:
			return False
	elif contenido[0]=="VARIABLE":
		try:
			valorExpresionVariable=contenido[1][0]
			nombreVariable=contenido[1][1]
			mensaje=str(nombreVariable)+" = "+str(valorExpresionVariable)
			return mensaje
		except:
			return False
	elif contenido[0]=="ETIQUETA":
		return contenido[1]+": "
	elif contenido[0]=="LLAMAR_SUBRUTINA":
		return "Ejecutar subrutina: "+contenido[1]
	elif contenido[0]=="SALTAR_SI":
		mensaje="Saltar a: " +contenido[1][3]+ " Si: "+contenido[1][1]+" "+contenido[1][2]+" "+contenido[1][0]
		return mensaje
