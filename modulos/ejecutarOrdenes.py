#!/usr/bin/env python
# -*- coding: utf-8 -*-
import rospy, sys, os, time, serial, inspect
sys.path.append('../cinematica')
from cinematica.kinematic_functions import *
from cinematica.auxiliar_cinematica import *
import numpy as np
from funciones import *
from std_msgs.msg import Float64
from scipy.interpolate import InterpolatedUnivariateSpline
from rly02 import send_command
from comprobarArchivo import *
from PyQt4 import QtCore, QtGui
import sympy as sp
from gazebo_msgs.msg import ModelStates, ContactsState
from sensor_msgs.msg import LaserScan
from gazebo_msgs.srv import GetModelProperties

from mostrarSubrutina import *

sys.path.insert(0, '../')

from path_absoluto import obtenerDireccionAbsoluta
direccionAbsoluta=obtenerDireccionAbsoluta()

''' orden ABRIR PINZA '''
def abrirPinza():
	if ejecutandose():
		print "se debe mostrar un mensaje de abrir pinza"
		if esRobotSimulacion():
			print "robot simulacion abre pinza"
			abrirPinzaSimulacion()
			tiempo_ejecucion=4
			return tiempo_ejecucion
		elif esRobotReal():
			print "robot real abre pinza"
			abrirPinzaReal()
			tiempo_ejecucion=4
			return tiempo_ejecucion

def abrirPinzaReal():
	try:
		send_command(0x70) # Rele 2 OFF
		time.sleep(0.1)
		send_command(0x65) # Rele 1 ON
	except:
		pass

def abrirPinzaSimulacion():
	t=3
	articulaciones=[-0.010,-0.010]
	pub = rospy.Publisher('/gripper/joint_trajectory_controller/follow_joint_trajectory/goal',FollowJointTrajectoryActionGoal)
	final=FollowJointTrajectoryActionGoal()
	final.goal.trajectory.joint_names=['base_joint_gripper_left', 'base_joint_gripper_right']
	final.goal.trajectory.points.append(JointTrajectoryPoint(positions = articulaciones, velocities = [0, 0],time_from_start = rospy.Duration(t)))
	pub.publish(final)


''' orden CERRAR PINZA '''
def cerrarPinza():
	if ejecutandose():
		if esRobotSimulacion():
			cerrarPinzaSimulacion()
			tiempo_ejecucion=4
			return tiempo_ejecucion
		elif esRobotReal():
			cerrarPinzaReal()
			tiempo_ejecucion=4
			return tiempo_ejecucion

def cerrarPinzaSimulacion():
	t=3
	articulaciones=[0,0]
	pub = rospy.Publisher('/gripper/joint_trajectory_controller/follow_joint_trajectory/goal',FollowJointTrajectoryActionGoal)
	final=FollowJointTrajectoryActionGoal()
	final.goal.trajectory.joint_names=['base_joint_gripper_left', 'base_joint_gripper_right']
	final.goal.trajectory.points.append(JointTrajectoryPoint(positions = articulaciones, velocities = [0, 0],time_from_start = rospy.Duration(t)))
	pub.publish(final)

def cerrarPinzaReal():
	try:
		send_command(0x6F) # Rele 1 OFF
		time.sleep(0.1)
		send_command(0x66) # Rele 2 ON
	except:
		pass

''' orden IR '''
def irPosicion(posicionFinal,valor,tipo="tiempo"):
	datos=obtenerDatosDePosicion(posicionFinal)
	datos[5], datos[6], datos[7] = np.deg2rad(datos[5]), np.deg2rad(datos[6]), np.deg2rad(datos[7])
	th_inicial=leerArticulacionesActuales()
	if comprobarHome(th_inicial):
		th_inicial=[0.1,0,0,0,0,0]
	if datos[0]==1:
		[x,y,z,roll,pitch,yaw]=datos[2:]
		matH=matHomo([x,y,z,roll,pitch,yaw])
		th_final=ikine(matH,th_inicial)
		if len(th_final)!=0:
			th=th_final[:6]
			if tipo=="velocidad":
				valor=obtenerTiempoDesdeVelocidad(th, valor)
			enviarPosicion(th,valor)
			return valor
	elif datos[0]==0:
		posicionReferencia=datos[1]
		if posicionFinal==posicionReferencia:
			QtGui.QMessageBox.about('Error','Las posiciones no pueden ser las mismas')
			return 0
		[x_rel,y_rel,z_rel,roll_rel,pitch_rel,yaw_rel]=obtenerDatosDePosicion(posicionFinal)[2:]
		[x_ref,y_ref,z_ref,roll_ref,pitch_ref,yaw_ref]=obtenerDatosDePosicion(posicionReferencia)[2:]
		x=x_rel+x_ref
		y=y_rel+y_ref
		z=z_rel+z_ref
		roll=roll_rel+roll_ref
		pitch=pitch_rel+pitch_ref
		yaw=yaw_rel+yaw_ref
		matH=matHomo([x,y,z,roll,pitch,yaw])
		th_final=ikine(matH,th_inicial)
		if len(th_final)!=0:
			th=th_final[:6]
			if tipo=="velocidad":
				valor=obtenerTiempoDesdeVelocidad(th, valor)
			enviarPosicion(th,valor)
			return valor

def enviarPosicion(articulaciones,tiempo):
	pub = rospy.Publisher('/arm/joint_trajectory_controller/follow_joint_trajectory/goal',FollowJointTrajectoryActionGoal,queue_size=15)
	if ejecutandose():
		#for i in range(len(articulaciones)):
		#	articulaciones[i]=round(articulaciones[i],8)
		#tiempo=round(tiempo,2)
		tiempo=int(tiempo)
		final=FollowJointTrajectoryActionGoal()
		final.goal.trajectory.joint_names=['arm_1_joint', 'arm_2_joint', 'arm_3_joint', 'arm_4_joint', 'arm_5_joint', 'arm_6_joint']
		final.goal.trajectory.points.append(JointTrajectoryPoint(positions = articulaciones, velocities = [0, 0, 0, 0, 0, 0],time_from_start = rospy.Duration(tiempo)))
		pub.publish(final)


''' orden IR ARTICULACION '''
def irPorArticulaciones(articulaciones, valor, tipo):
	articulaciones=map(float, articulaciones)
	if tipo=='velocidad':
		tiempo=obtenerTiempoDesdeVelocidad(articulaciones, valor)
	elif tipo=='tiempo':
		tiempo=valor
	articulaciones=map(np.deg2rad, articulaciones)
	enviarPosicion(articulaciones, tiempo)
	return tiempo

''' orden IR XYZ '''
def irPorXYZ(datos_posicion, valor, tipo):
	try:
		th_inicial=leerArticulacionesActuales()
		if comprobarHome(th_inicial):
			th_inicial=[0.1,0,0,0,0,0]
		angulos=datos_posicion[3:]
		angulos=map(float, angulos)
		angulos=map(np.deg2rad, angulos)
		datos_posicion[3:]=angulos
		matHomogenea=matHomo(datos_posicion)
		articulaciones=ikine(matHomogenea, th_inicial)
		articulaciones=articulaciones[:6]
		articulaciones=map(float, articulaciones)
		if tipo=='velocidad':
			tiempo=obtenerTiempoDesdeVelocidad(articulaciones, valor)
		elif tipo=='tiempo':
			tiempo=valor
		enviarPosicion(articulaciones, tiempo)
		return tiempo
	except:
		return 0

''' orden LINEAL '''
def movimientoLineal(posicionFinal,tiempo):
	th_inicial=leerArticulacionesActuales()
	[x_inicial, y_inicial, z_inicial, roll_inicial, pitch_inicial, yaw_inicial]=calcularPosicionConArticulacion(th_inicial)
	roll_inicial, pitch_inicial, yaw_inicial = np.deg2rad(roll_inicial), np.deg2rad(pitch_inicial), np.deg2rad(yaw_inicial)

	posicionFinal=obtenerPosicionSeleccionada(posicionFinal)
	[x_final, y_final, z_final, roll_final, pitch_final, yaw_final]=posicionFinal
	roll_final, pitch_final, yaw_final=np.deg2rad(roll_final), np.deg2rad(pitch_final), np.deg2rad(yaw_final)
	# Numero de posiciones muestreadas
	n=12
	m=float(n)

	# Muestreo de la trayectoria.
	if x_inicial==y_final:
		posiciones_X=np.zeros([n])
		posiciones_X[:]=x_inicial
	else:
		posiciones_X=np.linspace(x_inicial, x_final, num=n)

	if y_inicial==y_final:
		posiciones_Y=np.zeros([n])
		posiciones_Y[:]=y_inicial
	else:
		posiciones_Y=np.linspace(y_inicial, y_final, num=n)

	if z_inicial==z_final:
		posiciones_Z=np.zeros([n])
		posiciones_Z[:]=z_inicial
	else:
		posiciones_Z=np.linspace(z_inicial, z_final, num=n)

	# Calculo de la rotacion
	if yaw_inicial==yaw_final:
		orientaciones_Yaw=np.zeros([n])
		orientaciones_Yaw[:]=yaw_inicial
	else:
		orientaciones_Yaw=np.linspace(yaw_inicial, yaw_final, num=n)

	if pitch_inicial==pitch_final:
		orientaciones_Pitch=np.zeros([n])
		orientaciones_Pitch[:]=pitch_inicial
	else:
		orientaciones_Pitch=np.linspace(pitch_inicial, pitch_final, num=n)

	if roll_inicial==roll_final:
		orientaciones_Roll=np.zeros([n])
		orientaciones_Roll[:]=roll_inicial
	else:
		orientaciones_Roll=np.linspace(roll_inicial, roll_final, num=n)

	resultado0, resultado1, resultado2, resultado3=[], [], [], []
	resultado4, resultado5, resultado6, resultado7=[], [], [], []

	resultados={0:resultado0, 1:resultado1, 2:resultado2, 3:resultado3,
		4:resultado4, 5:resultado5, 6:resultado6, 7:resultado7}

	th_inicial=leerArticulacionesActuales()





	# crear los array
	lista_cero, lista_uno, lista_dos, lista_tres=[],[],[],[]
	lista_cuatro, lista_cinco, lista_seis, lista_siete=[],[],[],[]


	for i in range(n):
		matRot=obtenerMatRotacionDesdeAngulos(orientaciones_Roll[i],orientaciones_Pitch[i],orientaciones_Yaw[i])
		matPos=np.array([posiciones_X[i],posiciones_Y[i],posiciones_Z[i]])
		# Se crea la matriz homongena
		matH=np.array([[matRot[0,0], matRot[0,1], matRot[0,2],matPos[0]],
		[matRot[1,0], matRot[1,1], matRot[1,2],matPos[1]],[matRot[2,0],
		matRot[2,1], matRot[2,2],matPos[2]],[0, 0, 0,1]])
		th_p=[0,0,0,0,0,0]
		try:
			resultado_fallido=[999,999,999,999,999,999]
			resultados=ikine(matH,th_p)
			if len(resultados)==0:
				indices=[]
			else:
				indices=list(resultados[6,:])

			if 0 in indices:
				indice_=indices.index(0)
				resultado_=list(resultados[:6,indice_])
				lista_cero.append(resultado_)
			else:
				lista_cero.append(resultado_fallido)
			if 1 in indices:
				indice_=indices.index(1)
				resultado_=list(resultados[:6,indice_])
				lista_uno.append(resultado_)
			else:
				lista_uno.append(resultado_fallido)
			if 2 in indices:
				indice_=indices.index(2)
				resultado_=list(resultados[:6,indice_])
				lista_dos.append(resultado_)
			else:
				lista_dos.append(resultado_fallido)
			if 3 in indices:
				indice_=indices.index(3)
				resultado_=list(resultados[:6,indice_])
				lista_tres.append(resultado_)
			else:
				lista_tres.append(resultado_fallido)
			if 4 in indices:
				indice_=indices.index(4)
				resultado_=list(resultados[:6,indice_])
				lista_cuatro.append(resultado_)
			else:
				lista_cuatro.append(resultado_fallido)
			if 5 in indices:
				indice_=indices.index(5)
				resultado_=list(resultados[:6,indice_])
				lista_cinco.append(resultado_)
			else:
				lista_cinco.append(resultado_fallido)
			if 6 in indices:
				indice_=indices.index(6)
				resultado_=list(resultados[:6,indice_])
				lista_seis.append(resultado_)
			else:
				lista_seis.append(resultado_fallido)
			if 7 in indices:
				indice_=indices.index(7)
				resultado_=list(resultados[:6,indice_])
				lista_siete.append(resultado_)
			else:
				lista_siete.append(resultado_fallido)



		except:
			matH=np.array(matH)
			matH[:,3]=matH[:,3].round()
			try:
				resultadoInversa=ikine(matH,th_inicial)
				if comprobarHome(th_inicial):
					th_inicial=[0.1,0,0,0,0,0]
				th_inicial=resultadoInversa[:6]
			except:
				pass


	val0, val1, val2, val3, val4, val5, val6, val7=0,0,0,0,0,0,0,0
	if len(lista_cero)==n:
		for p in range(n):
			try:
				lista_cero[p].index(999)
				val0+=1
			except:
				pass
	if (len(lista_uno))==n:
		for p in range(n):
			try:
				lista_uno[p].index(999)
				val1+=1
			except:
				pass
	if (len(lista_dos))==n:
		for p in range(n):
			try:
				lista_dos[p].index(999)
				val2+=1
			except:
				pass
	if (len(lista_tres))==n:
		for p in range(n):
			try:
				lista_tres[p].index(999)
				val3+=1
			except:
				pass
	if (len(lista_cuatro))==n:
		for p in range(n):
			try:
				lista_cuatro[p].index(999)
				val4+=1
			except:
				pass
	if (len(lista_cinco))==n:
		for p in range(n):
			try:
				lista_cinco[p].index(999)
				val5+=1
			except:
				pass
	if (len(lista_seis))==n:
		for p in range(n):
			try:
				lista_seis[p].index(999)
				val6+=1
			except:
				pass
	if (len(lista_siete))==n:
		for p in range(n):
			try:
				lista_siete[p].index(999)
				val7+=1
			except:
				pass

	dif=[]
	th_ini=leerArticulacionesActuales()
	lista_buena=[]
	if val0==0:
		lista_buena.append(lista_cero)
		inicio_cero=lista_cero[0]
		dif_particular=[]
		for i in range(6):
			dif_particular.append(abs(inicio_cero[i]-th_ini[i]))
		dif.append(dif_particular)

	if val1==0:
		lista_buena.append(lista_uno)
		inicio_uno=lista_uno[0]
		dif_particular=[]
		for i in range(6):
			dif_particular.append(abs(inicio_uno[i]-th_ini[i]))
		dif.append(dif_particular)

	if val2==0:
		lista_buena.append(lista_dos)
		inicio_dos=lista_dos[0]
		dif_particular=[]
		for i in range(6):
			dif_particular.append(abs(inicio_dos[i]-th_ini[i]))
		dif.append(dif_particular)

	if val3==0:
		lista_buena.append(lista_tres)
		inicio_tres=lista_tres[0]
		dif_particular=[]
		for i in range(6):
			dif_particular.append(abs(inicio_tres[i]-th_ini[i]))
		dif.append(dif_particular)
	if val4==0:
		lista_buena.append(lista_cuatro)
		inicio_cuatro=lista_cuatro[0]
		dif_particular=[]
		for i in range(6):
			dif_particular.append(abs(inicio_cuatro[i]-th_ini[i]))
		dif.append(dif_particular)
	if val5==0:
		lista_buena.append(lista_cinco)
		inicio_cinco=lista_cinco[0]
		dif_particular=[]
		for i in range(6):
			dif_particular.append(abs(inicio_cinco[i]-th_ini[i]))
		dif.append(dif_particular)
	if val6==0:
		lista_buena.append(lista_seis)
		inicio_seis=lista_seis[0]
		dif_particular=[]
		for i in range(6):
			dif_particular.append(abs(inicio_seis[i]-th_ini[i]))
		dif.append(dif_particular)
	if val7==0:
		lista_buena.append(lista_siete)
		inicio_siete=lista_siete[0]
		dif_particular=[]
		for i in range(6):
			dif_particular.append(abs(inicio_siete[i]-th_ini[i]))
		dif.append(dif_particular)

	indice_bueno=None
	for i in range(len(dif)):
		rechazar=False
		for k in range(len(dif[i])):
			if dif[i][k]>0.5:
				rechazar=True
		if rechazar==False:
			indice_bueno=i
		rechazar=False
	if indice_bueno==None:
		print "Ninguna de las posibles es válida (raro)"
	else:
		resultados_definitivos=lista_buena[indice_bueno]


	th1, th2, th3, th4, th5, th6=[], [], [], [], [], []
	for k in range(n):
		th1.append(resultados_definitivos[k][0])
		th2.append(resultados_definitivos[k][1])
		th3.append(resultados_definitivos[k][2])
		th4.append(resultados_definitivos[k][3])
		th5.append(resultados_definitivos[k][4])
		th6.append(resultados_definitivos[k][5])
	thetas=[th1, th2, th3, th4, th5, th6]

	nuevoTamano=40
	resultadoInterpolado=np.empty(shape=(6,nuevoTamano))
	for p in range(6):
		theta=thetas[p]
		t=np.linspace(0, int(tiempo), num=n)
		theta_nueva = np.linspace(theta[0], theta[len(theta)-1] , num=nuevoTamano)  # Dominio
		tiempo_nuevo = np.linspace(t[0], t[len(t)-1] , num=nuevoTamano)  # Dominio
		resultadoInterpolado[p,:] = InterpolatedUnivariateSpline(t, theta)(tiempo_nuevo)
	limite_superado=irTrayectoria(resultadoInterpolado,tiempo_nuevo)
	if limite_superado:
		print "LIMITE SUPERADO"
		return True

def irTrayectoria(trayectoria,tiempo):
	pub=rospy.Publisher('/arm/joint_trajectory_controller/follow_joint_trajectory/goal',FollowJointTrajectoryActionGoal)
	if ejecutandose():
		ruta=FollowJointTrajectoryActionGoal()
		ruta.goal.trajectory.joint_names=['arm_1_joint', 'arm_2_joint', 'arm_3_joint', 'arm_4_joint', 'arm_5_joint', 'arm_6_joint']
		tiempos=tiempo
		for i in range(len(trayectoria[0])):
			posicion=trayectoria[:,i]
			if i>1:
				posicion_actual=posicion
				tiempo_actual=tiempos[i]
				posicion_anterior=trayectoria[:,i-1]
				tiempo_anterior=tiempos[i-1]
				limite_superado=comprobar_limite_velocidad(posicion_actual, posicion_anterior, tiempo_actual, tiempo_anterior)
				if limite_superado:
					return True
			ruta.goal.trajectory.points.append(JointTrajectoryPoint(positions = posicion, velocities = [0, 0, 0, 0, 0, 0],time_from_start = rospy.Duration(tiempos[i])))
		pub.publish(ruta)

def comprobar_limite_velocidad(posicion_actual, posicion_anterior, tiempo_actual, tiempo_anterior):
	diferencia_posicion=posicion_actual-posicion_anterior
	diferencia_tiempo=float(tiempo_actual-tiempo_anterior)
	lista_velocidad_requerida=diferencia_posicion/diferencia_tiempo
	velocidad_requerida=max(lista_velocidad_requerida)
	if velocidad_requerida>0.4:
		return True #Limite superado
	else:
		return False


''' orden IR TRAYECTORIA ARTICULACIONES '''
def ejecutar_trayectoria_articulaciones(datos):
	pub=rospy.Publisher('/arm/joint_trajectory_controller/follow_joint_trajectory/goal',FollowJointTrajectoryActionGoal)
	if ejecutandose():
		ruta=FollowJointTrajectoryActionGoal()
		ruta.goal.trajectory.joint_names=['arm_1_joint', 'arm_2_joint', 'arm_3_joint', 'arm_4_joint', 'arm_5_joint', 'arm_6_joint']
		for fila in datos:
			trayectoria=fila[:6]
			tiempo=float(fila[6])
			print "trayectoria: ", trayectoria
			print "tiempo: ", tiempo
			print
			ruta.goal.trajectory.points.append(JointTrajectoryPoint(positions = trayectoria, velocities = [0, 0, 0, 0, 0, 0],time_from_start = rospy.Duration(tiempo)))
		pub.publish(ruta)
	return tiempo


''' orden IR TRAYECTORIA CARTESIANA '''
def ejecutar_trayectoria_cartesiana(datos):
	pub=rospy.Publisher('/arm/joint_trajectory_controller/follow_joint_trajectory/goal',FollowJointTrajectoryActionGoal)
	if ejecutandose():
		ruta=FollowJointTrajectoryActionGoal()
		ruta.goal.trajectory.joint_names=['arm_1_joint', 'arm_2_joint', 'arm_3_joint', 'arm_4_joint', 'arm_5_joint', 'arm_6_joint']
		for fila in datos:
			try:
				posicion=fila[:6]
				tiempo=float(fila[6])
				print "t: ", tiempo
				matHomogenea=matHomo(posicion)
				th_ini=leerArticulacionesActuales()
				if comprobarHome(th_ini):
					th_ini=[0.1,0,0,0,0,0]
				trayectoria=ikine(matHomogenea, th_ini)[:6]
			except:
				os.system('clear')
				print "FUERA DEL ESPACIO DE TRABAJO"
			print "trayectoria: ", trayectoria
			print "tiempo: ", tiempo
			print
			ruta.goal.trajectory.points.append(JointTrajectoryPoint(positions = trayectoria, velocities = [0, 0, 0, 0, 0, 0],time_from_start = rospy.Duration(tiempo)))
		pub.publish(ruta)
	return tiempo


''' orden CIRCULAR '''
def movimientoCircular(numeroPosicionFinal, numeroPosicionMedia, tiempo):
	pass
	th_inicial=leerArticulacionesActuales()
	posicionInicial=calcularPosicionConArticulacion(th_inicial)
	for i in range(3):
		posicionInicial[i]=float(round(posicionInicial[i]))
	#print "posicionInicial: ", posicionInicial
	p1=posicionInicial[:3]
	#print "p1: ", p1

	posicionFinal=obtenerPosicionSeleccionada(numeroPosicionFinal)
	for i in range(3):
		posicionFinal[i]=float(round(posicionFinal[i]))
	#print "posicionFinal: ", posicionFinal
	p3=posicionFinal[:3]
	#print "p3: ", p3

	posicionMediaSuperior=obtenerPosicionSeleccionada(numeroPosicionMedia)
	for i in range(3):
		posicionMediaSuperior[i]=float(round(posicionMediaSuperior[i]))
	#print "posicionMedia: ", posicionMediaSuperior
	p2=posicionMediaSuperior[:3]
	#print "p2: ", p2

	MP=np.array([p1,p2,p3])
	MPM=np.array([p1+(p2-p1)/2,p3+(p2-p3)/2])

	x=sp.Symbol('x')
	y=sp.Symbol('y')
	z=sp.Symbol('z')
	vsym=np.array([x, y, z])
	matrizPlano=np.array([vsym-p1,p2-p1,p3-p1])
	matrizPlano=np.squeeze(matrizPlano)
	mPlano=sp.Matrix(matrizPlano)
	detPlano=mPlano.det()

	v_1=sp.diff(detPlano,x)
	v_2=sp.diff(detPlano,y)
	v_3=sp.diff(detPlano,z)
	n=np.array([v_1,v_2,v_3])

	v=np.array([p2-p1,p2-p3])

	MVi0=np.array([n,v[0]])
	MVi1=np.array([n,v[1]])

	x1=sp.Symbol('x1')
	y1=sp.Symbol('y1')
	z1=sp.Symbol('z1')
	vsym1=np.array([x1, y1, z1])
	Sistema1=np.dot(MVi0,vsym1)

	x2=sp.Symbol('x2')
	y2=sp.Symbol('y2')
	z2=sp.Symbol('z2')
	vsym2=np.array([x2, y2, z2])
	Sistema2=np.dot(MVi1,vsym2)

	if p1[0]==p2[0] and p1[0]==p3[0]: # X es constante
		#print "X es constante"
		y1=1
		Sistema1_0=str(Sistema1[0])
		Sistema1_1=str(Sistema1[1])
		sol1=sp.solve(eval(Sistema1_0))[0]
		sol2=sp.solve(eval(Sistema1_1))[0]
		vp3=np.array([sol1,y1,sol2])
		y2=1
		Sistema2_0=str(Sistema2[0])
		Sistema2_1=str(Sistema2[1])
		sol3=sp.solve(eval(Sistema2_0))[0]
		sol4=sp.solve(eval(Sistema2_1))[0]
		vp2=np.array([sol3,y2,sol4])

	elif p1[1]==p2[1] and p1[1]==p3[1]: # y es constante
		#print "Y es constante"
		x1=1
		Sistema1_0=str(Sistema1[0])
		Sistema1_1=str(Sistema1[1])
		sol1=sp.solve(eval(Sistema1_0))[0]
		sol2=sp.solve(eval(Sistema1_1))[0]
		vp3=np.array([x1,sol1,sol2])
		x2=1
		Sistema2_0=str(Sistema2[0])
		Sistema2_1=str(Sistema2[1])
		sol3=sp.solve(eval(Sistema2_0))[0]
		sol4=sp.solve(eval(Sistema2_1))[0]
		vp2=np.array([x2,sol3,sol4])

	elif p1[2]==p2[2] and p1[2]==p3[2]: # z es constante
		#print "Z es constante"
		x1=1
		Sistema1_0=str(Sistema1[0])
		Sistema1_1=str(Sistema1[1])
		sol1=sp.solve(eval(Sistema1_0))[0]
		sol2=sp.solve(eval(Sistema1_1))[0]
		vp3=np.array([x1,sol2,sol1])
		#print "vp3: ",vp3

		x2=1
		Sistema2_0=str(Sistema2[0])
		Sistema2_1=str(Sistema2[1])
		sol3=sp.solve(eval(Sistema2_0))[0]
		sol4=sp.solve(eval(Sistema2_1))[0]
		vp2=np.array([x2,sol4,sol3])
		#print "vp2: ",vp2

	else: # todos cambian
		#print "Ninguna es constante"
		x1=1
		Sistema1_0=str(Sistema1[0])
		Sistema1_1=str(Sistema1[1])
		Sistema1_0=eval(Sistema1_0)
		Sistema1_1=eval(Sistema1_1)
		solucion=sp.solve([Sistema1_0, Sistema1_1])
		vp3=np.array([x1,solucion[y1],solucion[z1]])
		x2=1
		Sistema2_0=str(Sistema2[0])
		Sistema2_1=str(Sistema2[1])
		Sistema2_0=eval(Sistema2_0)
		Sistema2_1=eval(Sistema2_1)
		solucion=sp.solve([Sistema2_0, Sistema2_1])
		vp2=np.array([x2,solucion[y2],solucion[z2]])

	MVP=np.array([vp3,vp2])

	r=sp.Symbol('r')
	s=sp.Symbol('s')

	Sistema3=MPM[0,:]+r*MVP[0,:]-MPM[1,:]-s*MVP[1,:]

	if p1[0]==p2[0] and p1[0]==p3[0]: # X es constante
		resolver=[Sistema3[1], Sistema3[2]]

	elif p1[1]==p2[1] and p1[1]==p3[1]: # Y es constante
		resolver=[Sistema3[0], Sistema3[2]]

	elif p1[2]==p2[2] and p1[2]==p3[2]: # Z es constante
		resolver=[Sistema3[0], Sistema3[1]]

	else:
		resolver=[Sistema3[2], Sistema3[1]]

	solucion=sp.solve(resolver)

	r=solucion[r]
	s=solucion[s]

	C=MPM[0,:]+r*MVP[0,:]

	MPaux=MP

	normAux_0=abs(MPaux[0,:]-C)
	normAux_1=abs(MPaux[1,:]-C)
	normAux_2=abs(MPaux[2,:]-C)

	norm_0=math.sqrt(normAux_0[0]**2+normAux_0[1]**2+normAux_0[2]**2)
	norm_1=math.sqrt(normAux_1[0]**2+normAux_1[1]**2+normAux_1[2]**2)
	norm_2=math.sqrt(normAux_2[0]**2+normAux_2[1]**2+normAux_2[2]**2)

	MR=np.array([norm_0, norm_1, norm_2])

	#,np.linalg.norm(MP[1,:]-C),np.linalg.norm(MP[2,:]-C)
	#MR=[norm(MP(1,:)-C);norm(MP(2,:)-C);norm(MP(3,:)-C)];
	centro=C
	radio=MR[0]
	#print "centro: ", centro
	#print "radio: ", radio

	centro_x, centro_y, centro_z=centro[0], centro[1], centro[2]

	A=-2*centro_x
	B=-2*centro_y
	C=-2*centro_z
	D=centro_x**2+centro_y**2+centro_z**2-radio**2

	n=5

	if p1[2]==p2[2] and p1[2]==p3[2]: # Z es constante
		#print "Z es constante (II)"
		z=p1[2]
		lista_z=np.linspace(z,z,num=n)
		'''
		- Hay dos posibilidads: que se mueva a traves del eje X o del eje Y
		- Con el siguiente If se averigua cual es (comparando si el X final es el mismo
		que el inicial, en ese caso, se mueve en Y)

		'''
		if p1[0]==p3[0]:
			lista_y=np.linspace(p1[1], p3[1], num=n)
			lista_x=[]
			for y in lista_y:
				N=y**2+z**2+B*y+C*z+D
				x1=(-A+math.sqrt(A**2-4*N))/2
				x2=(-A-math.sqrt(A**2-4*N))/2
				sub_1=abs(x1-p2[0])
				sub_2=abs(x2-p2[0])
				if sub_1<sub_2:
					lista_x.append(x1)
				else:
					lista_x.append(x2)
		else:
			lista_x=np.linspace(p1[0], p3[0], num=n)
			lista_y=[]
			for x in lista_x:
				N=x**2+z**2+A*x+C*z+D
				y1=(-B+math.sqrt(B**2-4*N))/2
				y2=(-B-math.sqrt(B**2-4*N))/2
				sub_1=abs(y1-p2[1])
				sub_2=abs(y2-p2[1])
				if sub_1<sub_2:
					lista_y.append(y1)
				else:
					lista_y.append(y2)

	elif p1[0]==p2[0] and p1[0]==p3[0]: # X es constante
		#print "X es constante (II)"
		x=p1[0]
		lista_x=np.linspace(x,x,num=n)
		if p1[1]==p3[1]: #Si es igual (la Y), se mueve en Z
			lista_z=np.linspace(p1[2], p3[2], num=n)
			lista_y=[]
			for z in lista_z:
				N=x**2+z**2+A*x+C*z+D
				y1=(-B+math.sqrt(B**2-4*N))/2
				y2=(-B-math.sqrt(B**2-4*N))/2
				sub_1=abs(y1-p2[1])
				sub_2=abs(y2-p2[1])
				if sub_1<sub_2:
					lista_y.append(y1)
				else:
					lista_y.append(y2)
		else:
			lista_y=np.linspace(p1[1], p3[1], num=n)
			lista_z=[]
			for y in lista_y:
				N=x**2+y**2+A*x+B*y+D
				z1=(-C+math.sqrt(C**2-4*N))/2
				z2=(-C-math.sqrt(C**2-4*N))/2
				sub_1=abs(z1-p2[2])
				sub_2=abs(z2-p2[2])
				if sub_1<sub_2:
					lista_z.append(z1)
				else:
					lista_z.append(z2)



	elif p1[1]==p2[1] and p1[1]==p3[1]: # Y es constante
		#print "Y es constante (II)"
		y=p1[1]
		lista_y=np.linspace(y,y,num=n)
		if p1[0]==p3[0]: #Si es igual (la Y), se mueve en Z
			lista_z=np.linspace(p1[2], p3[2], num=n)
			lista_x=[]
			for z in lista_z:
				N=y**2+z**2+B*y+C*z+D
				x1=(-A+math.sqrt(A**2-4*N))/2
				x2=(-A-math.sqrt(A**2-4*N))/2
				sub_1=abs(x1-p2[0])
				sub_2=abs(x2-p2[0])
				if sub_1<sub_2:
					lista_x.append(x1)
				else:
					lista_x.append(x2)
		else:
			lista_x=np.linspace(p1[0], p3[0], num=n)
			lista_z=[]
			for x in lista_x:
				N=y**2+x**2+B*y+A*x+D
				z1=(-C+math.sqrt(C**2-4*N))/2
				z2=(-C-math.sqrt(C**2-4*N))/2
				sub_1=abs(z1-p2[2])
				sub_2=abs(z2-p2[2])
				if sub_1<sub_2:
					lista_z.append(z1)
				else:
					lista_z.append(z2)

	'''
	En este punto hay una lista de posiciones X, Y, Z
	'''
	#print "X: \n", lista_x
	#print "Y: \n", lista_y
	#print "Z: \n", lista_z

	for i in range(len(posicionFinal)):
		posicionInicial[i]=round(posicionInicial[i],1)
		posicionFinal[i]=round(posicionFinal[i],1)

	lista_yaw=np.linspace(posicionInicial[3], posicionFinal[3], num=n)
	lista_pitch=np.linspace(posicionInicial[4], posicionFinal[4], num=n)
	lista_roll=np.linspace(posicionInicial[5], posicionFinal[5], num=n)

	#print "Yaw: \n", lista_yaw
	#print "Pitch: \n", lista_pitch
	#print "Roll: \n", lista_roll

	resultados=[]
	th_ini=leerArticulacionesActuales()
	for i in range(n):
		matHomogenea=np.zeros([4,4])
		matRotacion=obtenerMatRotacionDesdeAngulos(lista_roll[i], lista_pitch[i], lista_yaw[i])
		matHomogenea[:3,:3]=matRotacion
		matHomogenea[0,3]=lista_x[i]
		matHomogenea[1,3]=lista_y[i]
		matHomogenea[2,3]=lista_z[i]
		#print "matHomogenea: \n", matHomogenea
		resulAux=ikine(matHomogenea,th_ini)[:6]
		resultados.append(resulAux)
		if len(resulAux)!=0:
			th_ini=resulAux
			#print "Hay solucion ("+str(i)+")"


	if len(resultados)==n:
		#print "La solucion esta completa"
		resultadoFinal=resultados
		nuevoTamano=10
		#resultadoInterpolado=np.empty([6,nuevoTamano])
		resultadoInterpolado=np.empty(shape=(6,nuevoTamano))
		#print resultadoInterpolado[:,0]
		for p in range(6):
			theta=[]
			for k in range(n):
				theta.append(resultadoFinal[k][p])
			t=np.linspace(0, int(tiempo), num=n)
			theta_nueva = np.linspace(theta[0], theta[len(theta)-1] , num=nuevoTamano)  # Dominio
			tiempo_nuevo = np.linspace(t[0], t[len(t)-1] , num=nuevoTamano)  # Dominio
			resultadoInterpolado[p,:] = InterpolatedUnivariateSpline(t, theta)(tiempo_nuevo)
		limite_superado=irTrayectoria(resultadoInterpolado,tiempo_nuevo)
		if limite_superado:
			print "LIMITE SUPERADO"
			return True
	else:
		pass
		#print "No hay solucion?"


''' orden MEMORIZAR POSICION '''
def memorizar(numeroPosicion):
	nombreArchivo=direccionAbsoluta+"/posiciones/P"+str(numeroPosicion)+'.txt'
	# Siempre se guardar absoluta
	tipoPosicion=1
	RelativaA=0
	archivoPosicion=open(nombreArchivo,"w+")
	archivoPosicion.truncate()
	articulaciones=leerArticulacionesActuales()
	matHomo=fkineArm(articulaciones)
	X, Y, Z =matHomo[0:3,3]
	X, Y, Z = float(X), float(Y), float(Z)
	matRotacion=matHomo[:3,:3]
	roll, pitch, yaw=obtenerAngulosDesdeMatRot(matRotacion)
	roll, pitch, yaw=np.rad2deg(roll), np.rad2deg(pitch), np.rad2deg(yaw)
	posicionAEscribir=str(tipoPosicion)+" "+str(RelativaA)+" "+str(X)+" "+str(Y)+" "+str(Z)+" "+str(roll)+" "+str(pitch)+" "+str(yaw)
	archivoPosicion.write(posicionAEscribir)
	archivoPosicion.close()


''' orden ESPERAR '''
def esperarRobot(tiempo):
	global tiempoGazebo
	if esRobotReal:
		tiempo=float(tiempo)
		ciclos=float(tiempo/0.5)
		for i in range(int(ciclos)):
			time.sleep(0.5)
			#TODO: hacer la funcion para comprobar si se ha pulsado
			pulsadoStop=funcionParaRevisarSiSeHaPulsadoSTOP()
			if pulsadoStop:
				return False
	elif esRobotSimulacion:
		#TODO: Atinar mas en los tiempo, usar nanosec ?
		tiempoInicial=-1
		tiempoGazebo=-1
		tiempoAsignado=False
		while tiempoAsignado==False:
			if tiempoGazebo==-1:
				rospy.Subscriber("clock",Clock,reloj)
				tiempoInicial=tiempo_gazebo
				if tiempoGazebo!=-1:
					tiempoAsignado=True
			tiempo=float(tiempo)
			tiempoRestante=0.0
			while tiempoGazebo!=(tiempoInicial+tiempo):
				tiempo_restante=(tiempoInicial+tiempo)-tiempoGazebo
				tiempo_restante=float(tiempo_restante)
				ciclos=float(tiempo_restante/0.25)
				for i in range(int(ciclos)):
					time.sleep(0.25)
					pulsadoStop=funcionParaRevisarSiSeHaPulsadoSTOP()
					if pulsadoStop:
						return False
				rospy.Subscriber("clock",Clock,self.reloj)

def reloj(data):
	global tiempoGazebo
	tiempoGazebo=data.clock.secs


''' orden SALTAR A '''
def saltarA(etiquetaDestino,listaOrdenes,listaInfo):
	indices = [i for i, x in enumerate(listaOrdenes) if x == "ETIQUETA"]
	for i in indices:
		if listaInfo[i]==etiquetaDestino:
			return int(i)-1
	return False


''' orden VARIABLE '''
def ponerVariable(listaVariables, nombreVariable, expresion):
	lista_llaves=listaVariables.keys()
	if not lista_llaves==[]:
		for i in lista_llaves:
			exec(str(i)+"="+str(listaVariables[i]))
	try:
		listaVariables[nombreVariable]=eval(str(expresion))
		lista_llaves=listaVariables.keys()
		archivo=open(direccionAbsoluta+'/data/variables','w+')
		archivo.truncate()
		archivo.close()
		archivo=open(direccionAbsoluta+'/data/variables','a')
		print
		print
		print
		print "lista_llaves: ", lista_llaves
		for i in lista_llaves:
			textoGuardar=str(i)+" "+str(listaVariables[i])+'\n'
			print "textoGuardar (ponerVariable): ", textoGuardar
			archivo.write(textoGuardar)
		archivo.close()
	except:
		print "Poner variable ha fallado"
		return [False, None]
	return [True, listaVariables]


''' orden SALTAR SI '''
def saltar_si(indiceLocal, diccionarioVariables, valorCondicion, variableCondicion, condicion, etiquetaDestino, listaOrdenes, listaInfo):
	valorQueDebeSer=diccionarioVariables[variableCondicion]
	if comprobarCondicion(valorQueDebeSer, valorCondicion, condicion):
		indice=saltarA(etiquetaDestino,listaOrdenes, listaInfo)
		return indice
	else:
		return indiceLocal

def comprobarCondicion(valor1, valor2, condicion):
	valor1=float(valor1)
	valor2=float(valor2)
	if condicion=='==':
		if valor1==valor2:
			return True
		else:
			return False
	elif condicion=='>':
		if valor1>valor2:
			return True
		else:
			return False
	elif condicion=='<':
		if valor1<valor2:
			return True
		else:
			return False
	elif condicion=='>=':
		if valor1>=valor2:
			return True
		else:
			return False
	elif condicion=='<=':
		if valor1<=valor2:
			return True
		else:
			return False
	elif condicion=='!=':
		if valor1!=valor2:
			return True
		else:
			return False


''' orden COMPROBAR SI SENSOR == X , ... '''
def comprobar_estado_sensor(lista, indice, listaOrdenes, listaInfo, self):
	global listaObjetosGazebo, leidoModelStates
	tipoSensor=lista[0]
	numero=lista[1]
	estadoDeseado=lista[2]
	tipoDestino=lista[3]
	destino=lista[4]

	obtener_modelo=rospy.ServiceProxy('/gazebo/get_model_properties', GetModelProperties)
	nombre_objeto="sensor_"+tipoSensor+numero
	objeto_disponible=obtener_modelo(nombre_objeto).success
	if objeto_disponible==False:
		return False # El objeto no esta en el simulador

	if tipoSensor=="contacto":
		archivo=open(direccionAbsoluta+"/data/sensores/contacto"+numero, 'r')
		estadoDelSensor=archivo.read()
		archivo.close()
	elif tipoSensor=="laser":
		archivo=open(direccionAbsoluta+"/data/sensores/laser"+numero, 'r')
		estadoDelSensor=archivo.read()
		archivo.close()

	if estadoDelSensor==estadoDeseado:
		if tipoDestino=="ETIQUETA":
			indice=saltarA(destino,listaOrdenes,listaInfo)
			return indice
		elif tipoDestino=="SUBRUTINA":
			indice=ejecutar_subrutina(destino,indice)
			return indice
	else:
		return indice

	'''
	if estadoDelSensor=="True":
		if tipoDestino=="ETIQUETA":
			indice=saltarA(destino,listaOrdenes,listaInfo)
			return indice
		elif tipoDestino=="SUBRUTINA":
			indice=ejecutar_subrutina(destino,indice)
			return indice
	elif estadoDelSensor=="False":
		return indice
	'''


''' orden EJECUTAR SUBRUTINA '''
def realizarAccionSubrutina(listaOrdenes, listaInfo, indiceLocal):
	if listaOrdenes[indiceLocal]=="ABRIR_PINZA":
		tiempo_espera=abrirPinza()
		print "tiempo_espera: ", tiempo_espera
		return indiceLocal, float(tiempo_espera)
	elif listaOrdenes[indiceLocal]=="CERRAR_PINZA":
		tiempo_espera=cerrarPinza()
		print "tiempo_espera: ", tiempo_espera
		return indiceLocal, float(tiempo_espera)

	elif listaOrdenes[indiceLocal]=="IR":
		#print "Se mueve a una posicion"
		numeroPosicion=listaInfo[indiceLocal][0]
		variable=listaInfo[indiceLocal][1]
		valor=listaInfo[indiceLocal][2]
		if variable=='tiempo':
			tiempo_espera=int(valor)
			irPosicion(numeroPosicion, valor)
			#time.sleep(tiempo)
		else:
			tiempo_espera=irPosicion(numeroPosicion, valor, 'velocidad')
			#time.sleep(tiempo_espera)
		return indiceLocal, tiempo_espera

	elif listaOrdenes[indiceLocal]=="TR_ARTICULACIONES":
		direccionArchivo=listaInfo[indiceLocal]
		datos=np.loadtxt(direccionArchivo)
		tiempo_espera=ejecutar_trayectoria_articulaciones(datos)
		return indiceLocal, tiempo_espera

	elif listaOrdenes[indiceLocal]=="TR_XYZ":
		direccionArchivo=listaInfo[indiceLocal]
		datos=np.loadtxt(direccionArchivo)
		tiempo_espera=ejecutar_trayectoria_cartesiana(datos)
		return indiceLocal, tiempo_espera

	elif listaOrdenes[indiceLocal]=="LINEAL":
		#print "Se mueve linealmente a una posicion"
		posicionFinal=listaInfo[indiceLocal][0]
		variable=listaInfo[indiceLocal][1]
		valor=listaInfo[indiceLocal][2]
		if variable=='tiempo':
			tiempo=valor
		else:
			matHomogenea=np.zeros([4,4])
			x,y,z,roll,pitch,yaw=obtenerPosicionSeleccionada(posicionFinal)
			matHomogenea[:3,:3]=obtenerMatRotacionDesdeAngulos(roll,pitch,yaw)
			matHomogenea[0,3]=x
			matHomogenea[1,3]=y
			matHomogenea[2,3]=z
			th_actual=leerArticulacionesActuales()
			thetas_rad=ikine(matHomogenea,th_actual)
			tiempo=obtenerTiempoDesdeVelocidad(thetas_rad, valor)
		movimientoLineal(posicionFinal,tiempo)
		time.sleep(tiempo)
		return indiceLocal, tiempo

	elif listaOrdenes[indiceLocal]=="CIRCULAR":
		#print "Se mueve circularmente a una posicion"
		posicionFinal=listaInfo[indiceLocal][0]
		posicionMedia=listaInfo[indiceLocal][1]
		variable=listaInfo[indiceLocal][2]
		valor=listaInfo[indiceLocal][3]
		if variable=='tiempo':
			tiempo=valor
		else:
			matHomogenea=np.zeros([4,4])
			x,y,z,roll,pitch,yaw=obtenerPosicionSeleccionada(posicionFinal)
			matHomogenea[:3,:3]=obtenerMatRotacionDesdeAngulos(roll,pitch,yaw)
			matHomogenea[0,3]=x
			matHomogenea[1,3]=y
			matHomogenea[2,3]=z
			th_actual=leerArticulacionesActuales()
			thetas_rad=ikine(matHomogenea,th_actual)
			tiempo=obtenerTiempoDesdeVelocidad(thetas_rad, valor)
		movimientoCircular(posicionFinal, posicionMedia, tiempo)
		time.sleep(tiempo)
		return indiceLocal, tiempo

	elif listaOrdenes[indiceLocal]=="MEMORIZAR":
		#print "Memoriza una posicion"
		numeroPosicion=listaInfo[indiceLocal]
		memorizar(numeroPosicion)
		return indiceLocal, None

	elif listaOrdenes[indiceLocal]=="ESPERAR":
		#print "Esperar un tiempo"
		#print listaInfo[indiceLocal]
		tiempo=listaInfo[indiceLocal]
		#esperarRobot(tiempo)
		return indiceLocal, tiempo

	elif listaOrdenes[indiceLocal]=="COMENTARIO":
		#print "Comentario"
		return indiceLocal, None

	elif listaOrdenes[indiceLocal]=="VARIABLE":
		#print "Modificar el valor de una variable"
		#diccionarioVariables[nombreVariable]=ponerVariable(expresion)
		nombre=listaInfo[indiceLocal][1]
		expresion=listaInfo[indiceLocal][0]
		# hago que devuelva el nuevo valor por si quiero mostrarlo en la tabla
		# pero la modificacion de la varible la realiza al completo en
		# la funcion 'ponerVariable'
		funciona, nuevoValor=ponerVariable(expresion, nombre)
		return indiceLocal, None


''' orden ACTIVAR-DESACTIVAR INTERRUPCIONES '''
def activar_desactivar_interrupciones(dicInterrupciones, listaInfo):
	tipoSensor=listaInfo[0]
	numero=listaInfo[1]
	subrutina=listaInfo[2]
	estadoActivacion=listaInfo[3]
	if estadoActivacion=="activar":
		dicInterrupciones[tipoSensor+numero]=[True,subrutina]
	elif estadoActivacion=="desactivar":
		dicInterrupciones[tipoSensor+numero]=[False,None]
	return dicInterrupciones
