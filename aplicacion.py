#!/usr/bin/env python
# -*- coding: utf-8 -*-
from PyQt4 import QtCore, QtGui
from string import *
import sys, os, time, math, glob, random, thread, threading
import numpy as np
import subprocess
import rospy
from rosgraph_msgs.msg import Clock, Log
from std_msgs.msg import Float64
from control_msgs.msg import FollowJointTrajectoryActionGoal
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from sensor_msgs.msg import JointState
from gazebo_msgs.srv import SpawnModel
from geometry_msgs.msg import Pose
from actionlib_msgs.msg import GoalID
from cinematica.kinematic_functions import *
from cinematica.powerball_constants import *
from gazebo_msgs.srv import GetModelState
from interfaz.ventanaPrincipal import *
from modulos.funciones import *
from modulos.ejecutarOrdenes import *
from modulos.funciones_sensores import *
from guardarPosiciones import *
from movimientoManual import *
from listadoPrograma import *
from comandos import *
from tablaPosiciones import *
from introducirObjetos import *
from controlMando import *
from mostrarSubrutina import *
from std_srvs.srv import Trigger
from multiprocessing import Process
from subprocess import check_output

from path_absoluto import obtenerDireccionAbsoluta
direccionAbsoluta=obtenerDireccionAbsoluta()

class enviarJointState(QObject):
	senal=pyqtSignal(bool)
	def __init__(self):
		QObject.__init__(self)
	def punch(self,i):
		self.senal.emit(i)

def iniciar_simulador():
	iniciar_sim=Process(target=iniciar_sim_aux)
	iniciar_sim.start()

def parar_simulador():
	os.system("killall roslaunch")
	os.system("killall gzclient")
	os.system("killall gzclient")
	os.system("killall gzserver")
	os.system("killall gzserver")
	os.system("killall aggregator_node")
	os.system("killall cob_control_mode_adapter_node")
	os.system("killall cob_control_mode_adapter_node")
	os.system("killall cob_frame_tracker_node")
	os.system("killall cob_twist_controller_node ")
	os.system("killall interactive_frame_target_node")

def iniciar_general():
	roscore = subprocess.Popen('roscore')
	time.sleep(2)
	rospy.init_node('aplicacion_powerball', anonymous=False)
	suscribirseATodo()
	iniciarSuscripciones()

def parar_general():
	try:
		pararRobot()
		pararPinza()
	except:
		pass
	os.system("killall roslaunch")
	os.system("killall roscore")
	os.system("killall rosmaster")
	os.system("killall rosout")
	os.system("killall roslaunch")
	os.system("killall rosmaster")
	os.system("killall roscore")
	os.system("killall rosout")

def iniciar_real():
	iniciar_real=Process(target=iniciar_real_aux)
	iniciar_real.start()

def parar_real():
	stop_real()
	time.sleep(2)
	os.system("killall canopen_motor_node")
	os.system("killall cob_control_mode_adapter_node")
	os.system("killall roslaunch")
	os.system("killall robot_state_publisher")
	os.system("killall relay")

def stop_real():
	servicio_shutdown=rospy.ServiceProxy('/arm/driver/shutdown', Trigger)
	respuesta=servicio_shutdown()

def init_real():
	servicio_init=rospy.ServiceProxy('/arm/driver/init', Trigger)
	respuesta=servicio_init()

def iniciar_real_aux():
	os.system("gnome-terminal -x sh -c 'roslaunch schunk_lwa4p robot.launch && exec bash'");
	time.sleep(6)
	init_real()

def suscribirseATodo():
	suscribirseJointStates()
	suscribirseSensorContacto()
	suscribirseSensorLaser()
	suscribirseAlimentador()

def suscribirseAlimentador():
	#sub_alimentador0=rospy.Subscriber("/gazebo/alimentador",ContactsState,receptor_alimentador)
	sub_alimentador0=rospy.Subscriber("/gazebo/alimentador0",ContactsState,alimentador0)
	sub_alimentador1=rospy.Subscriber("/gazebo/alimentador1",ContactsState,alimentador1)
	sub_alimentador2=rospy.Subscriber("/gazebo/alimentador2",ContactsState,alimentador2)
	sub_alimentador3=rospy.Subscriber("/gazebo/alimentador3",ContactsState,alimentador3)
	sub_alimentador4=rospy.Subscriber("/gazebo/alimentador4",ContactsState,alimentador4)
	sub_alimentador5=rospy.Subscriber("/gazebo/alimentador5",ContactsState,alimentador5)
	sub_alimentador6=rospy.Subscriber("/gazebo/alimentador6",ContactsState,alimentador6)
	sub_alimentador7=rospy.Subscriber("/gazebo/alimentador7",ContactsState,alimentador7)
	sub_alimentador8=rospy.Subscriber("/gazebo/alimentador8",ContactsState,alimentador8)
	sub_alimentador9=rospy.Subscriber("/gazebo/alimentador9",ContactsState,alimentador9)

def suscribirseJointStates():
	rospy.Subscriber("/arm/joint_states",JointState,joint_actual)

def suscribirseSensorLaser():
	rospy.Subscriber("/gazebo/laser",LaserScan,chequear_sensor_laser)

def suscribirseSensorContacto():
	rospy.Subscriber("/gazebo/contacto",ContactsState,chequear_sensor_contacto)

def iniciar_sim_aux():
	os.system("gnome-terminal -x sh -c 'roslaunch schunk_lwa4p sim.launch && exec bash'");

def iniciarSuscripciones():
	th=leerArticulacionesActuales()
	tiempo=1
	enviarPosicion(th,tiempo)
	abrirPinzaSimulacion()

class MainWindow(QtGui.QMainWindow):
	def __init__(self):
		QtGui.QMainWindow.__init__(self, None, QtCore.Qt.WindowStaysOnTopHint)
		self.ui = Ui_Principal()
		self.ui.setupUi(self)
		self.show()
		self.move(0,0)
		self.connect(self.ui.botonOrdenesPosibles, QtCore.SIGNAL('clicked()'), self.comandos)
		self.connect(self.ui.botonOrdenesGuardadas, QtCore.SIGNAL('clicked()'), self.listado)
		self.connect(self.ui.botonGuardarPosiciones, QtCore.SIGNAL('clicked()'), self.guardarPosiciones)
		self.connect(self.ui.botonTablaPosiciones, QtCore.SIGNAL('clicked()'), self.tablaPosiciones)
		self.connect(self.ui.botonAnadirObjetos, QtCore.SIGNAL('clicked()'), self.anadirObjeto)
		self.connect(self.ui.botonMovimientoManual, QtCore.SIGNAL('clicked()'), self.movimientoManual)
		self.connect(self.ui.botonMando, QtCore.SIGNAL('clicked()'), self.activarMando)
		self.connect(self.ui.botonArrancarRobot, QtCore.SIGNAL('clicked()'), self.arrancarRobot)
		self.connect(self.ui.radioReal, QtCore.SIGNAL('clicked()'), self.cambiarTipoEjecucion)
		self.connect(self.ui.radioSimulacion, QtCore.SIGNAL('clicked()'), self.cambiarTipoEjecucion)
		self.cambiarTipoEjecucion()
		self.ejecutandoseFalse()
		iniciar_general()
		borrarTodo()
		self.cerrarPrograma=False

		global actualizar_joint_state
		actualizar_joint_state=enviarJointState()
		actualizar_joint_state.senal.connect(self.receptorActualizarJointState)

		hiloActualizarJointState=threading.Thread(target=self.actualizar_valor_posicion,name='Daemon')
		hiloActualizarJointState.start()

	def cambiarTipoEjecucion(self):
		archivo=open(direccionAbsoluta+'/data/tipoEjecucion','w+')
		if self.ui.radioReal.isChecked():
			archivo.write('robot_real')
			self.tipoEjecucion='robot_real'
		else:
			archivo.write('simulacion')
			self.tipoEjecucion='simulacion'
		archivo.close()

	def ejecutandoseFalse(self):
		archivo=open(direccionAbsoluta+'/data/ejecutandose','w+')
		archivo.write('False')
		self.ejecutandose=False
		archivo.close()

	def receptorActualizarJointState(self, estado):
		if estado==True:
			thetas=leerArticulacionesActuales()
			matHomogenea=fkineArm(thetas)
			X, Y, Z=matHomogenea[0,3], matHomogenea[1,3], matHomogenea[2,3]
			X, Y, Z=int(X), int(Y), int(Z)
			matRotacion=matHomogenea[:3,:3]
			roll, pitch, yaw=obtenerAngulosDesdeMatRot(matRotacion)
			roll, pitch, yaw=np.rad2deg(roll), np.rad2deg(pitch), np.rad2deg(yaw)
			roll, pitch, yaw=int(roll), int(pitch), int(yaw)
			self.ui.posicionX.setText(str(X))
			self.ui.posicionY.setText(str(Y))
			self.ui.posicionZ.setText(str(Z))
			self.ui.orientacionRoll.setText(str(roll))
			self.ui.orientacionPitch.setText(str(pitch))
			self.ui.orientacionYaw.setText(str(yaw))

	def actualizar_valor_posicion(self):
		while not self.cerrarPrograma:
			actualizar_joint_state.senal.emit(True)
			time.sleep(0.2)

	def actualizarPosicionOrientacion(self, data):
		th=data.position
		matHomongenea=fkineArm(th)
		X, Y, Z=matHomongenea[0,3], matHomongenea[1,3], matHomongenea[2,3]

	def arrancarRobot(self):
		if self.ui.botonArrancarRobot.isChecked():
			archivo=open(direccionAbsoluta+'/data/ejecutandose','w+')
			archivo.write('True')
			self.ejecutandose=True
			archivo.close()
			if self.ui.radioSimulacion.isChecked(): #ejecutar simulador
				iniciar_simulador()
			elif self.ui.radioReal.isChecked(): #ejecutar robot real
				iniciar_real()
			self.ui.radioReal.setEnabled(False)
			self.ui.radioSimulacion.setEnabled(False)
		else:
			try:
				pararRobot()
				pararPinza()
			except:
				pass
			archivo=open(direccionAbsoluta+'/data/ejecutandose','w+')
			archivo.write('False')
			self.ejecutandose=False
			archivo.close()
			if self.ui.radioSimulacion.isChecked():
				p=Process(target=parar_simulador)
				p.start()
			elif self.ui.radioReal.isChecked():
				p_real=Process(target=parar_real)
				p_real.start()
			self.ui.radioReal.setEnabled(True)
			self.ui.radioSimulacion.setEnabled(True)

	def closeEvent(self,event):
		mensaje=u'¿Quieres devolver al robot a su posición inicial?'
		respuesta=QtGui.QMessageBox.question(self, 'Robot a posicion Home', mensaje, QtGui.QMessageBox.No, QtGui.QMessageBox.Yes)
		if respuesta==QtGui.QMessageBox.Yes:
			th=[0,0,0,0,0,0]
			tiempo=3
			enviarPosicion(th,tiempo)
			time.sleep(3)
		ventanaMovimientoManual.close()
		ventanaGuardarPosiciones.close()
		ventanaTablaPosiciones.close()
		ventanaComandos.close()
		ventanaListadoPrograma.close()
		ventanaMando.close()
		ventanaAnadirObjetos.close()
		self.cerrarPrograma=True
		if self.ui.radioSimulacion.isChecked():
			p=Process(target=parar_simulador)
			p.start()
		elif self.ui.radioReal.isChecked():
			p=Process(target=parar_real)
			p.start()
		parar_general()
		self.close()

	def activarMando(self):
		ventanaMando.show()

	def comandos(self):
		ventanaComandos.show()

	def listado(self):
		ventanaListadoPrograma.show()

	def movimientoManual(self):
		ventanaMovimientoManual.show()

	def guardarPosiciones(self):
		ventanaGuardarPosiciones.show()

	def tablaPosiciones(self):
		ventanaTablaPosiciones.show()

	def anadirObjeto(self):
		ventanaAnadirObjetos.show()


if __name__ == '__main__':
	app = QtGui.QApplication(sys.argv)
	ventanaPrincipal=MainWindow()

	ventanaMostrarSubrutina=mostrarSubrutina()
	ventanaMostrarSubrutina.hide()

	ventanaMovimientoManual=movimientoManual()
	ventanaMovimientoManual.hide()

	ventanaGuardarPosiciones=guardarPosiciones()
	ventanaGuardarPosiciones.hide()

	ventanaComandos=comandos()
	ventanaComandos.hide()

	ventanaListadoPrograma=listadoPrograma(ventanaMostrarSubrutina=ventanaMostrarSubrutina)
	ventanaListadoPrograma.hide()

	ventanaTablaPosiciones=tablaPosiciones()
	ventanaTablaPosiciones.hide()

	ventanaAnadirObjetos=introducirObjetos()
	ventanaAnadirObjetos.hide()

	ventanaMando=controlMando()
	ventanaMando.hide()

	ventanaComandos.senalEnviarComandos.connect(ventanaListadoPrograma.recibirComandos)

	ventanaListadoPrograma.envioListaPosicionesATabla.connect(ventanaTablaPosiciones.receptorListaPosiciones)
	ventanaGuardarPosiciones.envioListaPosicionesATabla.connect(ventanaTablaPosiciones.receptorListaPosiciones)

	ventanaListadoPrograma.envioSenalArchivoCargado.connect(ventanaGuardarPosiciones.receptorSenalArchivoCargado)

	ventanaListadoPrograma.envioSenalOrdenSubrutina.connect(ventanaMostrarSubrutina.receptorSenalOrdenSubrutina)

	sys.exit(app.exec_())
