#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os, sys, rospy, thread, threading
from interfaz.ventanaIntroducirObjetos import *
from geometry_msgs.msg import Pose
from gazebo_msgs.srv import SpawnModel
from cinematica.transformations import quaternion_about_axis, quaternion_multiply
import random
import numpy as np
from multiprocessing import Process

from path_absoluto import obtenerDireccionAbsoluta
direccionAbsoluta=obtenerDireccionAbsoluta()

class introducirObjetos(QtGui.QDialog):
	numeroObjeto=0
	def __init__(self,parent=None):
		QtGui.QWidget.__init__(self, parent,QtCore.Qt.WindowStaysOnTopHint)
		self.ui = Ui_IntroducirObjetos()
		self.ui.setupUi(self)
		self.modificarActivadosGeneral(0)
		self.connect(self.ui.botonAnadir, QtCore.SIGNAL('clicked()'), self.anadirObjeto)
		self.connect(self.ui.listaObjetosSensores, QtCore.SIGNAL('currentIndexChanged(int)'), self.modificarActivadosGeneral)
		self.connect(self.ui.listaPiezasAlimentador, QtCore.SIGNAL('currentIndexChanged(int)'), self.modificarActivadosPiezas)

	def modificarActivadosPiezas(self, numero):
		if numero==0:
			self.ui.textoTamano1.setText('Alto')
			self.ui.textoTamano2.setText('Largo')
			self.ui.textoTamano3.setText('Ancho')
			self.ui.valorTamano1.setEnabled(True)
			self.ui.valorTamano2.setEnabled(True)
			self.ui.valorTamano3.setEnabled(True)
		elif numero==1:
			self.ui.textoTamano1.setText('Alto')
			self.ui.textoTamano2.setText('Radio')
			self.ui.textoTamano3.setText('')
			self.ui.valorTamano1.setEnabled(True)
			self.ui.valorTamano2.setEnabled(True)
			self.ui.valorTamano3.setEnabled(False)
		elif numero==2:
			self.ui.textoTamano1.setText('Radio')
			self.ui.textoTamano2.setText('')
			self.ui.textoTamano3.setText('')
			self.ui.valorTamano1.setEnabled(True)
			self.ui.valorTamano2.setEnabled(False)
			self.ui.valorTamano3.setEnabled(False)

	def modificarActivadosGeneral(self, numero):
		if numero==0:
			self.ui.textoTamano1.setText('Alto')
			self.ui.textoTamano2.setText('Largo')
			self.ui.textoTamano3.setText('Ancho')
			self.ui.valorTamano1.setEnabled(True)
			self.ui.valorTamano2.setEnabled(True)
			self.ui.valorTamano3.setEnabled(True)
			self.ui.listaColores.setEnabled(True)
			self.ui.numeroObjeto.setEnabled(False)
			self.ui.listaPiezasAlimentador.setEnabled(False)
			self.ui.cantidadAlimentador.setEnabled(False)
		elif numero==1:
			self.ui.textoTamano1.setText('Alto')
			self.ui.textoTamano2.setText('Radio')
			self.ui.textoTamano3.setText('')
			self.ui.valorTamano1.setEnabled(True)
			self.ui.valorTamano2.setEnabled(True)
			self.ui.valorTamano3.setEnabled(False)
			self.ui.listaColores.setEnabled(True)
			self.ui.numeroObjeto.setEnabled(False)
			self.ui.listaPiezasAlimentador.setEnabled(False)
			self.ui.cantidadAlimentador.setEnabled(False)
		elif numero==2:
			self.ui.textoTamano1.setText('Radio')
			self.ui.textoTamano2.setText('')
			self.ui.textoTamano3.setText('')
			self.ui.valorTamano1.setEnabled(True)
			self.ui.valorTamano2.setEnabled(False)
			self.ui.valorTamano3.setEnabled(False)
			self.ui.listaColores.setEnabled(True)
			self.ui.numeroObjeto.setEnabled(False)
			self.ui.listaPiezasAlimentador.setEnabled(False)
			self.ui.cantidadAlimentador.setEnabled(False)
		elif numero==3 or numero==4:
			self.ui.textoTamano1.setText('')
			self.ui.textoTamano2.setText('')
			self.ui.textoTamano3.setText('')
			self.ui.valorTamano1.setEnabled(False)
			self.ui.valorTamano2.setEnabled(False)
			self.ui.valorTamano3.setEnabled(False)
			self.ui.listaColores.setEnabled(True)
			self.ui.numeroObjeto.setEnabled(True)
			self.ui.listaPiezasAlimentador.setEnabled(False)
			self.ui.cantidadAlimentador.setEnabled(False)
		elif numero==5:
			self.ui.textoTamano1.setText('')
			self.ui.textoTamano2.setText('')
			self.ui.textoTamano3.setText('')
			self.ui.valorTamano1.setEnabled(True)
			self.ui.valorTamano2.setEnabled(True)
			self.ui.valorTamano3.setEnabled(True)
			self.ui.listaColores.setEnabled(True)
			self.ui.numeroObjeto.setEnabled(True)
			self.ui.listaPiezasAlimentador.setEnabled(True)
			self.ui.cantidadAlimentador.setEnabled(True)
			self.modificarActivadosPiezas(0)
		elif numero==6:
			self.ui.textoTamano1.setText('')
			self.ui.textoTamano2.setText('')
			self.ui.textoTamano3.setText('')
			self.ui.valorTamano1.setEnabled(False)
			self.ui.valorTamano2.setEnabled(False)
			self.ui.valorTamano3.setEnabled(False)
			self.ui.listaColores.setEnabled(False)
			self.ui.numeroObjeto.setEnabled(True)
			self.ui.listaPiezasAlimentador.setEnabled(False)
			self.ui.cantidadAlimentador.setEnabled(False)

	def obtenerPoseObjeto(self, tipo=""):
		x=self.ui.posicionX.text()
		y=self.ui.posicionY.text()
		z=self.ui.posicionZ.text()
		yaw=self.ui.orientacionYaw.text()
		pitch=self.ui.orientacionPitch.text()
		roll=self.ui.orientacionRoll.text()
		try:
			x, y, z=float(x)/1000, float(y)/1000, float(z)/1000
			yaw, pitch,roll=float(yaw), float(pitch), float(roll)
			if tipo=="laser":
				yaw=yaw+180
			yaw, pitch, roll=np.deg2rad(yaw), np.deg2rad(pitch), np.deg2rad(roll)
		except:
			mensaje=u'Valoores de posición incorrectos'
			QtGui.QMessageBox.about(self, 'Error',mensaje)
			return

		eje_x, eje_y, eje_z=[1,0,0], [0,1,0], [0,0,1]
		qx=quaternion_about_axis(roll,eje_x)
		qy=quaternion_about_axis(pitch,eje_y)
		qz=quaternion_about_axis(yaw,eje_z)
		q=quaternion_multiply(qx,qy)
		q=quaternion_multiply(q,qz)

		pose_inicial=Pose()
		pose_inicial.position.x=-x
		pose_inicial.position.y=-y
		pose_inicial.position.z=z+0.7505
		pose_inicial.orientation.x=q[0]
		pose_inicial.orientation.y=q[1]
		pose_inicial.orientation.z=q[2]
		pose_inicial.orientation.w=q[3]
		return pose_inicial

	def obtenerTamano(self, tipo):
		if tipo=='caja':
			try:
				alto=float(self.ui.valorTamano1.text())/1000
				largo=float(self.ui.valorTamano2.text())/1000
				ancho=float(self.ui.valorTamano3.text())/1000
				dimensiones=[alto, largo, ancho]
				return dimensiones
			except:
				mensaje=u'Solo números'
				QtGui.QMessageBox.about(self, 'Error',mensaje)
		elif tipo=='cilindro':
			try:
				alto=float(self.ui.valorTamano1.text())/1000
				radio=float(self.ui.valorTamano2.text())/1000
				dimensiones=[alto, radio]
				return dimensiones
			except:
				mensaje=u'Solo números'
				QtGui.QMessageBox.about(self, 'Error',mensaje)
		elif tipo=='esfera':
			try:
				radio=float(self.ui.valorTamano1.text())/1000
				return radio
			except:
				mensaje=u'Solo números'
				QtGui.QMessageBox.about(self, 'Error',mensaje)

	def cargarURDFCubo(self,seleccionarColor, indiceColorObjeto, contenido_final):
		archivo_primero=direccionAbsoluta+"/gazebo/piezas/prisma_primera.urdf"
		abierto_primero=open(archivo_primero,'r')
		urdf_primero=abierto_primero.read()
		archivo_segunda=direccionAbsoluta+"/gazebo/piezas/prisma_segunda.urdf"
		abierto_segunda=open(archivo_segunda,'r')
		urdf_segunda=abierto_segunda.read()
		archivo_tercera=direccionAbsoluta+"/gazebo/piezas/prisma_tercera.urdf"
		abierto_tercera=open(archivo_tercera,'r')
		urdf_tercera=abierto_tercera.read()
		dimensiones=self.obtenerTamano('caja')
		x, y, z=dimensiones[0], dimensiones[1], dimensiones[2]
		x, y, z=str(x), str(y), str(z)
		contenido_urdf=urdf_primero+'<box size="'+x+' '+y+' '+x+'" />\n'+urdf_segunda+'<box size="'+x+' '+y+' '+x+'" />\n'+urdf_tercera+seleccionarColor[indiceColorObjeto]+contenido_final
		return contenido_urdf

	def cargarURDFCilindro(self,seleccionarColor, indiceColorObjeto, contenido_final):
		archivo_primero=direccionAbsoluta+"/gazebo/piezas/cilindro_primero.urdf"
		abierto_primero=open(archivo_primero,'r')
		urdf_primero=abierto_primero.read()
		archivo_segunda=direccionAbsoluta+"/gazebo/piezas/cilindro_segundo.urdf"
		abierto_segunda=open(archivo_segunda,'r')
		urdf_segunda=abierto_segunda.read()
		archivo_tercera=direccionAbsoluta+"/gazebo/piezas/cilindro_tercero.urdf"
		abierto_tercera=open(archivo_tercera,'r')
		urdf_tercera=abierto_tercera.read()
		dimensiones=self.obtenerTamano('cilindro')
		alto, radio=dimensiones[0], dimensiones[1]
		alto, radio=str(alto), str(radio)
		contenido_urdf=urdf_primero+'<cylinder length="'+alto+'" radius="'+radio+'" />\n'+urdf_segunda+'<cylinder length="'+alto+'" radius="'+radio+'" />\n'+urdf_tercera+seleccionarColor[indiceColorObjeto]+contenido_final
		return contenido_urdf

	def cargarURDFEsfera(self,seleccionarColor, indiceColorObjeto, contenido_final):
		archivo_primero=direccionAbsoluta+"/gazebo/piezas/esfera_primera.urdf"
		abierto_primero=open(archivo_primero,'r')
		urdf_primero=abierto_primero.read()
		archivo_segunda=direccionAbsoluta+"/gazebo/piezas/esfera_segunda.urdf"
		abierto_segunda=open(archivo_segunda,'r')
		urdf_segunda=abierto_segunda.read()
		archivo_tercera=direccionAbsoluta+"/gazebo/piezas/esfera_tercera.urdf"
		abierto_tercera=open(archivo_tercera,'r')
		urdf_tercera=abierto_tercera.read()
		radio=self.obtenerTamano('esfera')
		radio=str(radio)
		contenido_urdf=urdf_primero+'<sphere radius="'+radio+'" />\n'+urdf_segunda+'<sphere radius="'+radio+'" />\n'+urdf_tercera+seleccionarColor[indiceColorObjeto]+contenido_final
		return contenido_urdf

	def cargarURDFContacto(self, seleccionarColor, indiceColorObjeto, contenido_final):
		archivoInicial=direccionAbsoluta+"/gazebo/sensores/contacto/contacto.urdf.xacro"
		inicio_urdf=open(archivoInicial,'r')
		contenido_urdf=inicio_urdf.read()+seleccionarColor[indiceColorObjeto]+contenido_final
		return contenido_urdf

	def cargarURDFLaser(self, numero):
		archivoInicial=direccionAbsoluta+"/gazebo/sensores/laser/laser_inicial.urdf"
		inicio_urdf=open(archivoInicial,'r')
		archivoMedio=direccionAbsoluta+"/gazebo/sensores/laser/laser_medio.urdf"
		medio_urdf=open(archivoMedio,'r')
		contenido_urdf=inicio_urdf.read()+"<frameName>"+"laser"+str(numero)+"</frameName>\n"+medio_urdf.read()#+"laser"+str(numero)+final_urdf.read()
		return contenido_urdf

	def anadirObjeto(self):
		global numero
		final_urdf=open(direccionAbsoluta+'/gazebo/piezas/final.urdf','r')
		contenido_final=final_urdf.read()
		seleccionarColor={0:"Gazebo/Red", 1:"Gazebo/Blue", 2:"Gazebo/Green", 3:"Gazebo/Yellow", 4:"Gazebo/White", 5:"Gazebo/Black", 6:"Gazebo/Grey"}
		indiceColorObjeto=self.ui.listaColores.currentIndex()
		objetoElegido=self.ui.listaObjetosSensores.currentIndex()
		if self.ui.numeroObjeto.isEnabled():
			try:
				numero=self.ui.numeroObjeto.text()
				numero=int(numero)
			except:
				mensaje=u'Introduce un número'
				QtGui.QMessageBox.about(self, 'Error',mensaje)
		else:
			numero=int(random.random()*100000)
		if objetoElegido==0: #Caja
			nombre="caja"+str(numero)
			contenido_urdf=self.cargarURDFCubo(seleccionarColor, indiceColorObjeto, contenido_final)
			pose_inicial=self.obtenerPoseObjeto()
		elif objetoElegido==1: #cilindro
			nombre="cilindro"+str(numero)
			contenido_urdf=self.cargarURDFCilindro(seleccionarColor, indiceColorObjeto, contenido_final)
			pose_inicial=self.obtenerPoseObjeto()
		elif objetoElegido==2: #esfera
			nombre="esfera"+str(numero)
			contenido_urdf=self.cargarURDFEsfera(seleccionarColor, indiceColorObjeto, contenido_final)
			pose_inicial=self.obtenerPoseObjeto()
		elif objetoElegido==3: #sensor laser
			nombre="sensor_laser"+str(numero)
			contenido_urdf=self.cargarURDFLaser(numero)
			pose_inicial=self.obtenerPoseObjeto('laser')
		elif objetoElegido==4: #sensor contacto
			nombre="sensor_contacto"+str(numero)
			contenido_urdf=self.cargarURDFContacto(seleccionarColor, indiceColorObjeto, contenido_final)
			pose_inicial=self.obtenerPoseObjeto()
		elif objetoElegido==5:
			dicPiezas={0:'caja', 1:'cilindro', 2:'esfera'}
			pose_inicial=self.obtenerPoseObjeto()
			nombre="alimentador"+str(numero)
			direccion=direccionAbsoluta+"/gazebo/otros/alimentador/numeros/alimentador"+str(numero)+".urdf.xacro"
			archivo_urdf=open(direccion,'r')
			contenido_urdf=archivo_urdf.read()
			try:
				cantidad=int(self.ui.cantidadAlimentador.text())
				indicePieza=self.ui.listaPiezasAlimentador.currentIndex()
				if indicePieza==0:
					tamano=self.obtenerTamano('caja')
				elif indicePieza==1:
					tamano=self.obtenerTamano('cilindro')
				elif indicePieza==2:
					tamano=self.obtenerTamano('esfera')
			except:
				return
			print "tamano: ", tamano
			if type(tamano)!=list:
				tamano_final=str(tamano)
			else:
				tamano=map(str,tamano)
				tamano_final=" ".join(tamano)
			config=open(direccionAbsoluta+'/data/alimentador/configuracion/alimentador'+str(numero), 'w+')
			texto=str(cantidad)+" "+str(indicePieza)+" "+tamano_final
			print "texto: ", texto
			config.write(texto)
			config.close()
		elif objetoElegido==6: #camara
			nombre="camara"+str(numero)
			archivo_primero=direccionAbsoluta+"gazebo/otros/camara/camara_primero.urdf"
			archivo_segundo=direccionAbsoluta+"gazebo/otros/camara/camara_segundo.urdf"
			inicio_urdf=open(archivo_primero,'r')
			final_urdf=open(archivo_segundo,'r')
			linea="<cameraName>rrbot/camera"+str(numero)+"</cameraName>"
			contenido_urdf=inicio_urdf.read()+linea+final_urdf.read()
			pose_inicial=self.obtenerPoseObjeto()
		try:
			servicio_modelo = rospy.ServiceProxy('/gazebo/spawn_urdf_model', SpawnModel)
			servicio_modelo(nombre,contenido_urdf,"gazebo",pose_inicial,"world")
			if objetoElegido==6:
				arrancar_ventana=Process(target=self.lanzarVentanaCamara)
				arrancar_ventana.start()

		except:
			QtGui.QMessageBox.about(self, 'Error','Revisa los datos. Hay un error.')

	def lanzarVentanaCamara(self):
		global numero
		orden="rosrun image_view image_view image:=/gazebo/rrbot/camera"+str(numero)+"/image_raw"
		os.system(orden)


if __name__ == '__main__':
	app = QtGui.QApplication(sys.argv)
	ventanaObjetos = introducirObjetos()
	ventanaObjetos.show()
	rospy.init_node('introducirObjetos')
	sys.exit(app.exec_())
